# Hermes

An inter-process message broker with message queueing capability.

1. [Installation](#installation)
2. [Usage](#usage)
    1. [Using Ephemeral Inboxes](#using-ephemeral-inboxes)
        1. [Subscribing to Ad Hoc Inboxes](#using-ephemeral-inboxessubscribing-to-ad-hoc-inboxes)
        2. [Subscribing to Well-Known Inboxes](#using-ephemeral-inboxessubscribing-to-well-known-inboxes)
    2. [Using Pseudo Permanent Inboxes](#using-pseudo-permanent-inboxes)
        1. [Schema Manipulation](#using-pseudo-permanent-inboxesschema-manipulation)
        2. [Subscribing to Well-Known Unboxes](#using-pseudo-permanent-inboxessubscribing-to-well-known-inboxes)
    3. [Unsubscribing from a Topic](#unsubscribing-from-a-topic)
    4. [Publishing to a Topic](#publishing-to-a-topic)
    5. [Halting/Resuming Hermes operation](#halting-resuming-hermes-operation)
    6. [Error Handling](#error-handling)
3. [Configuration](#configuration)

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `hermes` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:hermes, "~> 1.0.0"}
  ]
end
```

## Usage

Before being able to describe how to use Hermes' interface I have to provide you
with some context. So take a breath and consider this brief description
of its behaviour.

Since Hermes is a message broker, it works on a publish/subscribe principle.
So, process `A` subscribes a handler to a topic. Process `B` publishes messages
on said topic. All messages from `B` to `A` will be "bufferized" on an inbox
that is kept private to that handler.

Elixir will start Hermes before starting your application, since it was added
as a dependency. Once started, Hermes will commence operation, that is,
it will repeatedly sweep through its subscribed handlers list and try to
"feed them" their respective inbox messages.

Hermes has two types of inboxes:

 - **Ephemeral inboxes**: They do not survive an unsubscribe call, meaning that
 when a process unsubscribes from the topic, its inbox is dropped and
 the messages will be gone for good. The current dispatch task will
 be signalled to stop and the handler will be removed from the dispatcher.
 All new publications to that inbox will "sink" and be discarded.

 - **Pseudo Permanent Inboxes**: They survive an unsubscription call, that is,
 when a process unsubscribes from a topic, its inbox is kept running.
 The current dispatch task will be signalled to stop and the handler
 will be removed from the dispatcher. All new publications to that inbox
 will be kept and the inbox will keep growing until a new handler is subscribed,
 in which point the deliveries will resume. These inboxes
 are made part of a schema that survives application reinitialization.

In the current implementation, **all inboxes exist in memory only,
so they will not survive an application restart**, that's why I call them
"Pseudo Permanent Inboxes".

### Using Ephemeral Inboxes

The simplest usage of Hermes requires you to just subscribe to a topic.
This process will register a handler to a dispatch system and as soon
as messages are published on the topic id you provided, they will be
delivered to the given handler. Note that your handler will not receive
messages sent to the topic prior to subscription because those will be discarded
and as explained before, once you unsubscribe the handler, the inbox will be
discarded along with its content, hence the term "ephemeral" on the name.

#### Subscribing to an Ad Hoc Inbox

When you need to receive events from a topic but there is no prior information
regarding what inbox to use, you may omit the inbox id on the call
and have Hermes create one for you. Also, because the inbox name is unknown in
advance, ad hoc inboxes are always ephemeral.

_Examples_
```elixir
# You may use a function reference
{topic_id, inbox_id} = Hermes.subscribe("my-topic", &some_function/3)

# An anonymous function
local_func = fn t, i, m -> do_something_else(t, i, m) end
{topic_id, inbox_id} = Hermes.subscribe("my-topic", local_func)

# Or a pair module name / function name
{topic_id, inbox_id} = Hermes.subscribe("my-topic", My.Module, :my_function)
```

#### Subscribing to an Well-known Inbox

The name is quite self explanatory. You might decide prior to execution that
your handler will receive messages incoming via a specific inbox. If the name is
not part of a schema, it will be treated as an ephemeral inbox.

_Examples_
```elixir
# You may use a function reference
Hermes.subscribe("my-topic", "my-inbox", &some_function/3)

# An anonymous function
local_func = fn t, i, m -> do_something_else(t, i, m) end
Hermes.subscribe("my-topic", "my-inbox", local_func)

# Or a pair module name / function name
Hermes.subscribe("my-topic", "my-inbox", My.Module, :my_function)
```

### Using Pseudo Permanent Inboxes

As stated before, "Pseudo Permanent Inboxes" are created using a schema
that outlives application execution, that way, when Hermes starts,
those inboxes will be restored in an empty state and all publications to them
will be kept instead of sinking. As soon as you subscribe to the inbox,
the dispatcher starts to deliver accumulated messages to the given handler.

In order to have Pseudo Permanent Inboxes you only have to create a schema.

#### Schema manipulation

**Via Mix Tasks**

Hermes provides a set of mix tasks that allows you to prepare the schema before
starting the application.

_Examples_
```bash
# Dropping the current schema (it must exist first)
$ mix hermes.schema.drop

# Creating a new schema (there must be no prior schema)
$ mix hermes.schema.create

# Importing a schema specification (it will overwrite the current schema)
# See file specs ahead in this document
$ mix hermes.schema.import -f "path_to_schema_spec.terms"

# Add inboxes to a topic
$ mix hermes.inboxes.add -t my-topic -i inbox1,inbox2,inbox3

# Remove an entire topic
$ mix hermes.inboxes.drop -t my-topic

# Remove some inboxes from a topic
$ mix hermes.inboxes.drop -t my-topic -i inbox2,inbox5

# Show the current schema
$ mix hermes.inboxes.list
```

**Programmatically via Hermes API**

There is a set of functions provided by Hermes public interface,
used to manipulate the schema programmatically allowing the same operations
available via mix tasks.

_Examples_
```elixir
# Dropping the current schema (it must exist first)
Hermes.drop_schema()

# Creating a schema (there must be no schema at first)
Hermes.create_schema()

# Importing a schema (the current schema will be overwritten)
# See file specs ahead in this document
Hermes.import_schema("path_to_schema_spec.terms")

# To create inboxes, just call this function (calls are idempotent)
Hermes.add_inboxes("my-topic", ["inbox1", "inbox2", "inbox3"])

# To remove inboxes, call this (calls are idempotent)
Hermes.drop_inboxes("my-topic", ["inbox5", "inbox7"])

# Get a list of pairs topic/inboxes
Hermes.list_inboxes()
```

#### Subscribing to an Well-known Inbox

After creating the "Pseudo Permanent Inboxes" using the schema
as demonstrated in the examples above, you must provide the handlers using
the complete reference to each inbox, that is to say, you have to provide
the pair `topic_id` / `inbox_id` to the appropriate subscribe function.

_Examples_
```elixir
# You may use a function reference
Hermes.subscribe("my-topic", "my-inbox", &some_function/5)

# An anonymous function
local_func = fn t, i, m -> do_something_else(t, i, m) end
Hermes.subscribe("my-topic", "my-inbox", local_func)

# Or a pair module_name/function_name
Hermes.subscribe("my-topic", "my-inbox", My.Module, :my_function)
```

### Unsubscribing from a topic

In order to unsubscribe from a topic you will ALWAYS need to provide
the full identification of the inbox, the pair `topic_id/inbox_id`.

_Examples_
```elixir

# Suppose you have subscribed to a well-known inbox
Hermes.subscribe("my-topic", "my-inbox", &some_function/3)

# You use the same reference to unsubscribe
Hermes.unsubscribe("my-topic", "my-inbox")

# If yow have used an ad hoc inbox
{t_id, i_id} = Hermes.subscribe("my-topic", &some_function/3)

# You must use the same reference returned from the subscription call
Hermes.unsubscribe(t_id, i_id)
```

### Publishing to a topic

The publishing of messages is quite straightforward as can be seen
in the example below. Note that if there is no handler for the message,
it will be discarded. Only permanent inboxes will retain messages
without handlers. Also you publish to a topic and not to a specific inbox.

_Example_
```elixir

# Publishing to the entire topic
Hermes.publish("my-topic", "some message")
```

### Halting / Resuming Hermes Operation

As soon as it starts, Hermes will enter an operation loop that will
periodically sweep through a list of handlers and trigger a dispatch task
for each of them. This operation loop may be stopped and restarted
if needed.

```elixir

# Halting operation
Hermes.cease_operation()

# Resuming operation
Hermes.commence_operation()
```

### Error Handling

Currently, Hermes does not offer any sort of error recovery feature
such as message delivery retries. It does however guarantee that the
delivery of messages will proceed if a handler fails to perform its duty.

## Configuration

There are two classes of configuration used by hermes:

 - Static: May be resolved at compile time and is supposed to remain
 the same during the entire runtime of the application.

 - Dynamic: May change during the application runtime if needed.

Here is the list of variables in each category:

### Static configuration

Static configuration is done using a set of Elixir config variables:

 - `:schema_bootstrap_file`: An absolute path to a terms file containing
 inbox schema entries to be imported at first boot.
 - `:config_bootstrap_file`: An absolute path to a terms file containing a
 set of variables to be imported at first boot.

### Dynamic configuration

Dynamic configuration is initialized via a terms import file and adjusted
through a set of mix tasks.

**Variables:**

 - `:sleep_interval`: The interval between inbox sweeps in millis.
 - `:dispatch_on_publish`: Configures if and how a sweep will occur
 as a consequence of a publish call.
     - `:single_topic`: Only the current topic will be dispatched.
     - `:all_topics`: Hermes will try to dispatch to all topics
     whenever a message is published.
     - `:offline`: Publishing a message will not cause a dispatch.

**Tasks:**

 - hermes.sleep_interval [ -get | -set value ]
 - hermes.set_dispatch [ -get | -set value ]

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/hermes](https://hexdocs.pm/hermes).
