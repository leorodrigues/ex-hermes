import Config

# config :git_hooks,
#   auto_install: :true,
#   verbose: true,
#   hooks: [
#     pre_push: [
#       tasks: [
#         {:cmd, "mix clean --no-color"},
#         {:cmd, "mix compile --warnings-as-errors --no-color"},
#         {:cmd, "mix dialyzer --no-color"},
#         {:cmd, "mix unit --no-color"}
#       ]
#     ]
#   ]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

base_data_dir = Path.join([System.user_home(), ".hermes"])

config :hermes, :base_dir, base_data_dir

config :hermes, :scrolls_base_data_dir, base_data_dir
 