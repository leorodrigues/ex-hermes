import Config

# Print everything during test
config :logger,
  backends: [:console]

config :hermes, env_provider: ExTestSupport.EnvProviderMock

config :scrolls, env_provider: ExTestSupport.EnvProviderMock

config :ex_test_support, ExTestSupport.EnvProviderMock,
  mock_for_get_env: {Hermes.TestEnvProvider, :get_env}
