import Config

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $node $metadata[$level] $message\n",
  metadata: [
    :node, :module, :pid, :message_type, :message_id,
    :correlation_id, :source_id
  ]

import_config "#{config_env()}.exs"
