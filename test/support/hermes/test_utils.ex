defmodule Hermes.TestUtils do
  def stack(name, elements) do
    table = :ets.new(name, [:set, :public])
    :ets.insert(table, {:stack, elements})
    table
  end

  def pop(table) do
    case :ets.lookup(table, :stack) do
      [{:stack, [ ]}] -> throw {:error, "Stack empty"}
      [{:stack, [head|tail]}] ->
        :ets.insert(table, {:stack, tail})
        head
    end
  end

  def load_samples(context, file_name) do
    with {:ok, contents} <- :file.consult(file_name) do
      Map.put(context, :samples, Enum.into(contents, %{ }))
    end
  end

  def wait_on(payload, module, function, args, timeout) do
    :meck.wait(module, function, args, timeout)
    payload
  end

  def ensure_call_count(payload, count, module, function, args) do
    ^count = :meck.num_calls(module, function, args)
    payload
  end

  def call_hermes(message, topic), do: Hermes.publish(topic, message)

  def submit_command(payload, reply_topic, topic) do
    Hermes.Piping.submit_command(reply_topic, topic, payload)
  end

  def submit_command(payload, topic) do
    Hermes.Piping.submit_void_command(topic, payload)
  end

  def using_sample(map, key) do
    case Map.get(map, key) do
      nil -> throw {:error, {:test_case_requirement, :sample_not_found, key}}
      value -> value
    end
  end

  def for_each_sample(map, prefix, range, function) do
    Enum.map(range, fn i -> "#{prefix}#{i}" end)
      |> Enum.map(fn n -> using_sample(map, n) end)
      |> Enum.each(function)
  end

  def wait_for(payload, timeout) do
    Process.sleep(timeout)
    payload
  end
end
