defmodule Hermes.TestEnvProvider do

  alias ExTestSupport.Utilities

  def get_env(:hermes, :schema_bootstrap_file, _) do
    Utilities.setup_environment(:hermes)
    Utilities.resolve_resource_path(:hermes, ["bootstrap_schema.terms"])
  end

  def get_env(app_name, :scrolls_base_data_dir = key, default) do
    get_mocked_env(app_name, key, default, "scrolls_data")
  end

  def get_env(app_name, :base_dir = key, default) do
    get_mocked_env(app_name, key, default, "hermes_data")
  end

  def get_env(app_name, key, default) do
    Application.get_env(app_name, key, default)
  end

  defp get_mocked_env(app_name, key, default, path) do
    ExTestSupport.Utilities.setup_environment(app_name)
    case ExTestSupport.Utilities.make_session_resources_dir(app_name, [path]) do
      {:error, _} -> Application.get_env(app_name, key, default)
      {_, path} -> path
    end
  end
end
