defmodule Hermes.IntegrationSupport do
  def consume_and_stop_at_message(module, function, stop_message) do
    consume_and_do_action_at(module, function, stop_message, fn t, i, _, _ ->
      Hermes.unsubscribe(t, i)
    end)
  end

  def consume_and_raise_at_message(module, function, stop_message) do
    consume_and_do_action_at(module, function, stop_message, fn _, _, _, _ ->
      raise RuntimeError, "Thrown on purpose"
    end)
  end

  def consume_and_do_action_at(module, function, stop_message, action) do
    fn topic_id, inbox_id, message ->
      case message do
        ^stop_message ->
          action.(topic_id, inbox_id, message, stop_message)
        _ ->
          :timer.sleep(200)
          apply(module, function, [topic_id, inbox_id, message])
      end
    end
  end

  def unsubscribe_all do
    unsubscribe_topics(Hermes.list_inboxes())
  end

  defp unsubscribe_topics([ ]), do: :ok
  defp unsubscribe_topics([{topic, inboxes}|tail]) do
    unsubscribe_inboxes(topic, inboxes)
    unsubscribe_topics(tail)
  end

  defp unsubscribe_inboxes(_, [ ]), do: :ok
  defp unsubscribe_inboxes(topic, [head|tail]) do
    Hermes.unsubscribe(topic, head)
    unsubscribe_inboxes(topic, tail)
  end
end
