defmodule Hermes.Piping.ListenerTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :hermes

  alias Hermes.Dispatching.Coordinator

  alias Hermes.Piping
  alias Hermes.Piping.DummyListener

  alias Hermes.Piping.Test.Tasker

  @modules_to_spy_on [Coordinator, DummyListener]

  setup_all_environment do
    Hermes.set_sleep_interval(900)
    Hermes.set_dispatch_on_publish(:single_topic)
    [schema_bootstrap_file: resolve_resource_path(["bootstrap_schema.terms"])]
  end

  setup context do
    Task.Supervisor.start_link(name: Tasker)

    case context do
      %{schema_bootstrap_file: s} -> bootstrap_test(s)
      _ -> :ok
    end

    context
  end

  describe "Using the generic 'listen' macro," do
    test "the listener should handle opaque payloads w/o options." do
      shutdown_after(fn ->
        :ok = Piping.submit_void_command("test/hermes/piping/listener/opaque-payload", :opaque_payload)
        :meck.wait(DummyListener, :run_test_hermes_piping_listener_opaque_payload, :_, 5000)
      end)
    end

    test "the listener should handle opaque payloads with opaque options." do
      shutdown_after(fn ->
        :ok = Piping.submit_void_command("test/hermes/piping/listener/opaque-options", :opaque_payload)
        :meck.wait(DummyListener, :run_test_hermes_piping_listener_opaque_options, :_, 5000)
      end)
    end

    test "the listener should handle matching payloads w/o options." do
      shutdown_after(fn ->
        :ok = Piping.submit_void_command("test/hermes/piping/listener/matching-payload", %{content: 112358})
        :meck.wait(DummyListener, :run_test_hermes_piping_listener_matching_payload, :_, 5000)
      end)
    end

    test "the listener should handle opaque payloads with matching options." do
      shutdown_after(fn ->
        :ok = Piping.submit_void_command("test/hermes/piping/listener/matching-options", :opaque_payload)
        :meck.wait(DummyListener, :run_test_hermes_piping_listener_matching_options, :_, 5000)
      end)
    end

    test "the listener should handle dynamic topics." do
      shutdown_after(fn ->
        :ok = Piping.submit_void_command(Hermes.Piping.DummyTopics.dynamic, :opaque_payload)
        :meck.wait(DummyListener, :run_test_hermes_piping_listener_dynamic, :_, 5000)
      end)
    end
  end

  defp bootstrap_test(schema_bootstrap_file) do
    on_exit(fn -> :meck.unload() end)

    :meck.new(@modules_to_spy_on, [:passthrough])

    Hermes.drop_schema()
    Hermes.create_schema()
    Hermes.import_schema(schema_bootstrap_file)
    Hermes.commence_operation()

    :meck.wait(Coordinator, :handle_cast, [:commence_operation, :_], 1000)

    DummyListener.start_link()

    [ ]
  end

  defp shutdown_test do
    Hermes.cease_operation()

    :meck.wait(Coordinator, :handle_cast, [:cease_operation, :_], 1000)

    DummyListener.stop()

    Logger.flush()
  end

  defp shutdown_after(test_function) do
    try do
      test_function.()
    after
      shutdown_test()
    end
  end

end
