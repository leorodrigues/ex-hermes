defmodule Hermes.HaltingDispatchTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :hermes

  alias Hermes.Dispatching.Coordinator
  alias Hermes.Dispatching.Dispatcher
  alias Hermes.InboxSchemaServer
  alias Hermes.InboxServer

  import Hermes.IntegrationSupport

  @modules_to_spy_on [InboxServer, InboxSchemaServer, Dispatcher, Coordinator]

  @topic "integration/halt-dispatch-test"

  @inbox "halt-completely"

  setup_all_environment do
    Hermes.cease_operation
    Hermes.set_sleep_interval(1000)
    Hermes.set_dispatch_on_publish(:offline)
    [ ]
  end

  setup do
    bootstrap_test()
  end

  test "Dispatching should halt completely" do
    shutdown_after(fn ->
      :meck.expect(FakeModule, :handle, fn
        _, _, :stop -> Hermes.cease_operation; :ok
        _, _, _ -> Process.sleep(150); :ok
      end)

      Hermes.subscribe(@topic, @inbox, FakeModule, :handle)
      Hermes.publish(@topic, [:a, :b, :stop, :c, :d])

      :meck.wait(FakeModule, :handle, [@topic, @inbox, :a], 5000)
      :meck.wait(FakeModule, :handle, [@topic, @inbox, :b], 5000)
      :meck.wait(FakeModule, :handle, [@topic, @inbox, :stop], 5000)

      0 = :meck.num_calls(FakeModule, :handle, [:_, :_, :c])
      0 = :meck.num_calls(FakeModule, :handle, [:_, :_, :d])
    end)
  end

  defp bootstrap_test() do
    on_exit(fn -> :meck.unload() end)

    :meck.new(FakeModule, [:non_strict])
    :meck.new(@modules_to_spy_on, [:passthrough])

    Hermes.drop_schema()
    Hermes.create_schema()
    Hermes.commence_operation()

    :meck.wait(Coordinator, :handle_cast, [:commence_operation, :_], 1000)

    [ ]
  end

  defp shutdown_test do
    unsubscribe_all()

    Hermes.cease_operation()

    :meck.wait(Coordinator, :handle_cast, [:cease_operation, :_], 1000)

    Logger.flush()
  end

  defp shutdown_after(test_function) do
    try do
      test_function.()
    after
      shutdown_test()
    end
  end
end
