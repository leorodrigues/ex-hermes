defmodule Hermes.DispatchOnPublishTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :hermes

  alias Hermes.Dispatching.Consumption
  alias Hermes.Dispatching.Coordinator
  alias Hermes.Dispatching.Dispatcher
  alias Hermes.InboxSchemaServer
  alias Hermes.InboxServer

  import Enum, only: [reverse: 1, join: 2]

  @modules_to_spy_on [
    InboxServer, InboxSchemaServer, Dispatcher, Coordinator, Consumption
  ]

  @topic_templ ["at-line", "case", "dispatch-on-publish-test", "topics"]

  setup_all_environment do
    Hermes.cease_operation()
    Process.sleep(Coordinator.default_sleep_interval + 100)
    Hermes.set_sleep_interval(60000)
    Hermes.set_dispatch_on_publish(:offline)
    [ ]
  end

  setup ctx do
    case ctx do
      %{dispatch_on_publish: s} -> bootstrap_test(s)
      _ -> :ok
    end

    Map.put(ctx, :topic, join(reverse([ctx[:line]|@topic_templ]), "/"))
  end

  describe "Running Hermes with dispatch single topic on publish," do
    @tag dispatch_on_publish: :single_topic
    test "should trigger consumption but avoid dispatch of only one topic", %{topic: topic} do
      shutdown_after(fn ->
        Hermes.cease_operation

        topic_a = "#{topic}/a"
        topic_b = "#{topic}/b"

        :meck.expect(FakeModule, :handle, fn _, _, _ -> :ok end)

        Hermes.subscribe(topic_a, :inbox, FakeModule, :handle)
        Hermes.subscribe(topic_b, :inbox, FakeModule, :handle)
        Hermes.publish(topic_a, :message)

        :meck.wait(Consumption, :run, [topic_a, :inbox, :_], 1200)

        Process.sleep(1800)

        0 = :meck.num_calls(FakeModule, :handle, :_)
        0 = :meck.num_calls(Consumption, :run, [topic_b, :inbox, :_])
      end)
    end

    @tag dispatch_on_publish: :single_topic
    test "should trigger consumption of only one topic", %{topic: topic} do
      shutdown_after(fn ->
        topic_a = "#{topic}/a"
        topic_b = "#{topic}/b"

        :meck.expect(FakeModule, :handle, fn _, _, _ -> :ok end)

        Hermes.subscribe(topic_a, :inbox, FakeModule, :handle)
        Hermes.subscribe(topic_b, :inbox, FakeModule, :handle)
        Hermes.publish(topic_a, :message)

        :meck.wait(FakeModule, :handle, [topic_a, :inbox, :message], 1200)

        :meck.wait(Consumption, :run, [topic_a, :inbox, :_], 1200)

        Process.sleep(1800)

        0 = :meck.num_calls(Consumption, :run, [topic_b, :inbox, :_])
      end)
    end

    @tag dispatch_on_publish: :all_topics
    test "should trigger consumption of two topics but skip dispatch", %{topic: topic} do
      shutdown_after(fn ->
        Coordinator.cease_operation

        topic_a = "#{topic}/a"
        topic_b = "#{topic}/b"

        :meck.expect(FakeModule, :handle, fn _, _, _ -> :ok end)

        Hermes.subscribe(topic_a, :inbox, FakeModule, :handle)
        Hermes.subscribe(topic_b, :inbox, FakeModule, :handle)
        Hermes.publish(topic_a, :message)

        :meck.wait(Consumption, :run, [topic_a, :inbox, :_], 1200)
        :meck.wait(Consumption, :run, [topic_b, :inbox, :_], 1200)

        0 = :meck.num_calls(FakeModule, :handle, :_)
      end)
    end

    @tag dispatch_on_publish: :all_topics
    test "should trigger consumption of two topics", %{topic: topic} do
      shutdown_after(fn ->
        topic_a = "#{topic}/a"
        topic_b = "#{topic}/b"

        :meck.expect(FakeModule, :handle, fn _, _, _ -> :ok end)

        Hermes.subscribe(topic_a, :inbox, FakeModule, :handle)
        Hermes.subscribe(topic_b, :inbox, FakeModule, :handle)
        Hermes.publish(topic_a, :message)

        :meck.wait(FakeModule, :handle, [topic_a, :inbox, :message], 1200)

        :meck.wait(Consumption, :run, [topic_a, :inbox, :_], 1200)
        :meck.wait(Consumption, :run, [topic_b, :inbox, :_], 1200)
      end)
    end

  end

  defp bootstrap_test(dispatch_on_publish) do
    on_exit(fn -> :meck.unload() end)

    :meck.new(FakeModule, [:non_strict])
    :meck.new(@modules_to_spy_on, [:passthrough])

    Hermes.drop_schema()
    Hermes.create_schema()
    Hermes.set_dispatch_on_publish(dispatch_on_publish)
    Hermes.commence_operation()

    :meck.wait(Coordinator, :handle_cast, [:commence_operation, :_], 1000)

    [ ]
  end

  defp shutdown_test do
    Hermes.cease_operation()

    :meck.wait(Coordinator, :handle_cast, [:cease_operation, :_], 1000)

    Logger.flush()
  end

  defp shutdown_after(test_function) do
    try do
      test_function.()
    after
      shutdown_test()
    end
  end
end
