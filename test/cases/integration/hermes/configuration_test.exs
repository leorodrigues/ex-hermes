defmodule Hermes.ConfigurationTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :hermes

  alias Hermes.Configuration
  alias Hermes.Dispatching.Coordinator

  setup_all_environment do
    Hermes.cease_operation()
    Process.sleep(Coordinator.default_sleep_interval + 100)
    [ ]
  end

  describe "Hermes.Configuration," do
    test "set ang get values" do
      :no_value = Configuration.get(Fake.Server, :some_key, :no_value)

      :my_value = Configuration.set(Fake.Server, :some_key, :my_value)

      :my_value = Configuration.get(Fake.Server, :some_key, :another_value)
    end
  end
end
