defmodule Hermes.MainIntegrationTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :hermes

  alias Hermes.Dispatching.Coordinator
  alias Hermes.Dispatching.Dispatcher
  alias Hermes.InboxSchemaServer
  alias Hermes.InboxServer

  import Hermes.IntegrationSupport
  import Enum, only: [reverse: 1, join: 2]

  @modules_to_mock [InboxServer, InboxSchemaServer, Dispatcher, Coordinator]

  @topic "events/something-happened"

  @adhoc_inbox "test-consumer-adhoc-inbox"

  @pseudo_permanent_inbox "test-consumer-pseudo-permanent-inbox"

  @topic_templ ["at-line", "case", "main-integration-test", "topics"]

  setup_all_environment do
    Hermes.set_sleep_interval(900)
    Hermes.set_dispatch_on_publish(:offline)
    [schema_bootstrap_file: resolve_resource_path(["bootstrap_schema.terms"])]
  end

  setup ctx do
    case ctx do
      %{schema_bootstrap_file: s} -> bootstrap_test(s)
      _ -> :ok
    end

    Map.put(ctx, :topic, join(reverse([ctx[:line]|@topic_templ]), "/"))
  end

  describe "Using pseudo permanent inboxing," do

    @tag schema_name: "group1-case1-schema.bin"
    test "Hermes should publish/dispatch to an anonymous function", %{topic: topic} do
      shutdown_after(fn ->
        Hermes.add_inboxes(topic, [:inbox])

        :meck.expect(FakeModule, :handle, fn _, _, _ -> :ok end)

        invocation = fn topic_id, inbox_id, message ->
          apply(FakeModule, :handle, [topic_id, inbox_id, message])
        end

        Hermes.subscribe(topic, :inbox, invocation)
        Hermes.publish(topic, :test_message_a)

        :meck.wait(FakeModule, :handle, [topic, :inbox, :test_message_a], 5000)

        Hermes.unsubscribe(topic, :inbox)
        Hermes.drop_inboxes(topic, [:inbox])
      end)
    end

    @tag schema_name: "group1-case2-schema.bin"
    test "Hermes should publish/dispatch to a function reference", %{topic: topic} do
      shutdown_after(fn ->
        Hermes.add_inboxes(topic, [:inbox])

        :meck.expect(FakeModule, :handle, fn _, _, _ -> :ok end)

        Hermes.subscribe(topic, :inbox, &local_invocation/3)
        Hermes.publish(topic, :test_message_b)

        :meck.wait(FakeModule, :handle, [topic, :inbox, :test_message_b], 5000)

        Hermes.unsubscribe(topic, :inbox)
        Hermes.drop_inboxes(topic, [:inbox])
      end)
    end

    @tag schema_name: "group1-case3-schema.bin"
    test "Hermes should publish/dispatch to an MF", %{topic: topic} do
      shutdown_after(fn ->
        Hermes.add_inboxes(topic, [:inbox])
        :meck.expect(FakeModule, :handle, fn _, _, _ -> :ok end)

        Hermes.subscribe(topic, :inbox, FakeModule, :handle)
        Hermes.publish(topic, :test_message_c)

        Process.sleep(1500)

        :meck.wait(FakeModule, :handle, [topic, :inbox, :test_message_c], 5000)

        Hermes.unsubscribe(topic, :inbox)
        Hermes.drop_inboxes(topic, [:inbox])
      end)
    end

    @tag schema_name: "group1-case4-schema.bin"
    test "Unsubscription should keep the inbox but dispatch should halt", %{topic: topic} do
      shutdown_after(fn ->
        Hermes.add_inboxes(topic, [:inbox])
        :meck.expect(FakeModule, :handle, fn _, _, _ -> :ok end)

        invocation = consume_and_stop_at_message(FakeModule, :handle, :stop)

        Hermes.subscribe(topic, :inbox, invocation)
        Hermes.publish(topic, [:a, :b, :stop, :c, :d])

        :meck.wait(FakeModule, :handle, [topic, :inbox, :a], 5000)

        :meck.wait(FakeModule, :handle, [topic, :inbox, :b], 5000)

        :timer.sleep(1500)

        0 = :meck.num_calls(FakeModule, :handle, [topic, :inbox, :c])

        0 = :meck.num_calls(FakeModule, :handle, [topic, :inbox, :d])

        Hermes.subscribe(topic, :inbox, invocation)

        :meck.wait(FakeModule, :handle, [topic, :inbox, :c], 5000)

        :meck.wait(FakeModule, :handle, [topic, :inbox, :d], 5000)

        Hermes.unsubscribe(topic, :inbox)
        Hermes.drop_inboxes(topic, [:inbox])
      end)
    end

    @tag schema_name: "group1-case5-schema.bin"
    test "The bootstrap mechanics should work properly" do
      [
        {"events/something-happened", [
          "test-consumer-pseudo-permanent-inbox"
        ]},
        {"topics/events/fake-event", [
          "fake-application-1", "fake-application-2"
        ]}
      ] = Hermes.list_inboxes()
    end
  end

  describe "Using ephemeral inboxing," do

    @tag schema_name: "group2-case1-schema.bin"
    test "Hermes should publish/dispatch to some anonymous function", %{topic: topic} do
      shutdown_after(fn ->
        :meck.expect(FakeModule, :handle, fn _, _, _ -> :ok end)

        invocation = fn topic_id, inbox_id, message ->
          apply(FakeModule, :handle, [topic_id, inbox_id, message])
        end

        {topic, inbox_id} = Hermes.subscribe(topic, invocation)
        Hermes.publish(topic, :test_message_d)

        :meck.wait(FakeModule, :handle, [topic, inbox_id, :test_message_d], 5000)

        Hermes.unsubscribe(topic, inbox_id)
      end)
    end

    @tag schema_name: "group2-case2-schema.bin"
    test "Hermes should publish/dispatch to some function reference", %{topic: topic} do
      shutdown_after(fn ->
        :meck.expect(FakeModule, :handle, fn _, _, _ -> :ok end)

        {topic, inbox_id} = Hermes.subscribe(topic, &local_invocation/3)
        Hermes.publish(topic, :test_message_e)

        :meck.wait(FakeModule, :handle, [topic, inbox_id, :test_message_e], 5000)

        Hermes.unsubscribe(topic, inbox_id)
      end)
    end

    @tag schema_name: "group2-case3-schema.bin"
    test "Hermes should publish/dispatch to some MF", %{topic: topic} do
      shutdown_after(fn ->
        :meck.expect(FakeModule, :handle, fn _, _, _ -> :ok end)

        {topic, inbox_id} = Hermes.subscribe(topic, FakeModule, :handle)
        Hermes.publish(topic, :test_message_f)

        :meck.wait(FakeModule, :handle, [topic, inbox_id, :test_message_f], 5000)

        Hermes.unsubscribe(topic, inbox_id)
      end)
    end

    @tag schema_name: "group2-case4-schema.bin"
    test "Unsubscription should drop the inbox and dispatch should halt", %{topic: topic} do
      shutdown_after(fn ->
        :meck.expect(FakeModule, :handle, fn _, _, _ -> :ok end)

        invocation = consume_and_stop_at_message(FakeModule, :handle, :z)

        Hermes.subscribe(topic, @adhoc_inbox, invocation)
        Hermes.publish(topic, [:x, :y, :z, :w])

        :meck.wait(FakeModule, :handle, [topic, @adhoc_inbox, :x], 5000)

        :meck.wait(FakeModule, :handle, [topic, @adhoc_inbox, :y], 5000)

        :timer.sleep(1500)

        0 = :meck.num_calls(FakeModule, :handle, [topic, @adhoc_inbox, :z])

        0 = :meck.num_calls(FakeModule, :handle, [topic, @adhoc_inbox, :w])

        Hermes.unsubscribe(topic, @adhoc_inbox)
      end)
    end
  end

  describe "When a message handler fails by raising an error," do

    @tag schema_name: "group3-case1-schema.bin"
    test "Processing of the remaining messages should occur", %{topic: topic} do
      shutdown_after(fn ->
        :meck.expect(FakeModule, :handle, fn _, _, _ -> :ok end)

        invocation = consume_and_raise_at_message(FakeModule, :handle, :m2)

        Hermes.subscribe(topic, @adhoc_inbox, invocation)
        Hermes.publish(topic, [:m1, :m2, :m3])

        :meck.wait(FakeModule, :handle, [topic, @adhoc_inbox, :m1], 5000)

        :meck.wait(FakeModule, :handle, [topic, @adhoc_inbox, :m3], 5000)

        :timer.sleep(1500)

        0 = :meck.num_calls(FakeModule, :handle, [topic, @adhoc_inbox, :m2])

        Hermes.unsubscribe(topic, @adhoc_inbox)
      end)
    end
  end

  describe "When the inbox becomes permanent after a subscription," do

    @tag schema_name: "group4-case1-schema.bin"
    test "The inbox should behave exactly like a permanent inbox.", %{topic: topic} do
      shutdown_after(fn ->
        local_inbox = "my-new-inbox"

        :meck.expect(FakeModule, :handle, fn _, _, _ -> :ok end)

        invocation = consume_and_stop_at_message(FakeModule, :handle, :stop)

        Hermes.subscribe(topic, local_inbox, invocation)

        Hermes.add_inboxes(topic, [local_inbox])

        Hermes.publish(topic, [:m1_b, :m2_b, :stop, :m3_b, :m4_b])

        :meck.wait(FakeModule, :handle, [topic, local_inbox, :m1_b], 5000)

        :meck.wait(FakeModule, :handle, [topic, local_inbox, :m2_b], 5000)

        :timer.sleep(1500)

        0 = :meck.num_calls(FakeModule, :handle, [topic, local_inbox, :m3_b])

        0 = :meck.num_calls(FakeModule, :handle, [topic, local_inbox, :m4_b])

        Hermes.subscribe(topic, local_inbox, invocation)

        :meck.wait(FakeModule, :handle, [topic, local_inbox, :m3_b], 5000)

        :meck.wait(FakeModule, :handle, [topic, local_inbox, :m4_b], 5000)

        Hermes.unsubscribe(topic, local_inbox)
      end)
    end
  end

  defp local_invocation(topic_id, inbox_id, message) do
    apply(FakeModule, :handle, [topic_id, inbox_id, message])
  end

  defp bootstrap_test(schema_bootstrap_file) do
    on_exit(fn -> :meck.unload() end)

    :meck.new(FakeModule, [:non_strict])
    :meck.new(@modules_to_mock, [:passthrough])

    Hermes.drop_schema()
    Hermes.create_schema()
    Hermes.import_schema(schema_bootstrap_file)
    Hermes.add_inboxes(@topic, [@pseudo_permanent_inbox])
    Hermes.commence_operation()

    :meck.wait(Coordinator, :handle_cast, [:commence_operation, :_], 1000)

    [ ]
  end

  defp shutdown_test do
    Hermes.cease_operation()

    :meck.wait(Coordinator, :handle_cast, [:cease_operation, :_], 1000)

    Logger.flush()
  end

  defp shutdown_after(test_function) do
    try do
      test_function.()
    after
      shutdown_test()
    end
  end
end
