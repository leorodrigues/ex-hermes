defmodule Hermes.TopicHandlerTest do
  use ExUnit.Case

  alias Hermes.Dispatching.DispatchSignaling
  alias Hermes.TopicHandler
  alias Hermes.Topic

  @mods [
    Topic,
    Hermes.InboxServer,
    Hermes.InboxSchemaServer,
    DispatchSignaling
  ]

  setup do
    :meck.new(@mods)
    on_exit(fn -> :meck.unload() end)
  end

  describe "Hermes.TopicHandler" do
    test "create_inboxes; ok; create inboxes and register casts" do
      :meck.expect(Topic, :register_server_cast, fn _, _, _ -> :ok end)
      :meck.expect(Hermes.InboxServer, :self, fn -> :fake_server_name end)
      :meck.expect(Hermes.InboxSchemaServer, :add_inboxes, fn _, _ -> :ok end)
      :meck.expect(DispatchSignaling, :register_inbox, fn _, _ -> :ok end)

      TopicHandler.create_inboxes("test-topic", ["q1", "q2"])

      :meck.wait(Topic, :register_server_cast, [
        "test-topic", :fake_server_name, {:put, "test-topic", "q1"}
      ], 1000)

      :meck.wait(Topic, :register_server_cast, [
        "test-topic", :fake_server_name, {:put, "test-topic", "q2"}
      ], 1000)

      :meck.wait(Hermes.InboxSchemaServer, :add_inboxes, [
        "test-topic", ["q1", "q2"]
      ], 1000)

      :meck.wait(DispatchSignaling, :register_inbox,
        ["test-topic", "q1"], 1000)

      :meck.wait(DispatchSignaling, :register_inbox,
        ["test-topic", "q2"], 1000)
    end

    test "drop_inboxes; ok; unregister everything" do
      :meck.expect(Topic, :unregister_server_cast, fn _, _, _ -> :ok end)
      :meck.expect(Hermes.InboxServer, :self, fn -> :fake_server_name end)
      :meck.expect(Hermes.InboxSchemaServer, :drop_inboxes, fn _, _ -> :ok end)
      :meck.expect(DispatchSignaling, :unregister_inbox, fn _, _ -> :ok end)

      TopicHandler.drop_inboxes(:topic, [:inbox1, :inbox2])

      :meck.wait(Topic, :unregister_server_cast, [
        :topic, :fake_server_name, {:put, :topic, :inbox1}
      ], 1000)

      :meck.wait(Topic, :unregister_server_cast, [
        :topic, :fake_server_name, {:put, :topic, :inbox2}
      ], 1000)

      :meck.wait(Hermes.InboxSchemaServer, :drop_inboxes, [
        :topic, [:inbox1, :inbox2]
      ], 1000)

      :meck.wait(DispatchSignaling, :unregister_inbox,
        [:topic, :inbox1], 1000)

      :meck.wait(DispatchSignaling, :unregister_inbox,
        [:topic, :inbox2], 1000)
    end

    test "restore_inboxes; ok; register casts" do
      :meck.expect(Topic, :register_server_cast, fn _, _, _ -> :ok end)
      :meck.expect(Hermes.InboxServer, :self, fn -> :fake_server_name end)
      :meck.expect(DispatchSignaling, :register_inbox, fn _, _ -> :ok end)

      TopicHandler.restore_inboxes("test-topic", ["q1", "q2"])

      :meck.wait(Topic, :register_server_cast, [
        "test-topic", :fake_server_name, {:put, "test-topic", "q1"}
      ], 1000)

      :meck.wait(Topic, :register_server_cast, [
        "test-topic", :fake_server_name, {:put, "test-topic", "q2"}
      ], 1000)

      :meck.wait(DispatchSignaling, :register_inbox,
        ["test-topic", "q1"], 1000)

      :meck.wait(DispatchSignaling, :register_inbox,
        ["test-topic", "q2"], 1000)
    end

    test "broadcast; ok; wrap message and delegate" do
      :meck.expect(Topic, :broadcast, fn _, _ -> :ok end)

      TopicHandler.broadcast("test-topic", :test_message)

      :meck.wait(Topic, :broadcast, ["test-topic", :test_message], 1000)
    end

    test "subscribe; should register the cast and the function handler" do
      :meck.expect(Topic, :register_server_cast, fn _, _, _ -> :ok end)
      :meck.expect(Hermes.InboxServer, :self, fn -> :fake_server_name end)
      :meck.expect(Hermes.Dispatching.Dispatcher, :set_handler, fn _, _, _ -> :ok end)
      :meck.expect(Hermes.InboxSchemaServer, :inbox_exists, fn _, _ -> false end)
      :meck.expect(DispatchSignaling, :register_inbox, fn _, _ -> :ok end)

      TopicHandler.subscribe(:topic, :inbox1, :fake_function)

      :meck.wait(Hermes.InboxSchemaServer, :inbox_exists, [:topic, :inbox1], 1000)

      :meck.wait(Topic, :register_server_cast, [
        :topic, :fake_server_name, {:put, :topic, :inbox1}
      ], 1000)

      :meck.wait(Hermes.Dispatching.Dispatcher, :set_handler, [
        :topic, :inbox1, :fake_function
      ], 1000)

      :meck.wait(DispatchSignaling, :register_inbox,
        [:topic, :inbox1], 1000)
    end

    test "subscribe; should register the cast and the MF handler" do
      :meck.expect(Topic, :register_server_cast, fn _, _, _ -> :ok end)
      :meck.expect(Hermes.InboxServer, :self, fn -> :fake_server_name end)
      :meck.expect(Hermes.Dispatching.Dispatcher, :set_handler, fn _, _, _, _ -> :ok end)
      :meck.expect(Hermes.InboxSchemaServer, :inbox_exists, fn _, _ -> false end)
      :meck.expect(DispatchSignaling, :register_inbox, fn _, _ -> :ok end)

      TopicHandler.subscribe(:topic, :inbox1, My.Module, :my_func)

      :meck.wait(Hermes.InboxSchemaServer, :inbox_exists, [:topic, :inbox1], 1000)

      :meck.wait(Topic, :register_server_cast, [
        :topic, :fake_server_name, {:put, :topic, :inbox1}
      ], 1000)

      :meck.wait(Hermes.Dispatching.Dispatcher, :set_handler, [
        :topic, :inbox1, My.Module, :my_func
      ], 1000)

      :meck.wait(DispatchSignaling, :register_inbox,
        [:topic, :inbox1], 1000)
    end

    test "subscribe; should register just the function handler" do
      :meck.expect(Hermes.Dispatching.Dispatcher, :set_handler, fn _, _, _ -> :ok end)
      :meck.expect(Hermes.InboxSchemaServer, :inbox_exists, fn _, _ -> true end)
      :meck.expect(DispatchSignaling, :register_inbox, fn _, _ -> :ok end)

      TopicHandler.subscribe(:topic, :inbox1, :fake_function)

      :meck.wait(Hermes.InboxSchemaServer, :inbox_exists, [:topic, :inbox1], 1000)

      :meck.wait(Hermes.Dispatching.Dispatcher, :set_handler, [
        :topic, :inbox1, :fake_function
      ], 1000)

      :meck.wait(DispatchSignaling, :register_inbox,
        [:topic, :inbox1], 1000)
    end

    test "subscribe; should register just the MF handler" do
      :meck.expect(Hermes.Dispatching.Dispatcher, :set_handler, fn _, _, _, _ -> :ok end)
      :meck.expect(Hermes.InboxSchemaServer, :inbox_exists, fn _, _ -> true end)
      :meck.expect(DispatchSignaling, :register_inbox, fn _, _ -> :ok end)

      TopicHandler.subscribe(:topic, :inbox1, My.Module, :my_func)

      :meck.wait(Hermes.InboxSchemaServer, :inbox_exists, [:topic, :inbox1], 1000)

      :meck.wait(Hermes.Dispatching.Dispatcher, :set_handler, [
        :topic, :inbox1, My.Module, :my_func
      ], 1000)

      :meck.wait(DispatchSignaling, :register_inbox,
        [:topic, :inbox1], 1000)
    end

    test "unsubscribe; should remove both the handler and the server cast" do
      :meck.expect(Topic, :unregister_server_cast, fn _, _, _ -> :ok end)
      :meck.expect(Hermes.InboxServer, :self, fn -> :fake_server_name end)
      :meck.expect(Hermes.InboxServer, :drop, fn _, _ -> :ok end)
      :meck.expect(Hermes.InboxServer, :put, fn _, _, _, _ -> :ok end)
      :meck.expect(DispatchSignaling, :unregister_inbox, fn _, _ -> :ok end)
      :meck.expect(DispatchSignaling, :signal_halt, fn _, _ -> :ok end)

      :meck.expect(Hermes.Dispatching.Dispatcher, :clear_handler, fn _, _ -> :ok end)
      :meck.expect(Hermes.InboxSchemaServer, :inbox_exists, fn _, _ -> false end)

      TopicHandler.unsubscribe(:topic, :inbox1)

      :meck.wait(Hermes.InboxSchemaServer, :inbox_exists,
        [:topic, :inbox1], 1000)

      :meck.wait(Hermes.InboxServer, :drop,
        [:topic, :inbox1], 1000)

      :meck.wait(DispatchSignaling, :signal_halt,
        [:topic, :inbox1], 1000)

      :meck.wait(DispatchSignaling, :unregister_inbox,
        [:topic, :inbox1], 1000)

      :meck.wait(Topic, :unregister_server_cast,
        [:topic, :fake_server_name, {:put, :topic, :inbox1}], 1000)

      :meck.wait(Hermes.Dispatching.Dispatcher, :clear_handler,
        [:topic, :inbox1], 1000)
    end

    test "unsubscribe; should remove just the handler" do
      :meck.expect(Hermes.Dispatching.Dispatcher, :clear_handler, fn _, _ -> :ok end)
      :meck.expect(Hermes.InboxSchemaServer, :inbox_exists, fn _, _ -> true end)
      :meck.expect(Hermes.InboxServer, :put, fn _, _, _, _ -> :ok end)
      :meck.expect(DispatchSignaling, :unregister_inbox, fn _, _ -> :ok end)
      :meck.expect(DispatchSignaling, :signal_halt, fn _, _ -> :ok end)

      TopicHandler.unsubscribe(:topic, :inbox1)

      :meck.wait(DispatchSignaling, :signal_halt,
        [:topic, :inbox1], 1000)

      :meck.wait(DispatchSignaling, :unregister_inbox,
        [:topic, :inbox1], 1000)

      :meck.wait(Hermes.InboxSchemaServer, :inbox_exists,
        [:topic, :inbox1], 1000)

      :meck.wait(Hermes.Dispatching.Dispatcher, :clear_handler,
        [:topic, :inbox1], 1000)
    end
  end
end
