defmodule Hermes.Dispatching.DispatcherTest do
  use ExUnit.Case

  alias Hermes.Dispatching.Dispatcher
  alias Hermes.Dispatching.Consumption
  alias Hermes.Dispatching.Coordinator

  setup_all do
    Coordinator.cease_operation()
    Process.sleep(Coordinator.default_sleep_interval + 100)
  end

  setup do
    :meck.new(Consumption)
    :meck.new(Dispatcher, [:passthrough])
    on_exit(fn -> :meck.unload end)
  end

  describe "Hermes.Dispatching.Dispatcher.dispatch" do
    test "should dispatch to MFA handler until inbox goes empty" do
      invocation = fn _, _, _ -> {:inbox_empty, :topic, :inbox} end
      :meck.expect(Consumption, :run, invocation)

      :ok = Dispatcher.set_handler(:topic, :inbox, Mockery.SimplePrinter, :print)
      :ok = Dispatcher.dispatch

      arguments = [:topic, :inbox, {Mockery.SimplePrinter, :print}]
      :meck.wait(Consumption, :run, arguments, 1000)
      :meck.wait(Dispatcher, :handle_info, [{:_, {:inbox_empty, :topic, :inbox}}, :_], 1000)

      Dispatcher.clear_handler(:topic, :inbox)
    end

    test "should dispatch to lambda handler util inbox goes empty" do
      :meck.expect(Consumption, :run, fn _, _, _ -> {:inbox_empty, :topic, :inbox} end)

      f = fn -> :ok end
      :ok = Dispatcher.set_handler(:topic, :inbox, f)
      :ok = Dispatcher.dispatch

      :meck.wait(Consumption, :run, [:topic, :inbox, f], 1000)
      :meck.wait(Dispatcher, :handle_info, [{:_, {:inbox_empty, :topic, :inbox}}, :_], 1000)

      Dispatcher.clear_handler(:topic, :inbox)
    end

    test "should dispatch to three lambda handlers until inbox goes empty" do
      :meck.expect(Consumption, :run, fn t, i, _ -> {:inbox_empty, t, i} end)

      f = fn -> :ok end
      :ok = Dispatcher.set_handler(:topic_a, :inbox_a, f)
      :ok = Dispatcher.set_handler(:topic_a, :inbox_b, f)
      :ok = Dispatcher.set_handler(:topic_b, :inbox_c, f)
      :ok = Dispatcher.dispatch

      :meck.wait(Consumption, :run, [:topic_a, :inbox_a, f], 1000)
      :meck.wait(Consumption, :run, [:topic_a, :inbox_b, f], 1000)
      :meck.wait(Consumption, :run, [:topic_b, :inbox_c, f], 1000)
      :meck.wait(Dispatcher, :handle_info, [{:_, {:inbox_empty, :topic_a, :inbox_a}}, :_], 1000)
      :meck.wait(Dispatcher, :handle_info, [{:_, {:inbox_empty, :topic_a, :inbox_b}}, :_], 1000)
      :meck.wait(Dispatcher, :handle_info, [{:_, {:inbox_empty, :topic_b, :inbox_c}}, :_], 1000)

      :ok = Dispatcher.clear_handler(:topic_a, :inbox_a)
      :ok = Dispatcher.clear_handler(:topic_a, :inbox_b)
      :ok = Dispatcher.clear_handler(:topic_b, :inbox_c)
    end

    test "should dispatch to only one lambda handler emptying its inbox" do
      :meck.expect(Consumption, :run, fn t, i, _ -> {:inbox_empty, t, i} end)

      f = fn -> :ok end
      :ok = Dispatcher.set_handler(:topic_a, :inbox_a, f)
      :ok = Dispatcher.set_handler(:topic_a, :inbox_b, f)
      :ok = Dispatcher.set_handler(:topic_b, :inbox_c, f)
      :ok = Dispatcher.dispatch(:topic_b)

      :meck.wait(Consumption, :run, [:topic_b, :inbox_c, f], 1000)
      :meck.wait(Dispatcher, :handle_info, [{:_, {:inbox_empty, :topic_b, :inbox_c}}, :_], 1000)
      :meck.wait(0, Dispatcher, :handle_info, [{:_, {:_, :topic_a, :_}}, :_], 1000)

      :ok = Dispatcher.clear_handler(:topic_a, :inbox_a)
      :ok = Dispatcher.clear_handler(:topic_a, :inbox_b)
      :ok = Dispatcher.clear_handler(:topic_b, :inbox_c)
    end

    test "should dispatch even if the inbox does not exist" do
      :meck.expect(Consumption, :run, fn _, _, _ -> {:inbox_empty, :topic, :inbox} end)

      f = fn -> :ok end
      :ok = Dispatcher.set_handler(:topic, :inbox, f)
      :ok = Dispatcher.dispatch

      :meck.wait(Consumption, :run, [:topic, :inbox, f], 1000)
      :meck.wait(Dispatcher, :handle_info, [{:_, {:inbox_empty, :topic, :inbox}}, :_], 1000)

      :ok = Dispatcher.clear_handler(:topic, :inbox)
    end

    test "should dispatch until commanded to stop" do
      :meck.expect(Consumption, :run, fn _, _, _ -> {:interrupted, :topic, :inbox} end)

      f = fn -> :ok end
      :ok = Dispatcher.set_handler(:topic, :inbox, f)
      :ok = Dispatcher.dispatch

      :meck.wait(Consumption, :run, [:topic, :inbox, f], 1000)
      :meck.wait(Dispatcher, :handle_info, [{:_, {:interrupted, :topic, :inbox}}, :_], 1000)

      :ok = Dispatcher.clear_handler(:topic, :inbox)
    end

    test "should dispatch but the system is not operating" do
      :meck.expect(Consumption, :run, fn _, _, _ -> {:not_alive, :topic, :inbox} end)

      f = fn -> :ok end
      :ok = Dispatcher.set_handler(:topic, :inbox, f)
      :ok = Dispatcher.dispatch

      :meck.wait(Consumption, :run, [:topic, :inbox, f], 1000)
      :meck.wait(Dispatcher, :handle_info, [{:_, {:not_alive, :topic, :inbox}}, :_], 1000)

      :ok = Dispatcher.clear_handler(:topic, :inbox)
    end

    test "should keep dispatching after an error" do
      table = :ets.new(:fake_inbox, [:duplicate_bag, :public])
      :ets.insert(table, [{1, :throw}, {1, :end}])
      next_result_fn = fn t, d, _ -> take_result_from_ets(table, t, d) end

      :meck.expect(Consumption, :run, next_result_fn)

      f = fn -> :ok end
      :ok = Dispatcher.set_handler(:topic, :inbox, f)
      :ok = Dispatcher.dispatch

      :meck.wait(Consumption, :run, [:topic, :inbox, f], 1000)
      :meck.wait(Dispatcher, :handle_info, [{:_, {:inbox_empty, :topic, :inbox}}, :_], 1000)
      :meck.wait(Dispatcher, :handle_info, [{:DOWN, :_, :process, :_, :_}, :_], 1000)

      :ok = Dispatcher.clear_handler(:topic, :inbox)
    end
  end

  defp take_result_from_ets(table, topic_id, inbox_id) do
    case :ets.lookup(table, 1) do
      [{1, :throw} = entry|_remaining] ->
        :ets.delete_object(table, entry)
        throw {:error, :thrown_on_purpose}
      [{1, :end} = entry|_remaining] ->
        :ets.delete_object(table, entry)
        {:inbox_empty, topic_id, inbox_id}
    end
  end
end
