defmodule Hermes.Dispatching.DispatchSignalingTest do
  use ExUnit.Case

  alias Hermes.Dispatching.Coordinator
  alias Hermes.Dispatching.DispatchSignaling

  setup_all do
    Coordinator.cease_operation()
  end

  describe "Hermes.Dispatching.DispatchSignaling" do
    test "should return no signal" do
      :no_signal = DispatchSignaling.take("not-a-topic", "not-an-inbox")
    end

    test "should enqueue signals" do
      :ok = DispatchSignaling.register_inbox("topic-x", "inbox-x")
      :ok = DispatchSignaling.signal_halt()
      :ok = DispatchSignaling.signal_halt()
      :ok = DispatchSignaling.signal_halt()

      :stop_consuming = DispatchSignaling.take("topic-x", "inbox-x")
      :stop_consuming = DispatchSignaling.take("topic-x", "inbox-x")
      :stop_consuming = DispatchSignaling.take("topic-x", "inbox-x")
      :no_signal = DispatchSignaling.take("topic-x", "inbox-x")
    end

    test "should register and signal all" do
      :ok = DispatchSignaling.register_inbox("topic-a", "inbox-a")
      :ok = DispatchSignaling.register_inbox("topic-a", "inbox-b")
      :ok = DispatchSignaling.register_inbox("topic-b", "inbox-c")

      :ok = DispatchSignaling.signal_halt()

      :stop_consuming = DispatchSignaling.take("topic-a", "inbox-a")
      :stop_consuming = DispatchSignaling.take("topic-a", "inbox-b")
      :stop_consuming = DispatchSignaling.take("topic-b", "inbox-c")

      :ok = DispatchSignaling.unregister_inbox("topic-a", "inbox-a")
      :ok = DispatchSignaling.unregister_inbox("topic-a", "inbox-b")
      :ok = DispatchSignaling.unregister_inbox("topic-b", "inbox-c")
    end

    test "should register and signal only one" do
      :ok = DispatchSignaling.register_inbox("topic-a", "inbox-a")
      :ok = DispatchSignaling.register_inbox("topic-a", "inbox-b")
      :ok = DispatchSignaling.register_inbox("topic-b", "inbox-c")

      :ok = DispatchSignaling.signal_halt("topic-a", "inbox-b")

      :no_signal = DispatchSignaling.take("topic-a", "inbox-a")
      :stop_consuming = DispatchSignaling.take("topic-a", "inbox-b")
      :no_signal = DispatchSignaling.take("topic-b", "inbox-c")

      :ok = DispatchSignaling.unregister_inbox("topic-a", "inbox-a")
      :ok = DispatchSignaling.unregister_inbox("topic-a", "inbox-b")
      :ok = DispatchSignaling.unregister_inbox("topic-b", "inbox-c")
    end
  end

end
