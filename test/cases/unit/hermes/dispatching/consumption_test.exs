defmodule Hermes.Dispatching.ConsumptionTest do
  use ExUnit.Case

  alias Hermes.Dispatching.Consumption
  alias Hermes.Dispatching.DispatchSignaling

  setup do
    :meck.new(DispatchSignaling)
    :meck.new(Hermes.InboxServer)
    :meck.new(Hermes.Dispatching.Coordinator)
    :meck.new(Mockery.SimplePrinter)
    on_exit(fn -> :meck.unload end)
  end

  test "Hermes.Dispatching.Consumption.run; ok; inbox does not exist" do
    :meck.expect(DispatchSignaling, :take, fn _, _ -> :no_signal end)
    :meck.expect(Hermes.InboxServer, :take, fn _, _ -> :inbox_empty end)
    :meck.expect(Hermes.Dispatching.Coordinator, :is_alive, fn -> true end)

    {:inbox_empty, "test-topic", "test-inbox"} = Consumption.run(
      "test-topic", "test-inbox", fn -> :ok end)

    :meck.wait(Hermes.InboxServer, :take, ["test-topic", "test-inbox"], 1000)
  end

  test "Hermes.Dispatching.Consumption.run; ok; inbox is empty" do
    :meck.expect(DispatchSignaling, :take, fn _, _ -> :no_signal end)
    :meck.expect(Hermes.InboxServer, :take, fn _, _ -> :inbox_empty end)
    :meck.expect(Hermes.Dispatching.Coordinator, :is_alive, fn -> true end)

    {:inbox_empty, "test-topic", "test-inbox"} = Consumption.run(
      "test-topic", "test-inbox", fn -> :ok end)

    :meck.wait(Hermes.InboxServer, :take, ["test-topic", "test-inbox"], 1000)
  end

  test "Hermes.Dispatching.Consumption.run; ok; stop command received" do
    :meck.expect(DispatchSignaling, :take, fn _, _ -> :stop_consuming end)

    {:interrupted, "test-topic", "test-inbox"} = Consumption.run(
      "test-topic", "test-inbox", fn -> :ok end)

    :meck.wait(DispatchSignaling, :take, ["test-topic", "test-inbox"], 1000)
  end

  test "Hermes.Dispatching.Consumption.run; ok; coordinator is not alive" do
    :meck.expect(DispatchSignaling, :take, fn _, _ -> :no_signal end)
    :meck.expect(Hermes.InboxServer, :take, fn _, _ -> :not_alive end)
    :meck.expect(Hermes.Dispatching.Coordinator, :is_alive, fn -> true end)

    {:not_alive, "test-topic", "test-inbox"} = Consumption.run(
      "test-topic", "test-inbox", fn -> :ok end)

    :meck.wait(Hermes.InboxServer, :take, ["test-topic", "test-inbox"], 1000)
  end

  test "Hermes.Dispatching.Consumption.run; ok; consume and feed a function" do
    table = :ets.new(:fake_inbox, [:duplicate_bag, :public])
    :ets.insert(table, [{1, :message_1}, {1, :message_2}])
    take_fn = fn _, _ -> take_from_ets(table) end

    :meck.expect(DispatchSignaling, :take, fn _, _ -> :no_signal end)
    :meck.expect(Hermes.InboxServer, :take, take_fn)
    :meck.expect(Mockery.SimplePrinter, :print, fn _ -> :ok end)
    :meck.expect(Hermes.Dispatching.Coordinator, :is_alive, fn -> true end)

    f = fn _, _, m -> Mockery.SimplePrinter.print(m) end
    {:inbox_empty, "test-topic", "test-inbox"} = Consumption.run(
      "test-topic", "test-inbox", f)

    :meck.wait(Hermes.InboxServer, :take, ["test-topic", "test-inbox"], 100)
    :meck.wait(Mockery.SimplePrinter, :print, [:message_1], 100)
    :meck.wait(Mockery.SimplePrinter, :print, [:message_2], 100)
  end

  test "Hermes.Dispatching.Consumption.run; ok; consume and feed MFA" do
    table = :ets.new(:fake_inbox, [:duplicate_bag, :public])
    :ets.insert(table, [{1, :message_1}, {1, :message_2}])
    take_fn = fn _, _ -> take_from_ets(table) end

    :meck.expect(DispatchSignaling, :take, fn _, _ -> :no_signal end)
    :meck.expect(Hermes.InboxServer, :take, take_fn)
    :meck.expect(Mockery.SimplePrinter, :print, fn _, _, _ -> :ok end)
    :meck.expect(Hermes.Dispatching.Coordinator, :is_alive, fn -> true end)

    {:inbox_empty, "test-topic", "test-inbox"} = Consumption.run(
      "test-topic", "test-inbox", {Mockery.SimplePrinter, :print})

    :meck.wait(Hermes.InboxServer, :take, ["test-topic", "test-inbox"], 100)
    :meck.wait(Mockery.SimplePrinter, :print, ["test-topic", "test-inbox", :message_1], 100)
    :meck.wait(Mockery.SimplePrinter, :print, ["test-topic", "test-inbox", :message_2], 100)
  end

  defp take_from_ets(table) do
    case :ets.lookup(table, 1) do
      [{1, message} = entry|_remaining] ->
        :ets.delete_object(table, entry)
        message
      [] -> :inbox_empty
    end
  end
end
