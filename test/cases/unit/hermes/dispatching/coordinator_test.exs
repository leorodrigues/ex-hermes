defmodule Hermes.Dispatching.CoordinatorTest do
  use ExUnit.Case

  alias Hermes.Dispatching.DispatchSignaling
  alias Hermes.Dispatching.Dispatcher
  alias Hermes.Dispatching.Coordinator
  alias Hermes.Configuration

  setup_all %{} do
    Coordinator.cease_operation()
    Process.sleep(Coordinator.default_sleep_interval + 100)
  end

  setup do
    :meck.new(Dispatcher)
    :meck.new(Configuration)
    :meck.new(DispatchSignaling)
    :meck.new(Coordinator, [:passthrough])

    :meck.expect(DispatchSignaling, :signal_halt, fn -> :ok end)
    :meck.expect(Configuration, :get, fn _, :sleep_interval, _ -> 100 end)

    on_exit(fn ->
      Coordinator.cease_operation()
      Process.sleep(250)
      :meck.unload
    end)
  end

  describe "Hermes.Dispatching.Coordinator.is_alive" do
    test "should be false if operations have ceased" do
      false = Coordinator.is_alive()
    end

    test "should be true if the coordinator is running" do
      :meck.expect(Dispatcher, :dispatch, fn -> :ok end)

      :ok = Coordinator.commence_operation
      :meck.wait(Dispatcher, :dispatch, [], 2000)
      true = Coordinator.is_alive
    end
  end

  describe "Hermes.Dispatching.Coordinator.commence_operation" do
    test "should invoke the dispatcher when invoked once" do
      :meck.expect(Dispatcher, :dispatch, fn -> :ok end)

      :ok = Coordinator.commence_operation

      :meck.wait(Dispatcher, :dispatch, [ ], 1500)
    end

    test "should not be affected by second invocation" do
      :meck.expect(Dispatcher, :dispatch, fn -> :ok end)

      :ok = Coordinator.commence_operation
      :meck.wait(Coordinator, :handle_cast, [:commence_operation, :_], 1000)

      :ok = Coordinator.commence_operation
      :meck.wait(Coordinator, :handle_cast, [:commence_operation, :_], 1000)
    end
  end

end
