defmodule HermesTest do
  use ExUnit.Case

  @mods [
    UUID,
    Hermes.TopicHandler,
    Hermes.InboxSchemaServer,
    Hermes.Dispatching.Coordinator
  ]

  setup do
    :meck.new(@mods)
    on_exit(fn -> :meck.unload end)
  end

  describe "Hermes" do
    test "publish; delegate to topic handler" do
      :meck.expect(Hermes.TopicHandler, :broadcast, fn _, _ -> :fake_result end)
      :fake_result = Hermes.publish(:topic, :message)
      :meck.wait(Hermes.TopicHandler, :broadcast, [:topic, :message], 1000)
    end

    test "add_inboxes; delegate to topic handler and inbox schema server" do
      :meck.expect(Hermes.TopicHandler, :create_inboxes, fn _, _ -> :ok end)
      :ok = Hermes.add_inboxes(:topic, [:q1, :q2])
      :meck.wait(Hermes.TopicHandler, :create_inboxes, [:topic, [:q1, :q2]], 1000)
    end

    test "drop_inboxes; delegate to topic handler and inbox schema server" do
      :meck.expect(Hermes.TopicHandler, :drop_inboxes, fn _, _ -> :ok end)
      :ok = Hermes.drop_inboxes(:topic, [:q1, :q2])
      :meck.wait(Hermes.TopicHandler, :drop_inboxes, [:topic, [:q1, :q2]], 1000)
    end

    test "list_inboxes; delegate to inbox schema server" do
      :meck.expect(Hermes.InboxSchemaServer, :list_inboxes, fn -> :fake_inbox_list end)
      :fake_inbox_list = Hermes.list_inboxes
      :meck.wait(Hermes.InboxSchemaServer, :list_inboxes, [], 1000)
    end

    test "create_schema; delegate to inbox schema server" do
      :meck.expect(Hermes.InboxSchemaServer, :create_schema, fn -> :ok end)
      :ok = Hermes.create_schema
      :meck.wait(Hermes.InboxSchemaServer, :create_schema, [], 1000)
    end

    test "drop_schema; delegate to inbox schema server" do
      :meck.expect(Hermes.InboxSchemaServer, :drop_schema, fn -> :ok end)
      :ok = Hermes.drop_schema
      :meck.wait(Hermes.InboxSchemaServer, :drop_schema, [], 1000)
    end

    test "import_schema; delegate to inbox schema server" do
      :meck.expect(Hermes.InboxSchemaServer, :import_schema, fn _ -> :ok end)
      :ok = Hermes.import_schema("fake file path")
      :meck.wait(Hermes.InboxSchemaServer, :import_schema, ["fake file path"], 1000)
    end

    test "commence_operation; delegate to dispatching coordinator" do
      :meck.expect(Hermes.Dispatching.Coordinator, :commence_operation, fn -> :ok end)
      :ok = Hermes.commence_operation
      :meck.wait(Hermes.Dispatching.Coordinator, :commence_operation, [], 1000)
    end

    test "cease_operation; delegate to dispatching coordinator" do
      :meck.expect(Hermes.Dispatching.Coordinator, :cease_operation, fn -> :ok end)
      :ok = Hermes.cease_operation
      :meck.wait(Hermes.Dispatching.Coordinator, :cease_operation, [], 1000)
    end

    test "subscribe; add an adhoc inbox function handler" do
      test_func = fn -> :ok end
      :meck.expect(UUID, :uuid4, fn -> "1234qwer" end)
      :meck.expect(Hermes.TopicHandler, :subscribe, fn _, _, _ -> :fake_result end)

      :fake_result = Hermes.subscribe(:topic, test_func)

      :meck.wait(Hermes.TopicHandler, :subscribe, [:topic, "1234qwer", test_func], 1000)
    end

    test "subscribe; add a specific inbox function handler" do
      test_func = fn -> :ok end
      :meck.expect(Hermes.TopicHandler, :subscribe, fn _, _, _ -> :fake_result end)

      :fake_result = Hermes.subscribe(:topic, :my_inbox, test_func)

      :meck.wait(Hermes.TopicHandler, :subscribe, [:topic, :my_inbox, test_func], 1000)
    end

    test "subscribe; add an adhoc inbox MF handler" do
      :meck.expect(UUID, :uuid4, fn -> "5678tyui" end)
      :meck.expect(Hermes.TopicHandler, :subscribe, fn _, _, _, _ -> :fake_result end)

      :fake_result = Hermes.subscribe(:topic, :my_module, :my_func)

      :meck.wait(Hermes.TopicHandler, :subscribe, [:topic, "5678tyui", :my_module, :my_func], 1000)
    end

    test "subscribe; add a specific inbox MF handler" do
      :meck.expect(Hermes.TopicHandler, :subscribe, fn _, _, _, _ -> :fake_result end)

      :fake_result = Hermes.subscribe(:topic, :my_inbox, :my_module, :my_func)

      :meck.wait(Hermes.TopicHandler, :subscribe, [:topic, :my_inbox, :my_module, :my_func], 1000)
    end
  end
end
