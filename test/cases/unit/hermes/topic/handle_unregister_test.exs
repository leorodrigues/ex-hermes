defmodule Hermes.Topic.HandleUnregisterTest do
  use ExUnit.Case
  doctest Hermes.Topic.HandleUnregister

  alias Hermes.Topic.HandleUnregister

  test "HandleUnregister.invoke; a single callback from one topic" do
    state = [{"some/path", [{My.Module, [:my_fun1, :my_fun2]}]}]
    state = HandleUnregister.invoke({"some/path", {My.Module, :my_fun2}}, state)
    {:ok, [{"some/path", [{My.Module, [:my_fun1]}]}]} = state
  end

  test "HandleUnregister.invoke; the callback does not exist" do
    state = [{"some/path", [{My.Module, [:my_fun1]}]}]
    state = HandleUnregister.invoke({"some/path", {My.Module, :my_fun2}}, state)
    {:ok, [{"some/path", [{My.Module, [:my_fun1]}]}]} = state
  end

  test "HandleUnregister.invoke; the topic doesn't exist" do
    state = [{"some/path", [{My.Module, [:my_fun1]}]}]
    state = HandleUnregister.invoke({"some/other/path", {My.Module, :my_fun2}}, state)
    {:ok, [{"some/path", [{My.Module, [:my_fun1]}]}]} = state
  end

  test "HandleUnregister.invoke; the module doesn't exist" do
    state = [{"some/path", [{My.Module, [:my_fun1]}]}]
    state = HandleUnregister.invoke({"some/path", {My.Other.Module, :my_fun2}}, state)
    {:ok, [{"some/path", [{My.Module, [:my_fun1]}]}]} = state
  end
end
