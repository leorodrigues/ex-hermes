defmodule Hermes.Topic.ExpandTopicTest do
  use ExUnit.Case
  doctest Hermes.Topic.ExpandTopic

  alias Hermes.Topic.ExpandTopic

  test "ExpandTopic.invoke; no topics are available" do
    {:ok, [ ]} = ExpandTopic.invoke("some/path", [ ])
  end

  test "ExpandTopic.invoke; only topic available is not the right one" do
    topics = [{"some/path", [{{:module, My.Module}, [:my_fun]}]}]
    {:ok, [ ]} = ExpandTopic.invoke("some/other/path", topics)
  end

  test "ExpandTopic.invoke; topic was found" do
    topics = [{"some/path", [{{:module, My.Module}, [:my_fun]}]}]
    {:ok, [{:module, My.Module, :my_fun}]} = ExpandTopic.invoke("some/path", topics)
  end
end
