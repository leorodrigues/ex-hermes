defmodule Hermes.Topic.HandleRegisterTest do
  use ExUnit.Case
  doctest Hermes.Topic.HandleRegister

  alias Hermes.Topic.HandleRegister

  test "HandleRegister.invoke; single callback to a topic once" do
    state = [{"some/path", [{My.Module, [:my_fun]}]}]
    {:ok, ^state} = HandleRegister.invoke({"some/path", {My.Module, :my_fun}}, [ ])
  end

  test "HandleRegister.invoke; single callback to a topic twice" do
    state = [{"some/path", [{My.Module, [:my_fun]}]}]
    {:ok, ^state} = HandleRegister.invoke({"some/path", {My.Module, :my_fun}}, [ ])
    {:ok, ^state} = HandleRegister.invoke({"some/path", {My.Module, :my_fun}}, [ ])
  end

  test "HandleRegister.invoke; two callbacks on the same module and the same topic" do
    expected_state = [{"some/path", [{My.Module, [:my_other_fun, :my_fun]}]}]

    {:ok, state} = HandleRegister.invoke({"some/path", {My.Module, :my_fun}}, [ ])
    {:ok, state} = HandleRegister.invoke({"some/path", {My.Module, :my_other_fun}}, state)

    ^expected_state = state
  end

  test "HandleRegister.invoke; two callbacks from different modules to same topic" do
    expected_state = [{"some/path", [{My.OtherModule, [:my_fun]}, {My.Module, [:my_fun]}]}]

    {:ok, state} = HandleRegister.invoke({"some/path", {My.Module, :my_fun}}, [ ])
    {:ok, state} = HandleRegister.invoke({"some/path", {My.OtherModule, :my_fun}}, state)

    ^expected_state = state
  end
end
