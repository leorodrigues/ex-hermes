defmodule Hermes.TopicTest do
  use ExUnit.Case
  doctest Hermes.Topic

  require Logger

  alias Hermes.Topic
  alias Hermes.Topic.ExpandTopic
  alias Hermes.Topic.SinkMessage
  alias Hermes.Topic.HandleRegister
  alias Hermes.Topic.HandleUnregister
  alias Hermes.Dispatching.Dispatcher
  alias Hermes.Clock
  alias Hermes.Configuration

  @standard_modules [
    HandleUnregister,
    HandleRegister,
    Configuration,
    ExpandTopic,
    Dispatcher,
    Clock
  ]

  @passthrough_modules [SinkMessage, DummyServer]

  @non_strict_modules [My.Module, My.ErrorHandler]

  setup_all %{} do
    Hermes.Dispatching.Coordinator.cease_operation()
    Process.sleep(1000)
  end

  setup do
    :meck.new(@standard_modules)
    :meck.new(@passthrough_modules, [:passthrough])
    :meck.new(@non_strict_modules, [:non_strict])

    DummyServer.start_link()

    on_exit(fn -> :meck.unload() end)
  end

  describe "Hermes.Topic.get_dispatch_on_publish" do
    test "should delegate to Configuration" do
      :meck.expect(Configuration, :get, fn _, _, _ -> :fake_value end)
      :fake_value = Topic.get_dispatch_on_publish()
      :meck.wait(Configuration, :get, [:_, :dispatch_on_publish, Topic.default_dispatch_on_publish], 1000)
    end
  end

  describe "Hermes.Topic.set_dispatch_on_publish" do
    test "should delegate to Configuration" do
      :meck.expect(Configuration, :set, fn _, _, _ -> :fake_value end)
      :fake_value = Topic.set_dispatch_on_publish(:fake_value)
      :meck.wait(Configuration, :set, [:_, :dispatch_on_publish, :fake_value], 1000)
    end
  end

  describe "Hermes.Topic.register_server_cast" do
    test "should just delegate to the backend module" do
      :meck.expect(HandleRegister, :invoke, fn (_, state) -> {:ok, state} end)

      Topic.register_server_cast("some/topic", :my_server_id, :my_message_id)

      expected_arguments = [{"some/topic", {{:server, :my_server_id}, :my_message_id}}, :_]
      :meck.wait(HandleRegister, :invoke, expected_arguments, 1000)
    end
  end

  describe "Hermes.Topic.unregister_server_cast" do
    test "should just delegate to the backend module" do
      :meck.expect(HandleUnregister, :invoke, fn (_, state) -> {:ok, state} end)

      Topic.unregister_server_cast("some/topic", :my_server_id, :my_message_id)

      expected_arguments = [{"some/topic", {{:server, :my_server_id}, :my_message_id}}, :_]
      :meck.wait(HandleUnregister, :invoke, expected_arguments, 1000)
    end
  end

  describe "Hermes.Topic.broadcast" do
    test "should cast the message w/o auto dispatch" do
      :meck.expect(Configuration, :get, fn _, _, _ -> :offline end)
      :meck.expect(ExpandTopic, :invoke, fn
        "some/topic", _ -> {:ok, [{:server, DummyServer.self, :do_something}]}
      end)

      Topic.broadcast("some/topic", :my_message)

      :meck.wait(Configuration, :get, [Topic, :dispatch_on_publish, :_], 1000)
      :meck.wait(ExpandTopic, :invoke, ["some/topic", :_], 1000)
      :meck.wait(DummyServer, :handle_cast, [{:do_something, :my_message}, [ ]], 1000)
    end

    test "should cast the message with auto dispatch for single topic" do
      :meck.expect(Configuration, :get, fn _, _, _ -> :single_topic end)
      :meck.expect(Dispatcher, :dispatch, fn _ -> :ok end)
      :meck.expect(ExpandTopic, :invoke, fn
        "some/topic", _ -> {:ok, [{:server, DummyServer.self, :do_something}]}
      end)

      Topic.broadcast("some/topic", :my_message)

      :meck.wait(Configuration, :get, [Topic, :dispatch_on_publish, :_], 1000)
      :meck.wait(Dispatcher, :dispatch, ["some/topic"], 1000)
      :meck.wait(ExpandTopic, :invoke, ["some/topic", :_], 1000)
      :meck.wait(DummyServer, :handle_cast, [{:do_something, :my_message}, [ ]], 1000)
    end

    test "should cast the message with auto dispatch for all topics" do
      :meck.expect(Configuration, :get, fn _, _, _ -> :all_topics end)
      :meck.expect(Dispatcher, :dispatch, fn -> :ok end)
      :meck.expect(ExpandTopic, :invoke, fn
        "some/topic", _ -> {:ok, [{:server, DummyServer.self, :do_something}]}
      end)

      Topic.broadcast("some/topic", :my_message)

      :meck.wait(Configuration, :get, [Topic, :dispatch_on_publish, :_], 1000)
      :meck.wait(Dispatcher, :dispatch, [], 1000)
      :meck.wait(ExpandTopic, :invoke, ["some/topic", :_], 1000)
      :meck.wait(DummyServer, :handle_cast, [{:do_something, :my_message}, [ ]], 1000)
    end

    test "should sink messages for wich there is no topic" do
      :meck.expect(Configuration, :get, fn _, _, _ -> :all_topics end)
      :meck.expect(ExpandTopic, :invoke, fn (_, _) -> {:ok, [ ]} end)

      Topic.broadcast("some/topic", :my_message)

      :meck.wait(ExpandTopic, :invoke, ["some/topic", :_], 1000)
      :meck.wait(SinkMessage, :invoke, ["some/topic", :_, :_], 1000)
    end
  end

end
