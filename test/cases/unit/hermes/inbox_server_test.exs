defmodule Hermes.InboxServerTest do
  use ExUnit.Case

  alias Hermes.Dispatching.Coordinator
  alias Hermes.InboxServer
  alias Hermes.Inbox

  setup_all do
    Coordinator.cease_operation()
    Process.sleep(Coordinator.default_sleep_interval + 100)
  end

  setup do
    :meck.new(Inbox, [:passthrough])
    on_exit(fn -> :meck.unload() end)
  end

  describe "Hermes.InboxServer" do
    test "try taking from an non existent inbox" do
      :inbox_empty = InboxServer.take(:test_topic, :unknown_inbox)
    end

    test "put and take from an inbox" do
      :ok = InboxServer.put(:test_topic, :test_inbox_1, :m1)
      :m1 = InboxServer.take(:test_topic, :test_inbox_1)
      :meck.wait(Inbox, :put, [[], :m1, :end], 1000)
      :meck.wait(Inbox, :take, [[:m1]], 1000)
    end

    test "put via messaging and take from an inbox" do
      GenServer.cast(InboxServer, {{:put, :test_topic, :test_inbox_3}, :m3})
      :m3 = InboxServer.take(:test_topic, :test_inbox_3)
      :meck.wait(Inbox, :put, [[], :m3, :end], 1000)
      :meck.wait(Inbox, :take, [[:m3]], 1000)
    end

    test "put, drop the inbox then try to take" do
      :ok = InboxServer.put(:test_topic, :test_inbox_2, :m2)
      :ok = InboxServer.drop(:test_topic, :test_inbox_2)
      :inbox_empty = InboxServer.take(:test_topic, :test_inbox_2)
    end
  end
end
