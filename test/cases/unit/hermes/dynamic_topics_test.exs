defmodule Hermes.DynamicTopicsTest do
  use ExUnit.Case

  import Enum, only: [join: 2]

  import Hermes.DynamicTopics

  describe "Hermes.DynamicTopics.join_topic_elements" do
    test "shoud join elements using a default separator" do
      expected = join(["a", "b", "c"], separator())

      ^expected = join_topic_elements(["a", "b", "c"])
    end
  end

  describe "Hermes.DynamicTopic.compute_event_topic" do
    test "should concatenate everything" do
      "org.leorodrigues.hermes/ex-unit-test/events/some-event" = compute_event_topic(
        "org.leorodrigues.hermes", :ex_unit_test, :some_event
      )
    end
  end

  describe "Hermes.DynamicTopic.compute_command_topic" do
    test "should concatenate everything" do
      "org.leorodrigues.hermes/ex-unit-test/commands/some-command" = compute_command_topic(
        "org.leorodrigues.hermes", :ex_unit_test, :some_command
      )
    end
  end

  describe "Hermes.DynamicTopic.compute_reply_topic" do
    test "should concatenate everything" do
      "org.leorodrigues.hermes/fake-caller-app/replies/ex-unit-test/some-command" = compute_reply_topic(
        "org.leorodrigues.hermes", :ex_unit_test, :fake_caller_app, :some_command
      )
    end
  end

  describe "Hermes.DynamicTopic.compute_event_topic_elements" do
    test "should concatenate everything" do
      [
        "org.leorodrigues.hermes",
        "ex-unit-test",
        "events",
        "some-event"
      ] = compute_event_topic_elements(
        "org.leorodrigues.hermes",
        :ex_unit_test,
        :some_event
      )
    end
  end

  describe "Hermes.DynamicTopic.compute_command_topic_elements" do
    test "should concatenate everything" do
      [
        "org.leorodrigues.hermes",
        "ex-unit-test",
        "commands",
        "some-command"
      ] = compute_command_topic_elements(
        "org.leorodrigues.hermes",
        :ex_unit_test,
        :some_command
      )
    end
  end

  describe "Hermes.DynamicTopic.compute_reply_topic_elements" do
    test "should concatenate everything" do
      [
        "org.leorodrigues.hermes",
        "fake-caller-app",
        "replies",
        "ex-unit-test",
        "some-command"
      ] = compute_reply_topic_elements(
        "org.leorodrigues.hermes",
        :ex_unit_test,
        :fake_caller_app,
        :some_command
      )
    end
  end
end
