defmodule Hermes.Piping.Outbound.EventTriggertingTest do
  use ExUnit.Case

  alias Hermes.Piping.Outbound.EventTriggering
  alias Hermes.Piping.Outbound.MakeEventPipe

  @modules_to_stub [MakeEventPipe]

  setup do
    :meck.new(@modules_to_stub, [:non_strict])
    on_exit(fn -> :meck.unload() end)
  end

  describe "Hermes.Piping.Outbound.EventTriggering.convey" do
    test "should wrap the payload within an event envelope within a message envelope" do
      :meck.expect(MakeEventPipe, :convey, fn :payload, _ -> :event_payload end)

      result = EventTriggering.convey(:payload, [ ])

      assert result == :event_payload
    end
  end
end
