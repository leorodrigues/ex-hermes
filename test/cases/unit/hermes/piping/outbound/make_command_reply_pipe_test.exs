defmodule Hermes.Piping.Outbound.MakeCommandReplyPipeTest do
  use ExUnit.Case

  alias Hermes.Piping.Outbound.MakeCommandReplyPipe

  alias Hermes.Domain.Context

  @modules_to_stub [Context]

  setup do
    :meck.new(@modules_to_stub, [:non_strict])
    on_exit(fn -> :meck.unload() end)
  end

  describe "Hermes.Piping.Outbound.MakeCommandReplyPipe" do
    test "should invoke the command layer to make the reply" do
      :meck.expect(Context, :make_reply, fn :payload, _ -> :reply end)

      :reply = MakeCommandReplyPipe.convey(:payload, [ ])
    end

    test "should interrupt the piping and return any instantiation errors" do
      :meck.expect(Context, :make_reply, fn :payload, _ -> {:error, :some_reason} end)

      {:error, :some_reason} = MakeCommandReplyPipe.convey(:payload, [ ])
    end
  end
end
