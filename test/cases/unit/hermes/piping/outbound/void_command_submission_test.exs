defmodule Hermes.Piping.Outbound.VoidCommandSubmissionTest do
  use ExUnit.Case

  # subject
  alias Hermes.Piping.Outbound.VoidCommandSubmission

  # modules to be stubbed
  alias Hermes.Piping.Outbound.MakeCommandPipe

  @module_to_stub [MakeCommandPipe]

  setup do
    :meck.new(@module_to_stub, [:non_strict])
    on_exit(fn -> :meck.unload() end)
  end

  describe "Hermes.Piping.Outbound.VoidCommandSubmission.convey" do
    test "should convey the message through the command and message pipes" do
      :meck.expect(MakeCommandPipe, :convey, fn :payload_1, _ -> :payload_2 end)

      result = VoidCommandSubmission.convey(:payload_1, [:fake_options])

      assert result == :payload_2
    end
  end
end
