defmodule Hermes.Piping.Outbound.CommandSubmissionTest do
  use ExUnit.Case

  # subject
  alias Hermes.Piping.Outbound.CommandSubmission

  # modules to be stubbed
  alias Hermes.Piping.Outbound.MakeCommandPipe
  alias Hermes.Domain.Envelope

  @module_to_stub [MakeCommandPipe, Envelope]

  setup do
    :meck.new(@module_to_stub, [:non_strict])
    on_exit(fn -> :meck.unload() end)
  end

  describe "Hermes.Piping.Outbound.CommandSubmission.convey" do
    test "should run the full envelope sequence" do
      :meck.expect(MakeCommandPipe, :convey, fn :payload, _ -> :envelope end)
      :meck.expect(Envelope, :set_header, fn :envelope, :reply_topic, :topic -> :complete_envelope end)

      :complete_envelope = CommandSubmission.convey(:payload, [reply_topic: :topic])
    end

    test "should reject the payload if the reply topic is missing" do
      :meck.expect(MakeCommandPipe, :convey, fn :payload, _ -> :envelope end)
      assert {:error, :missing_reply_topic} == CommandSubmission.convey(:payload, [ ])
    end
  end
end
