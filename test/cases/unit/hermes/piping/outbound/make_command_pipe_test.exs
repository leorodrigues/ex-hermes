defmodule Hermes.Piping.Outbound.MakeCommandPipeTest do
  use ExUnit.Case

  alias Hermes.Piping.Outbound.MakeCommandPipe

  alias Hermes.Domain.Context

  @modules_to_stub [Context]

  setup do
    :meck.new(@modules_to_stub, [:non_strict])
    on_exit(fn -> :meck.unload() end)
  end

  describe "Hermes.Piping.Outbound.MakeCommandPipe" do
    test "should make a command and load it with context info" do
      :meck.expect(Context, :make_command, fn :payload, _ -> :command_payload end)

      :command_payload = MakeCommandPipe.convey(:payload)
    end
  end
end
