defmodule Hermes.Piping.Outbound.MakeEventPipeTest do
  use ExUnit.Case

  alias Hermes.Piping.Outbound.MakeEventPipe

  alias Hermes.Domain.Context

  @modules_to_stub [Context]

  setup do
    :meck.new(@modules_to_stub, [:non_strict])
    on_exit(fn -> :meck.unload() end)
  end

  describe "Hermes.Piping.Outbound.MakeEventPipe.convey" do
    test "should wrap the payload within an envelope" do
      :meck.expect(Context, :make_event, fn :payload, _ -> :event_instance end)

      :event_instance = MakeEventPipe.convey(:payload)
    end
  end
end
