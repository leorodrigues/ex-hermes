defmodule Hermes.Piping.Inbound.OpenCommandPipeTest do
  use ExUnit.Case

  alias Hermes.Piping.Inbound.ConfirmEnvelopeStructurePipe
  alias Hermes.Piping.Inbound.StandardEnvelopeOpeningPipe
  alias Hermes.Piping.Inbound.OpenCommandPipe

  @modules_to_stub [
    ConfirmEnvelopeStructurePipe,
    StandardEnvelopeOpeningPipe
  ]

  setup do
    :meck.new(@modules_to_stub, [:non_strict])
    on_exit(fn -> :meck.unload() end)
  end

  describe "Hermes.Piping.Inbound.OpenCommandPipe.handle" do
    test "should successfuly convey the payload" do
      :meck.expect(ConfirmEnvelopeStructurePipe, :convey, fn env, _ -> env end)
      :meck.expect(StandardEnvelopeOpeningPipe, :convey, fn env, _ -> env end)

      :fake_envelope = OpenCommandPipe.convey(:fake_envelope)
    end
  end
end
