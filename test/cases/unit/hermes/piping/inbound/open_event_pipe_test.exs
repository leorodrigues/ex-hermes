defmodule Hermes.Piping.Inbound.OpenEventPipeTest do
  use ExUnit.Case

  alias Hermes.Piping.Inbound.StandardEnvelopeOpeningPipe
  alias Hermes.Piping.Inbound.OpenEventPipe

  describe "Hermes.Piping.Inbound.OpenEventPipe" do
    test "should expose the event payload" do
      :meck.expect(StandardEnvelopeOpeningPipe, :convey, fn env, _ -> env end)
      :fake_envelope = OpenEventPipe.convey(:fake_envelope, [ ])
    end
  end
end
