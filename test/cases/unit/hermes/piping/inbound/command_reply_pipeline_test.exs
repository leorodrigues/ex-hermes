defmodule Hermes.Piping.Inbound.CommandReplyPipelineTest do
  use ExUnit.Case

  alias Hermes.Piping.Inbound.CommandReplyPipeline

  alias Hermes.Piping.Inbound.OpenCommandReplyPipe
  alias Hermes.Piping.Inbound.RunBusinessTargetPipe


  @modules_to_stub [
    OpenCommandReplyPipe,
    RunBusinessTargetPipe
  ]

  setup do
    :meck.new(@modules_to_stub, [:non_strict])
    on_exit(fn -> :meck.unload() end)
  end

  describe "Hermes.Piping.Inbound.CommandReplyPipeline" do
    test "should convey the payload through all pipes" do
      :meck.expect(OpenCommandReplyPipe, :convey, fn :command, _ -> :content end)
      :meck.expect(RunBusinessTargetPipe, :convey, fn :content, _ -> :result end)

      :result = CommandReplyPipeline.convey(:command, [ ])
    end
  end
end
