defmodule Hermes.Piping.Inbound.OpenVoidCommandPipeTest do
  use ExUnit.Case

  alias Hermes.Piping.Inbound.StandardEnvelopeOpeningPipe
  alias Hermes.Piping.Inbound.OpenVoidCommandPipe

  describe "Hermes.Piping.Inbound.OpenVoidCommandPipe.load_context" do
    test "should copy only specific headers and return the envelope content" do
      :meck.expect(StandardEnvelopeOpeningPipe, :convey, fn env, _ -> env end)
      :fake_envelope = OpenVoidCommandPipe.convey(:fake_envelope)
    end
  end
end
