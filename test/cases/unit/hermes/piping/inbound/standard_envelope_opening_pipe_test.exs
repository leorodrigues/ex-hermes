defmodule Hermes.Piping.Inbound.StandardEnvelopeOpeningPipeTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :hermes

  alias Hermes.Piping.Inbound.ConfirmEnvelopeStructurePipe

  alias Hermes.Piping.Inbound.StandardEnvelopeOpeningPipe

  alias Hermes.Domain.Context

  import Hermes.TestUtils

  @modules_to_stub [ConfirmEnvelopeStructurePipe]

  @modules_to_spy_on [Logger]

  setup_all_environment do
    [ ]
  end

  setup ctx do
    case ctx do
      %{samples_file: f} -> load_samples(ctx, resolve_test_resource_path([f]))
      _ -> :ok
    end
  end

  setup do
    :meck.new(@modules_to_stub, [:non_strict])
    :meck.new(@modules_to_spy_on, [:passthrough])

    on_exit(fn -> :meck.unload end)
  end

  describe "Hermes.Piping.Inbound.StandardEnvelopeOpeningPipe.convey" do
    @tag samples_file: "cases.terms"
    test "should load logger meta data, load the process context, and deliver the content", %{samples: s} do
      env = using_sample(s, :case1_envelope)
      metadata = using_sample(s, :case1_logger_metadata)

      :meck.expect(ConfirmEnvelopeStructurePipe, :convey, fn e, _ -> e end)
      :meck.expect(Context, :extract_logger_metadata, fn ^env -> metadata end)
      :meck.expect(Context, :copy_from_envelope_to_process, fn ^env -> env end)

      :payload = StandardEnvelopeOpeningPipe.convey(env, [scope: :test, message_type: :fake])

      :meck.expect(Context, :copy_from_envelope_to_process, [env], 2000)
      :meck.expect(Logger, :metadata, [metadata], 2000)
    end

    @tag samples_file: "cases.terms"
    test "should fail on wrong message_type", %{samples: s} do
      :meck.expect(ConfirmEnvelopeStructurePipe, :convey, fn e, _ -> e end)

      env = using_sample(s, :case2_envelope)

      error = using_sample(s, :case2_error)

      ^error = StandardEnvelopeOpeningPipe.convey(env, [scope: :test, message_type: :fake])
    end

    @tag samples_file: "cases.terms"
    test "should fail on missing expected message_type", %{samples: s} do
      :meck.expect(ConfirmEnvelopeStructurePipe, :convey, fn e, _ -> e end)

      env = using_sample(s, :case3_envelope)

      error = using_sample(s, :case3_error)

      ^error = StandardEnvelopeOpeningPipe.convey(env, [scope: :test])
    end
  end
end
