defmodule Hermes.Piping.Inbound.CommandPipelineTest do
  use ExUnit.Case

  alias Hermes.Piping.Inbound.CommandPipeline

  alias Hermes.Piping.Inbound.OpenCommandPipe
  alias Hermes.Piping.Inbound.RunBusinessTargetPipe
  alias Hermes.Piping.Outbound.MakeCommandReplyPipe
  alias Hermes.Piping.Outbound.PublishReplyPipe

  @modules_to_stub [
    OpenCommandPipe,
    RunBusinessTargetPipe,
    MakeCommandReplyPipe,
    PublishReplyPipe
  ]

  setup do
    :meck.new(@modules_to_stub, [:non_strict])
    on_exit(fn -> :meck.unload() end)
  end

  describe "Hermes.Piping.Inbound.CommandPipeline" do
    test "should convey the payload through all pipes" do
      :meck.expect(OpenCommandPipe, :convey, fn :command, _ -> :content end)
      :meck.expect(RunBusinessTargetPipe, :convey, fn :content, _ -> :result end)
      :meck.expect(MakeCommandReplyPipe, :convey, fn :result, _ -> :command_reply end)
      :meck.expect(PublishReplyPipe, :convey, fn :command_reply, _ -> :final_result end)

      :final_result = CommandPipeline.convey(:command, [ ])
    end
  end
end
