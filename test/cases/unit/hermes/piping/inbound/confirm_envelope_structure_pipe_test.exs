defmodule Hermes.Piping.Inbound.ConfirmEnvelopeStructurePipeTest do
  use ExUnit.Case

  alias Hermes.Domain.Envelope
  alias Hermes.Piping.Inbound.ConfirmEnvelopeStructurePipe

  describe "Hermes.Piping.Inbound.ConfirmEnvelopeStructurePipe.convey" do
    test "should return the envelope" do
      env = Envelope.new(:my_payload) |> Envelope.merge_header(%{some_key: :value})

      ^env = ConfirmEnvelopeStructurePipe.convey(env, scope: :test, names: [:some_key])
    end

    test "should return an error if the name is missing" do
      env = Envelope.new(:my_payload)

      error = {:error, {:malformed_envelope, [
        scope: :test,
        reason: :missing_header_keys,
        keys: [:some_key]
      ]}}

      ^error = ConfirmEnvelopeStructurePipe.convey(env, scope: :test, names: [:some_key])
    end

    test "should return an error if the payload is not an envelope" do
      error = {:error, {:malformed_envelope, [
        scope: :test,
        reason: :not_an_envelope
      ]}}

      ^error = ConfirmEnvelopeStructurePipe.convey(:payload, scope: :test, names: [:some_key])
    end

    test "should return the envelope if there is no list of names" do
      env = Envelope.new(:my_payload)

      ^env = ConfirmEnvelopeStructurePipe.convey(env, [ ])
    end
  end
end
