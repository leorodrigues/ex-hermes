defmodule Hermes.Piping.Inbound.VoidCommandPipelineTest do
  use ExUnit.Case

  alias Hermes.Piping.Inbound.VoidCommandPipeline

  alias Hermes.Piping.Inbound.OpenVoidCommandPipe
  alias Hermes.Piping.Inbound.RunBusinessTargetPipe


  @modules_to_stub [
    OpenVoidCommandPipe,
    RunBusinessTargetPipe
  ]

  setup do
    :meck.new(@modules_to_stub, [:non_strict])
    on_exit(fn -> :meck.unload() end)
  end

  describe "Hermes.Piping.Inbound.VoidCommandPipeline" do
    test "should convey the payload through all pipes" do
      :meck.expect(OpenVoidCommandPipe, :convey, fn :command, _ -> :content end)
      :meck.expect(RunBusinessTargetPipe, :convey, fn :content, _ -> :result end)

      :result = VoidCommandPipeline.convey(:command, [ ])
    end
  end
end
