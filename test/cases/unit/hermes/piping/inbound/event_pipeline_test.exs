defmodule Hermes.Piping.Inbound.EventPipelineTest do
  use ExUnit.Case

  alias Hermes.Piping.Inbound.EventPipeline

  alias Hermes.Piping.Inbound.OpenEventPipe
  alias Hermes.Piping.Inbound.RunBusinessTargetPipe

  @modules_to_stub [ OpenEventPipe, RunBusinessTargetPipe ]

  setup do
    :meck.new(@modules_to_stub, [:non_strict])
    on_exit(fn -> :meck.unload() end)
  end

  describe "Hermes.Piping.Inbound.EventPipeline" do
    test "should convey the payload through all pipes" do
      :meck.expect(OpenEventPipe, :convey, fn :event, _ -> :content end)
      :meck.expect(RunBusinessTargetPipe, :convey, fn :content, _ -> :final_result end)

      :final_result = EventPipeline.convey(:event, [ ])
    end
  end
end
