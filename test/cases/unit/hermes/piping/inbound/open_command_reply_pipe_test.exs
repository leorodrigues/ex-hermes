defmodule Hermes.Piping.Inbound.OpenCommandReplyPipeTest do
  use ExUnit.Case

  alias Hermes.Piping.Inbound.ConfirmEnvelopeStructurePipe
  alias Hermes.Piping.Inbound.StandardEnvelopeOpeningPipe
  alias Hermes.Piping.Inbound.OpenCommandReplyPipe

  @modules_to_stub [
    ConfirmEnvelopeStructurePipe,
    StandardEnvelopeOpeningPipe
  ]

  setup do
    :meck.new(@modules_to_stub, [:non_strict])
    on_exit(fn -> :meck.unload() end)
  end

  describe "Hermes.Piping.Inbound.OpenCommandReplyPipe" do
    test "should expose the command reply payload" do
      :meck.expect(ConfirmEnvelopeStructurePipe, :convey, fn env, _ -> env end)
      :meck.expect(StandardEnvelopeOpeningPipe, :convey, fn env, _ -> env end)

      :fake_envelope = OpenCommandReplyPipe.convey(:fake_envelope, [ ])
    end
  end
end
