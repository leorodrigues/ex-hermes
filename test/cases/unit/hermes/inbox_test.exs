defmodule Hermes.InboxTest do
  use ExUnit.Case

  alias Hermes.Inbox

  test "Hermes.Inbox.put; ok; add one message to empty inbox" do
    [1] = Inbox.put([], 1)
  end

  test "Hermes.Inbox.put; ok; add two messages to empty inbox" do
    [11, 7] = Inbox.put([], [11, 7])
  end

  test "Hermes.Inbox.put; ok; add one message to inbox containing one message" do
    [19, 23] = Inbox.put([19], 23)
  end

  test "Hermes.Inbox.put; ok; add two messages to inbox containing one message" do
    [29, 11, 3] = Inbox.put([29], [11, 3])
  end

  test "Hermes.Inbox.put; ok; add one message to the start of inbox" do
    [3, 4, 5] = Inbox.put([4, 5], 3, :start)
  end

  test "Hermes.Inbox.put; ok; add two messages to the start of an inbox" do
    [2, 3, 4, 5] = Inbox.put([4, 5], [3, 2], :start)
  end

  test "Hermes.Inbox.take; ok; take one message from empty inbox" do
    {nil, []} = Inbox.take([])
  end

  test "Hermes.Inbox.take; ok; take one message from inbox with two messages" do
    {31, [17]} = Inbox.take([31, 17])
  end
end
