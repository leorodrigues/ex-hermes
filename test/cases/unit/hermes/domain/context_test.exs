defmodule Hermes.Domain.ContextTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :hermes

  alias Hermes.Domain.Envelope
  alias Hermes.Domain.Context

  import Hermes.TestUtils

  setup_all_environment do
    [ ]
  end

  setup ctx do
    case ctx do
      %{samples_file: f} -> load_samples(ctx, resolve_test_resource_path([f]))
      _ -> :ok
    end
  end

  setup do
    :meck.new([UUID])
    :meck.expect(UUID, :uuid4, fn -> "fake-id" end)
    on_exit(fn -> :meck.unload end)
  end

  describe "Hermes.Domain.Context.copy_from_envelope_to_process" do
    test "should create a context by copying the envelope header" do
      Context.copy_from_envelope_to_process(Envelope.new(:fake_content))
      %{id: _} = Process.get(Context.context_name)
    end
  end

  describe "Hermes.Domain.Context.make_command" do

    @tag samples_file: "case1.terms"
    test "should make a command with a message id, correlation id and no source id", %{samples: s} do
      expected_command = using_sample(s, :command1)
      ^expected_command = Context.make_command(:fake_content)
    end

    @tag samples_file: "case1.terms"
    test "should make a command with a message id, correlation id and source id", %{samples: s} do
      expected_command = using_sample(s, :command2)
      Process.put(Context.context_name, %{message_id: "the-source"})
      ^expected_command = Context.make_command(:fake_content)
    end

    @tag samples_file: "case1.terms"
    test "should make a command with a message id, inherited correlation id and source id", %{samples: s} do
      expected_command = using_sample(s, :command3)
      Process.put(Context.context_name, %{message_id: "the-source", correlation_id: "some-business"})
      ^expected_command = Context.make_command(:fake_content)
    end

    @tag samples_file: "case1.terms"
    test "should make a command with a message id, overriden correlation id and source id", %{samples: s} do
      expected_command = using_sample(s, :command4)
      Process.put(Context.context_name, %{message_id: "the-source", correlation_id: "some-business"})
      ^expected_command = Context.make_command(:fake_content, [correlation_id: "overridden"])
    end
  end

  describe "Hermes.Domain.Context.make_event" do

    @tag samples_file: "case2.terms"
    test "should make a event with a message id, correlation id and no source id", %{samples: s} do
      expected_event = using_sample(s, :event1)
      ^expected_event = Context.make_event(:fake_content)
    end

    @tag samples_file: "case2.terms"
    test "should make a event with a message id, correlation id and source id", %{samples: s} do
      expected_event = using_sample(s, :event2)
      Process.put(Context.context_name, %{message_id: "the-source"})
      ^expected_event = Context.make_event(:fake_content)
    end

    @tag samples_file: "case2.terms"
    test "should make a event with a message id, inherited correlation id and source id", %{samples: s} do
      expected_event = using_sample(s, :event3)
      Process.put(Context.context_name, %{message_id: "the-source", correlation_id: "some-business"})
      ^expected_event = Context.make_event(:fake_content)
    end

    @tag samples_file: "case2.terms"
    test "should make a event with a message id, overriden correlation id and source id", %{samples: s} do
      expected_event = using_sample(s, :event4)
      Process.put(Context.context_name, %{message_id: "the-source", correlation_id: "some-business"})
      ^expected_event = Context.make_event(:fake_content, [correlation_id: "overridden"])
    end
  end

  describe "Hermes.Domain.Context.make_reply" do

    @tag samples_file: "case3.terms"
    test "should make a reply with a message id, correlation id and source id", %{samples: s} do
      expected_reply = using_sample(s, :reply1)
      Process.put(Context.context_name, %{message_id: "the-source"})
      ^expected_reply = Context.make_reply(:fake_content)
    end

    @tag samples_file: "case3.terms"
    test "should make a reply with a message id, inherited correlation id and source id", %{samples: s} do
      expected_reply = using_sample(s, :reply2)
      Process.put(Context.context_name, %{message_id: "the-source", correlation_id: "some-business"})
      ^expected_reply = Context.make_reply(:fake_content)
    end

    @tag samples_file: "case3.terms"
    test "should make a reply with a message id, overriden correlation id and source id", %{samples: s} do
      expected_reply = using_sample(s, :reply3)
      Process.put(Context.context_name, %{message_id: "the-source", correlation_id: "some-business"})
      ^expected_reply = Context.make_reply(:fake_content, [correlation_id: "overridden"])
    end

    @tag samples_file: "case3.terms"
    test "should fail to make a reply without a command context", %{samples: s} do
      expected_error = using_sample(s, :error)
      ^expected_error = Context.make_reply(:fake_content)
    end
  end
end
