defmodule Hermes.Domain.EnvelopeTest do
  use ExUnit.Case

  alias Hermes.Domain.Envelope

  setup do
    :meck.new(UUID, [:passthrough])
    on_exit(fn -> :meck.unload end)
  end

  describe "Hermes.Domain.Envelope.new" do
    test "should return an envelope with an empty header" do
      :meck.expect(UUID, :uuid4, fn -> "112358" end)
      envelope = %Envelope{content: :fake_payload, header: %{id: "112358"}}
      ^envelope = Envelope.new(:fake_payload)
    end
  end

  describe "Hermes.Domain.Envelope.merge_header" do
    test "should merge the original header with the new one" do
      old = %Envelope{content: :fake_payload, header: %{a: 1}}
      new = %Envelope{content: :fake_payload, header: %{a: 2, b: 3}}
      ^new = Envelope.merge_header(old, %{a: 2, b: 3})
    end

    test "should merge the original header with the new one except for some keys" do
      old = %Envelope{content: :fake_payload, header: %{a: 1}}
      new = %Envelope{content: :fake_payload, header: %{a: 1, b: 3}}
      ^new = Envelope.merge_header(old, %{a: 2, b: 3}, [:a])
    end
  end

  describe "Hermes.Domain.Envelope.copy_header_to_process" do
    test "should copy all header pairs from an envelope to the current process" do
      env = Envelope.new(:fake_payload) |> Envelope.merge_header(%{some_key_1: 7, some_key_2: 11})
      Envelope.copy_header_to_process(env, :test_context)
      %{some_key_1: 7, some_key_2: 11} = Process.get(:test_context)
    end

    test "should copy specific header pairs from an envelope to the current process" do
      env = Envelope.new(:fake_payload) |> Envelope.merge_header(%{some_key_1: 13, some_key_2: 17})
      Envelope.copy_header_to_process(env, :test_context, [:some_key_2])
      %{some_key_2: 17} = Process.get(:test_context)
    end
  end
end
