defmodule Hermes.InboxSchemaServerTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :hermes

  alias Hermes.InboxSchemaServer
  alias Hermes.Dispatching.Coordinator

  require Logger

  setup_all_environment do
    Coordinator.cease_operation()
    Process.sleep(Coordinator.default_sleep_interval)
  end

  setup ctx do
    case ctx do
      %{import_name: n} -> [case_import_path: resolve_test_resource_path([n])]
      _ -> ctx
    end
  end

  setup do
    InboxSchemaServer.drop_schema()
    Process.sleep(100)
  end

  describe "Hermes.InboxSchemaServer" do
    test "ok; add inboxes to existing schema" do
      :ok = InboxSchemaServer.create_schema()

      [ ] = InboxSchemaServer.list_inboxes()

      :ok = InboxSchemaServer.add_inboxes(:test_topic, [:inbox_one, :inbox_two])

      [{:test_topic, [:inbox_one, :inbox_two]}] = InboxSchemaServer.list_inboxes()
    end

    test "ok; add inboxes after creation" do
      :ok = InboxSchemaServer.create_schema()

      :ok = InboxSchemaServer.add_inboxes(:test_topic, [:a, :b, :c])

      [{:test_topic, [:a, :b, :c]}] = InboxSchemaServer.list_inboxes()
    end

    @tag import_name: "sample-schema1.terms"
    test "ok; drop inboxes only", %{case_import_path: p} do
      :ok = InboxSchemaServer.create_schema()

      InboxSchemaServer.import_schema(p)

      initial_schema = [
        {:topic_one, [:inbox_one, :inbox_two]},
        {:topic_two, [:inbox_three, :inbox_four]}
      ]

      ^initial_schema = InboxSchemaServer.list_inboxes()

      :ok = InboxSchemaServer.drop_inboxes(:topic_one, [:inbox_two])

      final_schema = [
        {:topic_one, [:inbox_one]},
        {:topic_two, [:inbox_three, :inbox_four]}
      ]

      ^final_schema = InboxSchemaServer.list_inboxes()
    end

    @tag import_name: "sample-schema1.terms"
    test "ok; drop entire topic", %{case_import_path: p} do
      :ok = InboxSchemaServer.create_schema()

      InboxSchemaServer.import_schema(p)

      initial_schema = [
        {:topic_one, [:inbox_one, :inbox_two]},
        {:topic_two, [:inbox_three, :inbox_four]}
      ]

      ^initial_schema = InboxSchemaServer.list_inboxes()

      :ok = InboxSchemaServer.drop_inboxes(:topic_one)

      final_schema = [
        {:topic_two, [:inbox_three, :inbox_four]}
      ]

      ^final_schema = InboxSchemaServer.list_inboxes()
    end

    @tag import_name: "non-existent-schema.terms"
    test "error; import non existent terms file", %{case_import_path: p} do
      :ok = InboxSchemaServer.create_schema()
      expected_result = {:error, {:file_not_found, p}}
      ^expected_result = InboxSchemaServer.import_schema(p)
    end

    @tag import_name: "sample-schema1.terms"
    test "error; import schema before creating", %{case_import_path: p} do
      {:error, {:schema_not_found, _}} = InboxSchemaServer.import_schema(p)
    end

    test "error; drop schema before creating" do
      {:error, {:schema_not_found, _}} = InboxSchemaServer.drop_schema()
    end

    test "error; create schema twice" do
      :ok = InboxSchemaServer.create_schema()
      Process.sleep(100)
      {:error, {:schema_already_exists, _}} = InboxSchemaServer.create_schema()
    end
  end
end
