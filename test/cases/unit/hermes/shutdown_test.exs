defmodule Hermes.ShutdownTest do
  use ExUnit.Case

  alias Hermes.Dispatching.Coordinator
  alias Hermes.InboxSchemaServer

  @mocked_modules [Coordinator, InboxSchemaServer]

  setup do
    :meck.new(@mocked_modules)
    on_exit(fn -> :meck.unload() end)
  end

  describe "Hermes.Shutdown" do
    test "Should sync the schem and stop the coordinator" do
      :meck.expect(Coordinator, :cease_operation, fn -> :ok end)
      :meck.expect(InboxSchemaServer, :sync, fn -> :ok end)

      Hermes.Shutdown.run()

      :meck.wait(Coordinator, :cease_operation, [ ], 1000)
      :meck.wait(InboxSchemaServer, :sync, [ ], 1000)
    end
  end
end
