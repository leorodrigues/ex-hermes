defmodule Hermes.BootstrapSchemaTest do
  use ExUnit.Case

  alias Hermes.BootstrapSchema

  @mocked_modules [
    Hermes.TopicHandler,
    Hermes.InboxSchemaServer,
    Hermes.Dispatching.Coordinator
  ]

  setup do
    :meck.new(@mocked_modules)
    on_exit(fn -> :meck.unload end)
  end

  describe "Hermes.BootstrapSchema.run" do
    test "Should reload all topics" do
      :meck.expect(Hermes.Dispatching.Coordinator, :commence_operation, fn -> :ok end)
      :meck.expect(Hermes.InboxSchemaServer, :list_inboxes, fn -> [
        {:topic_one, [:inbox_one, :inbox_two]},
        {:topic_two, [:inbox_three]}
      ] end)

      :meck.expect(Hermes.TopicHandler, :restore_inboxes, fn _, _ -> :ok end)

      BootstrapSchema.run(nil)

      :meck.wait(Hermes.Dispatching.Coordinator, :commence_operation, [ ], 1000)

      :meck.wait(Hermes.TopicHandler, :restore_inboxes, [
        :topic_one, [:inbox_one, :inbox_two]], 1000)

      :meck.wait(Hermes.TopicHandler, :restore_inboxes, [
        :topic_two, [:inbox_three]], 1000)
    end

    test "Should create the schema if it does not exists" do
      outcome = {:error, {:schema_not_found, "fake-file-path.bin"}}

      :meck.expect(Hermes.Dispatching.Coordinator, :commence_operation, fn -> :ok end)
      :meck.expect(Hermes.InboxSchemaServer, :list_inboxes, fn -> outcome end)
      :meck.expect(Hermes.InboxSchemaServer, :create_schema, fn -> :ok end)

      BootstrapSchema.run(nil)

      :meck.wait(Hermes.InboxSchemaServer, :list_inboxes, [ ], 1000)
      :meck.wait(Hermes.InboxSchemaServer, :create_schema, [ ], 1000)
      :meck.wait(Hermes.Dispatching.Coordinator, :commence_operation, [ ], 1000)
    end
  end
end
