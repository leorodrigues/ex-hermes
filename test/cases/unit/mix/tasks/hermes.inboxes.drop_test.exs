defmodule Mix.Tasks.Hermes.Inboxes.DropTest do
  use ExUnit.Case

  alias Mix.TaskLib.ServerHandling
  alias Mix.TaskLib.ArgumentParsing
  alias Mix.TaskLib.OutcomeReporting
  alias Hermes.InboxSchemaServer

  @modules [
    ServerHandling, ArgumentParsing, OutcomeReporting, InboxSchemaServer
  ]

  setup do
    :meck.new(@modules)
    on_exit(fn -> :meck.unload end)
  end

  describe "Mix.Tasks.Hermes.Inboxes.Drop" do
    test "Should invoke the server with topic name and inboxe list then succeed" do
      parse_argument = fn _, _ -> {:ok, %{topic: :t, inboxes: []}} end
      invoke_server = fn _, fun -> fun.() end

      :meck.expect(ArgumentParsing, :parse_arguments, parse_argument)
      :meck.expect(InboxSchemaServer, :drop_inboxes, fn _, _ -> :ok end)
      :meck.expect(ServerHandling, :with_server, invoke_server)
      :meck.expect(OutcomeReporting, :report_results, fn _ -> :ok end)

      Mix.Tasks.Hermes.Inboxes.Drop.run([:a, :b, :c])

      :meck.wait(ArgumentParsing, :parse_arguments, [[:a, :b, :c], [:topic]], 1000)
      :meck.wait(InboxSchemaServer, :drop_inboxes, [:t, []], 1000)
      :meck.wait(ServerHandling, :with_server, [InboxSchemaServer, :_], 1000)
      :meck.wait(OutcomeReporting, :report_results, [:ok], 1000)
    end

    test "Should invoke the server with just the topic name and then succeed" do
      parse_argument = fn _, _ -> {:ok, %{topic: :t}} end
      invoke_server = fn _, fun -> fun.() end

      :meck.expect(ArgumentParsing, :parse_arguments, parse_argument)
      :meck.expect(InboxSchemaServer, :drop_inboxes, fn _ -> :ok end)
      :meck.expect(ServerHandling, :with_server, invoke_server)
      :meck.expect(OutcomeReporting, :report_results, fn _ -> :ok end)

      Mix.Tasks.Hermes.Inboxes.Drop.run([:a, :b, :c])

      :meck.wait(ArgumentParsing, :parse_arguments, [[:a, :b, :c], [:topic]], 1000)
      :meck.wait(InboxSchemaServer, :drop_inboxes, [:t], 1000)
      :meck.wait(ServerHandling, :with_server, [InboxSchemaServer, :_], 1000)
      :meck.wait(OutcomeReporting, :report_results, [:ok], 1000)
    end

    test "Should print the failure in the command line args" do
      parse_argument = fn _, _ -> {:error, :fake_error} end

      :meck.expect(ArgumentParsing, :parse_arguments, parse_argument)
      :meck.expect(OutcomeReporting, :report_results, fn _ -> :ok end)

      Mix.Tasks.Hermes.Inboxes.Drop.run([:a, :b, :c])

      :meck.wait(ArgumentParsing, :parse_arguments, [[:a, :b, :c], [:topic]], 1000)
      :meck.wait(OutcomeReporting, :report_results, [{:error, :fake_error}], 1000)
    end
  end
end
