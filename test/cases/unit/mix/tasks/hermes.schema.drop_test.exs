defmodule Mix.Tasks.Hermes.Schema.DropTest do
  use ExUnit.Case

  alias Mix.TaskLib.ServerHandling
  alias Mix.TaskLib.OutcomeReporting
  alias Hermes.InboxSchemaServer

  @modules [
    ServerHandling, OutcomeReporting, InboxSchemaServer
  ]

  setup do
    :meck.new(@modules)
    on_exit(fn -> :meck.unload end)
  end

  describe "Mix.Tasks.Hermes.Schema.Drop" do
    test "Should invoke the server then succeed" do
      invoke_server = fn _, fun -> fun.() end

      :meck.expect(InboxSchemaServer, :drop_schema, fn -> :ok end)
      :meck.expect(ServerHandling, :with_server, invoke_server)
      :meck.expect(OutcomeReporting, :report_results, fn _ -> :ok end)

      Mix.Tasks.Hermes.Schema.Drop.run([:a, :b, :c])

      :meck.wait(InboxSchemaServer, :drop_schema, [], 1000)
      :meck.wait(ServerHandling, :with_server, [InboxSchemaServer, :_], 1000)
      :meck.wait(OutcomeReporting, :report_results, [:ok], 1000)
    end
  end
end
