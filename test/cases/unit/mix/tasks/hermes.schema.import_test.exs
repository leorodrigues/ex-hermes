defmodule Mix.Tasks.Hermes.Schema.ImportTest do
  use ExUnit.Case

  alias Mix.TaskLib.ServerHandling
  alias Mix.TaskLib.ArgumentParsing
  alias Mix.TaskLib.OutcomeReporting
  alias Hermes.InboxSchemaServer

  @modules [
    ServerHandling, ArgumentParsing, OutcomeReporting, InboxSchemaServer
  ]

  setup do
    :meck.new(@modules)
    on_exit(fn -> :meck.unload end)
  end

  describe "Mix.Tasks.Hermes.Schema.Import" do
    test "Should invoke the server then succeed" do
      parse_argument = fn _, _ -> {:ok, %{file: "fake file path"}} end
      invoke_server = fn _, fun -> fun.() end

      :meck.expect(ArgumentParsing, :parse_arguments, parse_argument)
      :meck.expect(InboxSchemaServer, :import_schema, fn _ -> :ok end)
      :meck.expect(ServerHandling, :with_server, invoke_server)
      :meck.expect(OutcomeReporting, :report_results, fn _ -> :ok end)

      Mix.Tasks.Hermes.Schema.Import.run([:a, :b, :c])

      :meck.wait(ArgumentParsing, :parse_arguments, [[:a, :b, :c], [:file]], 1000)
      :meck.wait(InboxSchemaServer, :import_schema, ["fake file path"], 1000)
      :meck.wait(ServerHandling, :with_server, [InboxSchemaServer, :_], 1000)
      :meck.wait(OutcomeReporting, :report_results, [:ok], 1000)
    end
  end
end
