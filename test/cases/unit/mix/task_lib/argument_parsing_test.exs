defmodule Mix.TaskLib.ArgumentParsingTest do
  use ExUnit.Case

  import Mix.TaskLib.ArgumentParsing, only: [parse_arguments: 2]

  describe "Mix.TaskLib.ArgumentParsing.parse_arguments" do
    test "Should return only the default parameters" do
      {:ok, %{env: "prod"}} = parse_arguments([], [])
    end

    test "Should override the environment" do
      {:ok, %{env: "dev"}} = parse_arguments(["-e", "dev"], [])
      {:ok, %{env: "dev"}} = parse_arguments(["--env", "dev"], [])
    end

    test "Should parse the predefined options" do
      args = [
        "--topic", "some topic",
        "--inboxes", "inbox1,inbox2,inbox3",
        "--file", "my_schema.terms"
      ]

      expected_result = {:ok, %{
        env: "prod",
        topic: "some topic",
        inboxes: ["inbox1", "inbox2", "inbox3"],
        file: "my_schema.terms"
      }}

      ^expected_result = parse_arguments(args, [])
    end

    test "Should parse w/o complaints" do
      args = ["--topic", "some topic"]

      expected_result = {:ok, %{env: "prod", topic: "some topic"}}

      ^expected_result = parse_arguments(args, [:topic])
    end

    test "Should complain about the missing argument" do
      args = ["--topic", "some topic"]

      expected_result = {:error, "Parameter ':inboxes' is required."}

      ^expected_result = parse_arguments(args, [:inboxes])
    end
  end
end
