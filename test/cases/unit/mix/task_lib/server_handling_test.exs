defmodule Mix.TaskLib.ServerHandlingest do
  use ExUnit.Case

  import Mix.TaskLib.ServerHandling, only: [with_server: 2]

  setup do
    :meck.new(My.Server, [:non_strict])
    on_exit(fn -> :meck.unload end)
  end

  describe "Mix.TaskLib.ServerHandling.with_server" do
    test "Should wrap the given function w/ server.start and server.stop" do
      func = fn -> apply(My.Server, :some_func, []) end

      :meck.expect(My.Server, :start_link, fn -> :ok end)
      :meck.expect(My.Server, :stop, fn -> :ok end)
      :meck.expect(My.Server, :some_func, fn -> :some_result end)

      :some_result = with_server(My.Server, func)

      :meck.wait(My.Server, :start_link, [], 1000)
      :meck.wait(My.Server, :stop, [], 1000)
      :meck.wait(My.Server, :some_func, [], 1000)
    end
  end
end
