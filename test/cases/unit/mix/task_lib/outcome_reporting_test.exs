defmodule Mix.TaskLib.OutcomeReportingTest do
  use ExUnit.Case

  import ExUnit.CaptureLog

  import Mix.TaskLib.OutcomeReporting, only: [report_results: 1]

  describe "Mix.TaskLib.OutcomeReporting.report_results" do
    test "Should print 'Done' if things are ok" do
      line = capture_log(fn -> report_results(:ok) end)
      true = Regex.match?(~r/Done.*$/s, line)
    end

    test "Should print the error reason" do
      logs = capture_log(fn -> report_results({:error, "some reason"}) end)
      true = Regex.match?(~r/ some reason.*$/s, logs)
    end

    test "Should print just the final message" do
      logs = capture_log(fn -> report_results([]) end)
      true = Regex.match?(~r/^.*No more elements to display.*$/s, logs)
    end

    test "Should print all elements of the list" do
      logs = capture_log(fn -> report_results(["text line"]) end)

      true = Regex.match?(~r/^.*[info].*"text line".*$/s, logs)

      true = Regex.match?(~r/^.*No more elements to display.*$/s, logs)
    end
  end
end
