defmodule Hermes.EnsureSingleDispatchTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :hermes

  alias Hermes.Dispatching.Coordinator

  alias Hermes.Piping.EnsureSingleDispatchListener
  alias Hermes.Piping.EnsureSingleDispatchServer
  alias Hermes.Piping.EnsureSingleDispatchTopics, as: Topics

  alias Hermes.Piping.Test.Tasker

  import Hermes.TestUtils

  require Logger

  @modules_to_spy_on [Coordinator, EnsureSingleDispatchServer]

  @message_count 3

  setup_all_environment do
    Hermes.set_dispatch_on_publish(:single_topic)
    Hermes.set_sleep_interval(1000)
    Process.sleep(Hermes.Dispatching.Coordinator.default_sleep_interval + 100)
    [ ]
  end

  setup ctx do
    Task.Supervisor.start_link(name: Tasker)

    :meck.new(@modules_to_spy_on, [:passthrough])

    bootstrap_hermes()

    on_exit(fn -> :meck.unload end)

    case ctx do
      %{samples_file: f} -> load_samples(ctx, resolve_test_resource_path([f]))
      _ -> :ok
    end
  end

  describe "Hermes" do

    @tag samples_file: "case1.terms"
    test "should dispatch messages only once", %{samples: samples} do
      shutdown_hermes_after(fn ->

        for_each_sample(samples, "command", 1..@message_count, fn cmd ->
          submit_command(cmd, Topics.handle_1)
        end)

        Process.sleep(15500)

        for_each_sample(samples, "payload", 1..@message_count, fn payload ->
          payload
            |> wait_on(EnsureSingleDispatchServer, :handle_1, [payload], 5000)
            |> ensure_call_count(1, EnsureSingleDispatchServer, :handle_1, [payload])

            |> wait_on(EnsureSingleDispatchServer, :handle_2, [payload], 5000)
            |> ensure_call_count(1, EnsureSingleDispatchServer, :handle_2, [payload])

            |> wait_on(EnsureSingleDispatchServer, :handle_3, [payload], 5000)
            |> ensure_call_count(2, EnsureSingleDispatchServer, :handle_3, [payload])

            |> wait_on(EnsureSingleDispatchServer, :handle_4, [payload], 5000)
            |> ensure_call_count(3, EnsureSingleDispatchServer, :handle_4, [payload])

            |> wait_on(EnsureSingleDispatchServer, :handle_5, [payload], 5000)
            |> ensure_call_count(5, EnsureSingleDispatchServer, :handle_5, [payload])
        end)

      end)

    end
  end

  defp bootstrap_hermes do
    Hermes.drop_schema()
    Hermes.create_schema()
    Hermes.commence_operation()

    start_supervised(EnsureSingleDispatchServer)
    EnsureSingleDispatchListener.start_link()


    :meck.wait(Coordinator, :handle_cast, [:commence_operation, :_], 1000)
  end

  defp shutdown_hermes do
    EnsureSingleDispatchListener.stop()
    Hermes.cease_operation()

    :meck.wait(Coordinator, :handle_cast, [:cease_operation, :_], 1000)

    Logger.flush()
  end

  defp shutdown_hermes_after(test_function) do
    try do
      test_function.()
    after
      shutdown_hermes()
    end
  end

end
