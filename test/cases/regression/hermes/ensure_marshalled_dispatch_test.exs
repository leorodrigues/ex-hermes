defmodule Hermes.EnsureMarshalledDispatchTest do
  use ExTestSupport.EnvironmentTools, target_app_name: :hermes

  alias Hermes.Dispatching.Coordinator

  alias Hermes.Piping.Test.Tasker

  alias Hermes.EnsureMarshalledDispatch.CommandListener
  alias Hermes.EnsureMarshalledDispatch.NumberAdder
  alias Hermes.EnsureMarshalledDispatch.Topics

  import Hermes.TestUtils

  require Logger

  @modules_to_spy_on [Coordinator, NumberAdder]

  setup_all_environment do
    Hermes.set_sleep_interval(500)
    Process.sleep(Hermes.Dispatching.Coordinator.default_sleep_interval + 100)
    [ ]
  end

  setup ctx do
    Task.Supervisor.start_link(name: Tasker)

    :meck.new(@modules_to_spy_on, [:passthrough])

    bootstrap_hermes()

    on_exit(fn -> :meck.unload end)

    case ctx do
      %{samples_file: f} -> load_samples(ctx, resolve_test_resource_path([f]))
      _ -> :ok
    end
  end

  describe "Hermes" do
    test "should marshall dispatch attempts when :dispatch_on_publish is :offline" do
      Hermes.set_dispatch_on_publish(:offline)

      shutdown_hermes_after(fn ->
        NumberAdder.set(1)

        submit_command(2, Topics.add_amount)
        submit_command(3, Topics.add_amount)

        Process.sleep(2000)

        :meck.wait(NumberAdder, :add, [2], 5000)
        :meck.wait(NumberAdder, :add, [3], 5000)

        1 = :meck.num_calls(NumberAdder, :add, [2])
        1 = :meck.num_calls(NumberAdder, :add, [3])

        assert NumberAdder.get == 6

      end)
    end

    test "should marshall dispatch attempts when :dispatch_on_publish is :single_topic" do
      Hermes.set_dispatch_on_publish(:single_topic)

      shutdown_hermes_after(fn ->
        NumberAdder.set(0)

        submit_command(1, Topics.add_amount)
        submit_command(1, Topics.add_amount)
        submit_command(1, Topics.add_amount)
        submit_command(1, Topics.add_amount)
        submit_command(1, Topics.add_amount)

        Process.sleep(5500)

        :meck.wait(NumberAdder, :add, [1], 5000)
        5 = :meck.num_calls(NumberAdder, :add, [1])

        assert NumberAdder.get == 5

      end)
    end
  end

  defp bootstrap_hermes do

    Hermes.drop_schema()
    Hermes.create_schema()
    Hermes.commence_operation()

    CommandListener.start_link()

    start_supervised(NumberAdder)

    :meck.wait(Coordinator, :handle_cast, [:commence_operation, :_], 1000)
  end

  defp shutdown_hermes do
    CommandListener.stop()

    stop_supervised(NumberAdder)
    
    Hermes.cease_operation()

    :meck.wait(Coordinator, :handle_cast, [:cease_operation, :_], 1000)

    Logger.flush()
  end

  defp shutdown_hermes_after(test_function) do
    try do
      test_function.()
    after
      shutdown_hermes()
    end
  end

end
