defmodule DispatcherPrototype do
  use GenServer

  require Logger

  alias Hermes.InboxServer
  alias Hermes.Dispatcher.TaskSupervisor, as: Tasker

  import List, only: [keyreplace: 4, keyfind: 3]

  @myself __MODULE__
  def self, do: @myself

  def commence_operation do
    GenServer.cast(@myself, :commence_operation)
  end

  def cease_operation do
    GenServer.cast(@myself, :cease_operation)
  end

  def is_alive do
    GenServer.call(@myself, :is_alive)
  end

  def set_handler(topic_id, inbox_id, fun) do
    GenServer.call(@myself, {:set_handler, {topic_id, inbox_id, fun}})
  end

  def set_handler(topic_id, inbox_id, module_name, function_name) do
    handler_spec = {topic_id, inbox_id, module_name, function_name}
    GenServer.call(@myself, {:set_handler, handler_spec})
  end

  def consume(topic_id, inbox_id) do
    GenServer.cast(@myself, {:consume, topic_id, inbox_id})
  end

  def put(topic_id, inbox_id, payload) do
    InboxServer.put(topic_id, inbox_id, payload)
  end

  def start_link(opts \\ [ ]) do
    GenServer.start_link(__MODULE__, [ ], Keyword.put(opts, :name, @myself))
  end

  def stop do
    GenServer.stop(@myself)
  end

  @impl true
  def init(_init_arg \\ [ ]) do
    {:ok, %{
      running: [ ], handlers: %{ }, sweep_interval: 1, alive: false, refs: [ ]
    }}
  end

  @impl true
  def handle_cast({:consume, topic_id, inbox_id}, state) do
    t = start_consume_task(topic_id, inbox_id, state)
    {:noreply, refadd(t.ref, topic_id, inbox_id, state)}
  end

  @impl true
  def handle_cast(:commence_operation, state) do
    {:noreply, handle_commence_operation(state)}
  end

  @impl true
  def handle_cast(:cease_operation, state) do
    {:noreply, handle_cease_operation(state)}
  end

  @impl true
  def handle_call(:is_alive, _from, %{alive: a} = state) do
    {:reply, a, state}
  end

  @impl true
  def handle_call({:set_handler, handler_spec}, _from, state) do
    {:reply, :ok, handle_set_handler(handler_spec, state)}
  end

  @impl true
  def handle_info(:wake_up, state) do
    {:noreply, state |> sweep_handlers() |> schedule_wake_up_call()}
  end

  @impl true
  def handle_info({reference, {:not_alive, topic_id, inbox_id}}, state) do
    Process.demonitor(reference, [:flush])
    Logger.debug("Dispatcher is not in operation or it was stopped.")
    Logger.debug("Task completed successfuly for inbox {#{topic_id},#{inbox_id}}")
    {:noreply, clear_task(state, topic_id, inbox_id, reference)}
  end

  @impl true
  def handle_info({reference, {:inbox_empty, topic_id, inbox_id}}, state) do
    Process.demonitor(reference, [:flush])
    Logger.debug("No more messages at {#{topic_id},#{inbox_id}}")
    Logger.debug("Task completed successfuly for inbox {#{topic_id},#{inbox_id}}")
    {:noreply, clear_task(state, topic_id, inbox_id, reference)}
  end

  # Because the task was demonitored in the previous handler (the success case),
  # this handler will only run if the task was interrupted by a failure.
  @impl true
  def handle_info({:DOWN, reference, :process, _, reason}, state) do
    Logger.warning("Task failed with reason #{inspect(reason)}")
    {:noreply, handle_task_failure(reference, state)}
  end

  @impl true
  def handle_info(msg, state) do
    Logger.debug("Something came up: #{inspect(msg)}")
    {:noreply, state}
  end

  defp handle_set_handler(handler_spec, %{handlers: h_map} = state) do
    Map.put(state, :handlers, puthandler(h_map, handler_spec))
  end

  defp handle_task_failure(reference, %{refs: r_list} = state) do
    if not List.keymember?(r_list, reference, 0) do
      state
    else
      {_, {topic_id, inbox_id}} = keyfind(r_list, reference, 0)
      Logger.debug("Replacing task for {#{topic_id}},{#{inbox_id}}")

      t = start_consume_task(topic_id, inbox_id, state)
      refreplace(reference, t.ref, topic_id, inbox_id, state)
    end
  end

  defp handle_commence_operation(%{alive: true} = state) do
    Logger.warning("Already running")
    state
  end

  defp handle_commence_operation(state) do
    Logger.info("Commencing operation")
    schedule_wake_up_call(Map.put(state, :alive, true))
  end

  defp handle_cease_operation(%{alive: false} = state) do
    Logger.warning("Already stopped")
    state
  end

  defp handle_cease_operation(state) do
    Logger.info("Ceasing operation")
    Map.put(state, :alive, false)
  end

  defp schedule_wake_up_call(%{alive: false} = state) do
    Logger.debug("Scheduling cycle stopped.")
    state
  end

  defp schedule_wake_up_call(%{sweep_interval: s, timer: t} = state) do
    Process.cancel_timer(t)
    timer = Process.send_after(@myself, :wake_up, :timer.seconds(s))
    Map.put(state, :timer, timer)
  end

  defp schedule_wake_up_call(%{sweep_interval: s} = state) do
    timer = Process.send_after(@myself, :wake_up, :timer.seconds(s))
    Map.put(state, :timer, timer)
  end

  defp sweep_handlers(%{handlers: h_map, running: r_list} = state) do
    Logger.debug("Sweeping handlers...")
    all_names = Map.keys(h_map)
    not_running = Enum.filter(all_names, fn n -> n not in r_list end)
    new_state = Map.put(state, :running, all_names)
    Enum.each(not_running, fn {t, q} -> consume(t, q) end)
    new_state
  end

  def run_consume(topic_id, inbox_id, %{ } = handler_map) do
    Process.sleep(1000)
    case try_taking_next(topic_id, inbox_id) do
      :inbox_empty -> {:inbox_empty, topic_id, inbox_id}
      :not_alive -> {:not_alive, topic_id, inbox_id}
      message ->
        handler = Map.get(handler_map, {topic_id, inbox_id})
        dispatch(message, topic_id, inbox_id, handler)
        run_consume(topic_id, inbox_id, handler_map)
    end
  end

  def dispatch(message, topic_id, inbox_id, {module_name, function_name}) do
    apply(module_name, function_name, [topic_id, inbox_id, message])
  end

  def dispatch(message, topic_id, inbox_id, fun) do
    fun.(topic_id, inbox_id, message)
  end

  defp try_taking_next(topic_id, inbox_id) do
    case is_alive() do
      true -> InboxServer.take(topic_id, inbox_id)
      false -> :not_alive
    end
  end

  defp clear_task(state, topic_id, inbox_id, reference) do
    state |> clear_running(topic_id, inbox_id) |> clear_ref(reference)
  end

  defp clear_running(%{running: running} = state, topic_id, inbox_id) do
    key = {topic_id, inbox_id}
    Map.put(state, :running, Enum.filter(running, fn n -> n !== key end))
  end

  defp clear_ref(%{refs: r_list} = state, reference) do
    {_, new_ref_list} = List.keytake(r_list, reference, 0)
    Map.put(state, :refs, new_ref_list)
  end

  defp refadd(reference, topic_id, inbox_id, %{refs: r_list} = state) do
    Map.put(state, :refs, [{reference, {topic_id, inbox_id}}|r_list])
  end

  defp refreplace(old_ref, new_ref, topic_id, inbox_id, %{refs: r} = state) do
    new_ref_list = keyreplace(r, old_ref, 0, {new_ref, {topic_id, inbox_id}})
    Map.put(state, :refs, new_ref_list)
  end

  defp start_consume_task(topic_id, inbox_id, %{handlers: h_map}) do
    arguments = [topic_id, inbox_id, h_map]
    Task.Supervisor.async_nolink(Tasker, __MODULE__, :run_consume, arguments)
  end

  defp puthandler(handler_map, {topic_id, inbox_id, module_name, f_name}) do
    Map.put(handler_map, {topic_id, inbox_id}, {module_name, f_name})
  end

  defp puthandler(handler_map, {topic_id, inbox_id, fun}) do
    Map.put(handler_map, {topic_id, inbox_id}, fun)
  end
end
