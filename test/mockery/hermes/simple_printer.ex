defmodule Mockery.SimplePrinter do
  require Logger

  def print(term) do
    Logger.info(inspect(term))
  end

  def print(topic_id, name, message) do
    Logger.info("Message from inbox #{inspect(topic_id)} #{inspect(name)}: #{inspect(message)}")
  end
end
