defmodule Hermes.Piping.EnsureSingleDispatchListener do
  use Hermes.Piping.Listener,
    supervisor: Hermes.Piping.Test.Tasker,
    pipeline: Hermes.Piping.Inbound.VoidCommandPipeline

  alias Hermes.Piping.EnsureSingleDispatchServer, as: Server
  alias Hermes.Piping.EnsureSingleDispatchTopics, as: Topics

  listen_to Topics.handle_1, p, do: Server.handle_1(p)

  listen_to Topics.handle_2, p, do: Server.handle_2(p)

  listen_to Topics.handle_3, p, do: Server.handle_3(p)

  listen_to Topics.handle_4, p, do: Server.handle_4(p)

  listen_to Topics.handle_5, p, do: Server.handle_5(p)
end
