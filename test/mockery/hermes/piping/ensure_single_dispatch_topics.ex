defmodule Hermes.Piping.EnsureSingleDispatchTopics do

  @domain "test.hermes"

  @app_name :ensure_single_dispatch

  import Hermes.DynamicTopics

  def handle_1, do: compute_command_topic(@domain, @app_name, :handle_1)
  
  def handle_2, do: compute_command_topic(@domain, @app_name, :handle_2)
  
  def handle_3, do: compute_command_topic(@domain, @app_name, :handle_3)
  
  def handle_4, do: compute_command_topic(@domain, @app_name, :handle_4)
  
  def handle_5, do: compute_command_topic(@domain, @app_name, :handle_5)
end
