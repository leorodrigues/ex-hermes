defmodule Hermes.Piping.DummyListener do
  use Hermes.Piping.Listener,
    supervisor: Hermes.Piping.Test.Tasker,
    pipeline: Hermes.Piping.Inbound.VoidCommandPipeline

  alias Hermes.Piping.DummyTopics, as: Dt

  require Logger

  listen_to Dt.dynamic, p do
    Logger.info(comment: "Message received", topic: Dt.dynamic, payload: p)
  end

  listen_to "test/hermes/piping/listener/matching-payload", %{content: c} do
    Logger.info(comment: "Matching payload received", content: c)
  end

  listen_to "test/hermes/piping/listener/opaque-payload", p do
    Logger.info(comment: "Opaque payload received", payload: p)
  end

  listen_to "test/hermes/piping/listener/matching-options", p, %{call_depth: d} do
    Logger.info(comment: "Matching options received", payload: p, depth: d)
  end

  listen_to "test/hermes/piping/listener/opaque-options", p, o do
    Logger.info(comment: "Opaque options received", payload: p, options: o)
  end
end
