defmodule Hermes.Piping.EnsureSingleDispatchServer do
  use GenServer

  alias Hermes.Piping

  import Hermes.DynamicTopics

  require Logger

  @domain "test.hermes"

  @app_name :ensure_single_dispatch

  @self __MODULE__

  def handle_1(p) do
    GenServer.call(@self, {:one, p})
  end

  def handle_2(p) do
    GenServer.call(@self, {:two, p})
  end

  def handle_3(p) do
    GenServer.call(@self, {:three, p})
  end

  def handle_4(p) do
    GenServer.call(@self, {:four, p})
  end

  def handle_5(p) do
    GenServer.call(@self, {:five, p})
  end

  def start_link(_opts \\ [ ]) do
    GenServer.start_link(@self, [ ], name: @self)
  end

  @impl true
  def init(_args \\ [ ]) do
    {:ok, nil}
  end

  @impl true
  def handle_call({:one, p}, _from, state) do
    Logger.info(comment: "single dispatch handle_1", payload: p)
    wait_a_little()
    Piping.submit_void_command(compute_command_topic(@domain, @app_name, :handle_2), p)
    Piping.submit_void_command(compute_command_topic(@domain, @app_name, :handle_3), p)
    Piping.submit_void_command(compute_command_topic(@domain, @app_name, :handle_4), p)
    {:reply, :ok, state}
  end

  @impl true
  def handle_call({:two, p}, _from, state) do
    Logger.info(comment: "single dispatch handle_2", payload: p)
    Piping.submit_void_command(compute_command_topic(@domain, @app_name, :handle_3), p)
    {:reply, :ok, state}
  end

  @impl true
  def handle_call({:three, p}, _from, state) do
    Logger.info(comment: "single dispatch handle_3", payload: p)
    wait_a_little()
    Piping.submit_void_command(compute_command_topic(@domain, @app_name, :handle_4), p)
    Piping.submit_void_command(compute_command_topic(@domain, @app_name, :handle_5), p)
    {:reply, :ok, state}
  end

  @impl true
  def handle_call({:four, p}, _from, state) do
    Logger.info(comment: "single dispatch handle_4", payload: p)
    wait_a_little()
    Piping.submit_void_command(compute_command_topic(@domain, @app_name, :handle_5), p)
    {:reply, :ok, state}
  end

  @impl true
  def handle_call({:five, p}, _from, state) do
    Logger.info(comment: "single dispatch handle_5", payload: p)
    wait_a_little()
    {:reply, :ok, state}
  end

  defp wait_a_little, do: wait_a_little(random_fib() * 92)

  defp wait_a_little(millis) do
    Logger.info(comment: "sleeping for a while", millis: millis)
    Process.sleep(millis)
  end

  defp random_fib, do: Enum.at([2,3,5,8], :rand.uniform(4) - 1)
end
