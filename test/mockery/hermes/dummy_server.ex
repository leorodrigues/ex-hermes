defmodule DummyServer do
  use GenServer

  require Logger

  @self __MODULE__
  def self, do: @self

  def start_link(_opts \\ [ ]) do
    GenServer.start_link(__MODULE__, [ ], name: @self)
  end

  def init(_args \\ [ ]) do
    {:ok, []}
  end

  def handle_cast(request, state) do
    Logger.info([comment: "Faking cast...", request: request])
    {:noreply, state}
  end
end
