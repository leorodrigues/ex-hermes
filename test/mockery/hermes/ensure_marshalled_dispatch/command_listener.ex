defmodule Hermes.EnsureMarshalledDispatch.CommandListener do
  use Hermes.Piping.Listener,
    supervisor: Hermes.Piping.Test.Tasker,
    pipeline: Hermes.Piping.Inbound.VoidCommandPipeline

  alias Hermes.EnsureMarshalledDispatch.NumberAdder
  alias Hermes.EnsureMarshalledDispatch.Topics

  listen_to Topics.add_amount, number, do: NumberAdder.add(number)
end
