defmodule Hermes.EnsureMarshalledDispatch.Topics do

  import Hermes.DynamicTopics

  @domain "test.hermes"

  @app_name :ensure_marshalled_dispatch

  def add_amount, do: compute_command_topic(@domain, @app_name, :add_amount)
end
