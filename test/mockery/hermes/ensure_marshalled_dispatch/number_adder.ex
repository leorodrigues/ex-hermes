defmodule Hermes.EnsureMarshalledDispatch.NumberAdder do
  use GenServer

  @self __MODULE__

  @fib_sequence [1, 2, 3, 5, 8]

  def get do
    GenServer.call(@self, :get)
  end

  def set(number) do
    GenServer.call(@self, {:set, number})
  end

  def add(number) do
    GenServer.call(@self, {:add, number})
  end

  def start_link(_opts \\ [ ]) do
    GenServer.start_link(@self, [ ], name: @self)
  end

  def stop do
    GenServer.stop(@self, :normal)
  end

  @impl true
  def init(_args \\ [ ]) do
    {:ok, 0}
  end

  @impl true
  def handle_call(:get, _from, state) do
    {:reply, state, state}
  end

  @impl true
  def handle_call({:set, number}, _from, state) do
    {:reply, state, number}
  end

  @impl true
  def handle_call({:add, number}, _from, state) do
    wait_a_while()
    {:reply, state, number + state}
  end

  defp wait_a_while do
    Process.sleep(random_fib() * 100)
  end

  defp random_fib do
    Enum.at(@fib_sequence, :rand.uniform(Enum.count(@fib_sequence)) - 1)
  end
end
