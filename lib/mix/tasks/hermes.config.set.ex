defmodule Mix.Tasks.Hermes.Config.Set do
  @moduledoc """
  Sets hermes variables.
  """
  @shortdoc "Set hermes variables."
  @requirements ["app.config"]

  use Mix.Task

  require Logger

  alias Hermes.Topic
  alias Hermes.Dispatching.Coordinator
  alias Hermes.Configuration

  import Mix.TaskLib.ServerHandling
  import Mix.TaskLib.ArgumentParsing
  import Mix.TaskLib.OutcomeReporting

  @impl Mix.Task
  def run(args) do
    parse_arguments(args, [ ]) |> IO.inspect()
      |> handle_set()
      |> report_results()
  end

  defp handle_set({:error, _} = e), do: e

  defp handle_set({:ok, options}) do
    with_server(Configuration, fn ->
      set_and_collect([ ], options, :dispatch_on_publish, &set_dispatch/1)
        |> set_and_collect(options, :sleep_interval, &set_sleep_interval/1)
        |> report_results()
    end)
  end

  defp set_dispatch(value) do
    Topic.set_dispatch_on_publish(String.to_atom(value))
  end

  def set_sleep_interval(value) do
    Coordinator.set_sleep_interval(value)
  end

  def set_and_collect(results, options, key, set) do
    case Map.get(options, key) do
      nil -> results
      value -> Keyword.put(results, key, set.(value))
    end
  end
end
