defmodule Mix.Tasks.Hermes.Inboxes.List do
  @moduledoc """
  Display the current schema.
  """
  @shortdoc "Display the current schema."
  @requirements ["app.config"]

  use Mix.Task

  require Logger

  alias Hermes.InboxSchemaServer

  import Mix.TaskLib.ServerHandling, only: [with_server: 2]
  import Mix.TaskLib.OutcomeReporting, only: [report_results: 1]

  @impl Mix.Task
  def run(_) do
    list_inboxes = fn -> InboxSchemaServer.list_inboxes() end
    with_server(InboxSchemaServer, list_inboxes) |> report_results()
  end
end
