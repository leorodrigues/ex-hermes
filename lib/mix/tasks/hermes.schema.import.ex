defmodule Mix.Tasks.Hermes.Schema.Import do
  @moduledoc """
  Imports a schema replacing the current one.
  """
  @shortdoc "Imports a schema file."
  @requirements ["app.config"]

  @required_args [:file]

  use Mix.Task

  require Logger

  alias Hermes.InboxSchemaServer

  import Mix.TaskLib.ServerHandling, only: [with_server: 2]
  import Mix.TaskLib.ArgumentParsing, only: [parse_arguments: 2]
  import Mix.TaskLib.OutcomeReporting, only: [report_results: 1]

  @impl Mix.Task
  def run(args) do
    parse_arguments(args, @required_args)
      |> handle_import_schema()
      |> report_results()
  end

  defp handle_import_schema({:ok, %{file: file_path}}) do
    import_schema = fn -> InboxSchemaServer.import_schema(file_path) end
    with_server(InboxSchemaServer, import_schema)
  end
end
