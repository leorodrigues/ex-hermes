defmodule Mix.Tasks.Hermes.Schema.Drop do
  @moduledoc """
  Drops the current Hermes schema.
  """
  @shortdoc "Drops the current Hermes schema."
  @requirements ["app.config"]

  use Mix.Task

  require Logger

  alias Hermes.InboxSchemaServer

  import Mix.TaskLib.ServerHandling, only: [with_server: 2]
  import Mix.TaskLib.OutcomeReporting, only: [report_results: 1]

  @impl Mix.Task
  def run(_) do
    drop_schema = fn -> InboxSchemaServer.drop_schema() end
    with_server(InboxSchemaServer, drop_schema) |> report_results()
  end
end
