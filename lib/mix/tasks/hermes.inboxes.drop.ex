defmodule Mix.Tasks.Hermes.Inboxes.Drop do
  @moduledoc """
  Removes inboxes under a certain topic or a topic entirely.
  """
  @shortdoc "Drop inboxes from Hermes."
  @requirements ["app.config"]

  use Mix.Task

  require Logger

  alias Hermes.InboxSchemaServer

  import Mix.TaskLib.ServerHandling, only: [with_server: 2]
  import Mix.TaskLib.ArgumentParsing, only: [parse_arguments: 2]
  import Mix.TaskLib.OutcomeReporting, only: [report_results: 1]

  @required_args [:topic]

  @impl Mix.Task
  def run(args) do
    parse_arguments(args, @required_args)
      |> handle_drop_inboxes()
      |> report_results()
  end

  defp handle_drop_inboxes({:error, _} = e), do: e

  defp handle_drop_inboxes({:ok, %{topic: t, inboxes: i}}) do
    drop_inboxes = fn -> InboxSchemaServer.drop_inboxes(t, i) end
    with_server(InboxSchemaServer, drop_inboxes)
  end

  defp handle_drop_inboxes({:ok, %{topic: t}}) do
    drop_inboxes = fn -> InboxSchemaServer.drop_inboxes(t) end
    with_server(InboxSchemaServer, drop_inboxes)
  end
end
