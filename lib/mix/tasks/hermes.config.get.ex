defmodule Mix.Tasks.Hermes.Config.Get do
  @moduledoc """
  Gets hermes variables.
  """
  @shortdoc "Get hermes variables."
  @requirements ["app.config"]

  use Mix.Task

  require Logger

  alias Hermes.Topic
  alias Hermes.Dispatching.Coordinator
  alias Hermes.Configuration

  import Mix.TaskLib.ServerHandling
  import Mix.TaskLib.ArgumentParsing
  import Mix.TaskLib.OutcomeReporting

  @required_args [:name]

  @impl Mix.Task
  def run(args) do
    parse_arguments(args, @required_args)
      |> handle_get()
      |> report_results()
  end

  defp handle_get({:error, _} = e), do: e

  defp handle_get({:ok, %{name: name}}) do
    with_server(Configuration, fn ->
      case name do
        "dispatch-on-publish" -> inspect(Topic.get_dispatch_on_publish())
        "sleep-interval" -> to_string(Coordinator.get_sleep_interval())
        _ -> "unknown"
      end
    end)
  end
end
