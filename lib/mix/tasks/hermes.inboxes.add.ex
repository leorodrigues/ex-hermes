defmodule Mix.Tasks.Hermes.Inboxes.Add do
  @moduledoc """
  Creates one or more inboxes under a certain topic.
  """
  @shortdoc "Add inboxes to Hermes."
  @requirements ["app.config"]

  use Mix.Task

  require Logger

  alias Hermes.InboxSchemaServer

  import Mix.TaskLib.ServerHandling, only: [with_server: 2]
  import Mix.TaskLib.ArgumentParsing, only: [parse_arguments: 2]
  import Mix.TaskLib.OutcomeReporting, only: [report_results: 1]

  @required_args [:topic, :inboxes]

  @impl Mix.Task
  def run(args) do
    parse_arguments(args, @required_args)
      |> handle_add_inboxes()
      |> report_results()
  end

  defp handle_add_inboxes({:error, _} = e), do: e

  defp handle_add_inboxes({:ok, %{topic: t, inboxes: i}}) do
    add_inboxes = fn -> InboxSchemaServer.add_inboxes(t, i) end
    with_server(InboxSchemaServer, add_inboxes)
  end
end
