defmodule Mix.Tasks.Hermes.Schema.Create do
  @moduledoc """
  Creates an empty Hermes schema.
  """
  @shortdoc "Creates an empty schema."
  @requirements ["app.config"]

  use Mix.Task

  require Logger

  alias Hermes.InboxSchemaServer

  import Mix.TaskLib.ServerHandling, only: [with_server: 2]
  import Mix.TaskLib.OutcomeReporting, only: [report_results: 1]

  @impl Mix.Task
  def run(_) do
    create_schema = fn -> InboxSchemaServer.create_schema() end
    with_server(InboxSchemaServer, create_schema) |> report_results()
  end
end
