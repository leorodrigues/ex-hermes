defmodule Mix.TaskLib.ServerHandling do
  def with_server(server_module, action) do
    apply(server_module, :start_link, [])
    result = action.()
    apply(server_module, :stop, [])
    result
  end
end
