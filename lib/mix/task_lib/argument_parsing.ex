defmodule Mix.TaskLib.ArgumentParsing do
  @parse_options [
    switches: [
      env: :string,
      topic: :string,
      inboxes: :string,
      file: :string,
      name: :string,
      dispatch_on_publish: :string,
      sleep_interval: :integer
    ],
    aliases: [e: :env, t: :topic, i: :inboxes, f: :file]
  ]

  @defaults %{env: "prod"}

  def parse_arguments(args, requirements \\ [ ]) do
    OptionParser.parse(args, @parse_options)
      |> collect_options()
      |> confirm_required_options(requirements)
  end

  defp collect_options({parsed, _, _}) do
    Enum.reduce(parsed, @defaults, &collect_parameter/2)
  end

  defp collect_parameter({:inboxes, inbox_list}, map) do
    Map.put(map, :inboxes, String.split(inbox_list, ","))
  end

  defp collect_parameter({key, value}, map) do
    Map.put(map, key, value)
  end

  defp confirm_required_options(%{ } = opts, [ ]), do: {:ok, opts}

  defp confirm_required_options(%{ } = opts, [current|reminder]) do
    case Map.has_key?(opts, current) do
      false -> {:error, "Parameter '#{inspect(current)}' is required."}
      true -> confirm_required_options(opts, reminder)
    end
  end
end
