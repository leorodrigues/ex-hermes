defmodule Mix.TaskLib.OutcomeReporting do
  require Logger

  def report_results(:ok), do: Logger.info("Done")

  def report_results({:error, reason}), do: Logger.error(reason)

  def report_results([ ]) do
    Logger.info("No more elements to display.")
  end

  def report_results([element|remaining]) do
    Logger.info(inspect(element))
    report_results(remaining)
  end

  def report_results(message) do
    Logger.info(message)
  end
end
