defmodule Hermes.Piping.Listener do

  require Logger

  alias Hermes.Piping

  @pipeline_key :pipeline

  @default_pipeline Hermes.Piping.Inbound.RunBusinessTargetPipe

  @missing_supervisor_msg "Missing option :supervisor"

  @cannot_use_local_function_msg "Cannot use local function."
    <> " The module is not compiled yet."

  @cannot_use_attribute_msg "Cannot use attribute."
    <> " The module is not compiled yet."

  @type selector :: :command | :reply | :event

  @type listening_topic :: String.t | atom

  @type command_name :: String.t | atom

  @type code_block :: [{:do, any}, ...]

  @type handling_code_block :: {:__block__, [ ], [
    {:=, [ ], [...]} | {:__block__, [ ], [...]}, ...
  ]}

  defmacro __using__(options \\ [ ]) do

    unless Keyword.has_key?(options, :supervisor) do
      raise RuntimeError, @missing_supervisor_msg
    end

    inbox = make_inbox(Keyword.get(options, :inbox), __CALLER__.module)

    supervisor = Keyword.get(options, :supervisor)

    pipeline = Keyword.get(options, @pipeline_key, @default_pipeline)

    quote do
      use GenServer

      import Hermes.Piping.Listener, only: [listen_to: 3, listen_to: 4]

      require Logger

      @handling_msg "Handling message at entry-point."

      @registration_msg "Registering entry-point."

      @unregistration_msg "Unregistering entry-point."

      @starting_link_msg "Starting link."

      @stopping_link_msg "Stopping link."

      @running_business "Running business logic."

      @pipeline unquote(pipeline)

      @supervisor unquote(supervisor)

      @inbox unquote(inbox)

      @myself __MODULE__

      Module.register_attribute(__MODULE__, :entry_points, accumulate: true)
      @before_compile Hermes.Piping.Listener

      def start_link(opts \\ [ ]) do
        Logger.debug(comment: @starting_link_msg, inbox_id: @inbox)
        args = Keyword.get(opts, :init_args, [ ])
        GenServer.start_link(__MODULE__, args, name: @myself)
      end

      def stop do
        Logger.debug(comment: @stopping_link_msg, inbox_id: @inbox)
        GenServer.stop(@myself)
      end

      @impl true
      def init(args \\ [ ]) do
        register_entrypoints()
        {:ok, Enum.into(args, %{ })}
      end

      @impl true
      def terminate(_, _) do
        unregister_entrypoints()
      end

      @impl true
      def handle_call({:respond_to, operation, payload, opts}, _from, state) do
        add_business(opts, operation)
          |> add_state(state)
          |> run(payload)
          |> reply(state)
      end
    end
  end

  @spec listen_to(listening_topic, any, code_block) :: any
  defmacro listen_to(topic, payload, do: block) do
    make_business_code_block(expand(topic, __CALLER__), block, payload)
  end

  @spec listen_to(listening_topic, any, Keyword.t, code_block) :: any
  defmacro listen_to(topic, payload, options, do: block) do
    make_business_code_block(expand(topic, __CALLER__), block, payload, options)
  end

  defp make_business_code_block(topic, block, business_payload) do
    entry_point_name = compute_entry_point_name(topic)
    business_name = compute_business_name(topic)

    quote do
      @entry_points {
        unquote(topic),
        @inbox,
        unquote(entry_point_name),
        unquote(business_name)
      }

      def unquote(business_name)(unquote(business_payload), _) do
        Logger.debug([
          comment: @running_business,
          topic_id: unquote(topic),
          inbox_id: @inbox,
          name: unquote(business_name)
        ])

        unquote(block)
      end
    end
  end

  defp make_business_code_block(topic, block, bus_payload, bus_options) do
    entry_point_name = compute_entry_point_name(topic)
    business_name = compute_business_name(topic)

    quote do
      @entry_points {
        unquote(topic),
        @inbox,
        unquote(entry_point_name),
        unquote(business_name)
      }

      def unquote(business_name)(unquote(bus_payload), unquote(bus_options)) do
        Logger.debug([
          comment: @running_business,
          topic_id: unquote(topic),
          inbox_id: @inbox,
          name: unquote(business_name)
        ])

        unquote(block)
      end
    end
  end

  defp make_entry_point_code_block({topic, _, entry_point_name, buss_name}) do
    quote do
      def unquote(entry_point_name)(_, _, payload, options \\ [ ]) do
        Logger.debug([
          comment: @handling_msg,
          topic_id: unquote(topic),
          inbox_id: @inbox,
          name: unquote(entry_point_name)
        ])

        GenServer.call(__MODULE__, {
          :respond_to, unquote(buss_name), payload, options
        })
      end
    end
  end

  defmacro __before_compile__(env) do
    main_block = quote do
      alias Hermes.AsyncHandling
      alias Hermes.Piping

      defp register_entrypoints do
        register_entrypoints(@entry_points)
      end

      defp register_entrypoints([ ]), do: :ok
      defp register_entrypoints([{topic, inbox, name, _}|remaining]) do
        Logger.debug([
          comment: @registration_msg,
          topic_id: topic,
          inbox_id: inbox,
          name: name
        ])

        Piping.subscribe_pipeline_handling(topic, inbox, __MODULE__, name)

        register_entrypoints(remaining)
      end

      defp unregister_entrypoints do
        unregister_entrypoints(@entry_points)
      end

      defp unregister_entrypoints([ ]), do: :ok
      defp unregister_entrypoints([{topic, inbox, name, _}|remaining]) do
        Logger.debug(
          comment: @unregistration_msg,
          topic_id: topic,
          inbox_id: inbox,
          name: name
        )

        Piping.unsubscribe_pipeline_handling(topic, inbox)

        unregister_entrypoints(remaining)
      end

      defp run(options, payload) do
        AsyncHandling.run_task(@supervisor, @pipeline, :convey, [
          payload, options
        ])
      end

      defp reply(result, state), do: {:reply, result, state}

      defp add_business(options, function_name) do
        Keyword.put(options, :business_target, {__MODULE__, function_name})
      end

      defp add_state(options, state) do
        Keyword.put(options, :state, state)
      end
    end

    entry_point_blocks = Module.get_attribute(env.module, :entry_points)
      |> Enum.uniq() |> Enum.map(&make_entry_point_code_block/1)

    [main_block|entry_point_blocks]
  end

  defp make_inbox(nil, module), do: module |> to_kebab_case()
  defp make_inbox(inbox, _), do: inbox

  defp to_kebab_case(module) do
    module
      |> to_string()
      |> replace(~r/Elixir\./u, "")
      |> replace(~r/(.)([A-Z])/u, "\\1-\\2")
      |> replace(~r/\./u, "")
      |> String.downcase()
  end

  defp compute_entry_point_name(topic) do
    "respond_to_#{make_snake_case(topic)}"
      |> remove_repeated_dash()
      |> to_atom()
  end

  defp compute_business_name(topic) do
    "run_#{make_snake_case(topic)}"
      |> remove_repeated_dash()
      |> to_atom()
  end

  defp make_snake_case(input) do
    to_string(input)
      |> replace(~r/[^[:alnum:]]/u, "_")
      |> remove_repeated_dash()
  end

  defp remove_repeated_dash(input) do
    input |> replace(~r/[_]+/u, "_")
  end

  defp replace(input, regex, replacement) do
    Regex.replace(regex, input, replacement)
  end

  defp to_atom(input), do: :"#{input}"

  defp expand({{:., _, _}, _, _} = param, env) do
    {result, _} = Code.eval_quoted(param, [ ], env); result
  end

  defp expand({:@, info, _}, _) do
    throw {:error, info, @cannot_use_attribute_msg}
  end

  defp expand({:., _, _} = param, env) do
    Macro.expand(param, env)
  end

  defp expand({:__aliases__, _, _} = param, env) do
    Macro.expand(param, env)
  end

  defp expand({a, info, _}, _) when is_atom(a) do
    throw {:error, info, @cannot_use_local_function_msg}
  end

  defp expand(param, env) do
    Macro.expand(param, env)
  end
end
