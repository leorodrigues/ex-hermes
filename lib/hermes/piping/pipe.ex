defmodule Hermes.Piping.Pipe do
  @valid_merge_strategies [
    :override_runtime,
    :override_compile,
    :ignore_runtime
  ]

  @merge_key :opts_merge_strategy

  @invalid_strategy "Invalid merge strategy:"

  defmacro __using__(opts \\ []) do
    merge_strategy = Keyword.get(opts, @merge_key, :override_compile)

    unless merge_strategy in @valid_merge_strategies do
      raise RuntimeError, "#{@invalid_strategy} #{inspect(merge_strategy)}"
    end

    quote do
      @opts_merge_strategy unquote(merge_strategy)

      import Hermes.Piping.Pipe, only: [pipe: 1, pipe: 2, halt: 1, terminate: 1]

      Module.register_attribute(__MODULE__, :pipes, accumulate: true)
      @before_compile Hermes.Piping.Pipe
    end
  end

  defmacro __before_compile__(env) do
    pipes = Module.get_attribute(env.module, :pipes)
      |> expand_pipes([ ])

    strategy = Module.get_attribute(env.module, :opts_merge_strategy)

    quoted_invocation = compute_invocation(strategy)

    get_call_depth = quote do Keyword.get(opts, :call_depth, 0) end

    quote do
      def convey(payload, opts \\ [ ]) do
        pre_run(payload, opts, unquote(get_call_depth), unquote(pipes))
      end

      defp supress_halt({:halt, reason}), do: reason
      defp supress_halt(value), do: value

      defp pre_run(payload, opts, 0, pipes) do
        run(payload, Keyword.put(opts, :call_depth, 1), pipes) |> supress_halt()
      end

      defp pre_run(payload, opts, depth, pipes) do
        run(payload, Keyword.put(opts, :call_depth, depth + 1), pipes)
      end

      defp run({:halt, _} = payload, _, _), do: payload
      defp run({:terminate, payload}, _, _), do: payload
      defp run(payload, _, [ ]), do: payload
      defp run(payload, runtime_opts, [{function, compile_opts}|remaining]) do
        unquote(quoted_invocation) |> run(runtime_opts, remaining)
      end

      defoverridable convey: 1, convey: 2
    end
  end

  defmacro pipe(name, opts \\ [ ]) do
    convey_opts = Keyword.get(opts, :convey_opts, [ ])
    a_pipe = expand_alias(name, convey_opts, __CALLER__)
    quote do @pipes unquote(a_pipe) end
  end

  defmacro halt(reason) do
    quote do {:halt, unquote(reason)} end
  end

  defmacro terminate(payload) do
    quote do {:terminate, unquote(payload)} end
  end

  defp compute_invocation(:ignore_runtime) do
    quote do function.(payload, compile_opts) end
  end

  defp compute_invocation(strategy) do
    merge_call = compute_merge_call(strategy)
    quote do function.(payload, unquote(merge_call)) end
  end

  defp compute_merge_call(:override_runtime) do
    quote do Keyword.merge(runtime_opts, compile_opts) end
  end

  defp compute_merge_call(:override_compile) do
    quote do Keyword.merge(compile_opts, runtime_opts) end
  end

  defp expand_alias({:__aliases__, _, _} = alias, opts, environment) do
    name = Macro.expand(alias, environment)
    quote do {:mod, unquote(name), unquote(opts)} end
  end

  defp expand_alias(name, opts, _) when is_atom(name) do
    quote do {:fun, unquote(name), unquote(opts)} end
  end

  defp expand_pipes([ ], result) do
    quote do [unquote_splicing(result)] end
  end

  defp expand_pipes([pipe|remaining], result) do
    expand_pipes(remaining, [make_reference(pipe)|result])
  end

  defp make_reference({type, name, opts}) do
    make_invocation(type, name, opts)
  end

  defp make_invocation(:mod, name, opts) do
    quote do {&unquote(name).convey/2, unquote(opts)} end
  end

  defp make_invocation(:fun, name, opts) do
    quote do {&__place_holder__/2, unquote(opts)} end |> Macro.prewalk(fn
      {:__place_holder__, meta, args} -> {name, meta, args}
      other -> other
    end)
  end
end
