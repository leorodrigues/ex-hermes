defmodule Hermes.Piping.Outbound.PublishReplyPipe do
  use Hermes.Piping.Pipe

  import Hermes.Domain.Context

  require Logger

  @comment "Publishing message."

  pipe :publish_message

  defp publish_message(message, _options) do
    Logger.debug(comment: @comment, message: message)
    Hermes.publish(get_reply_topic_from_process(), message)
  end
end
