defmodule Hermes.Piping.Outbound.VoidCommandSubmission do
  use Hermes.Piping.Pipe

  require Logger

  @submitting_void_command_msg "Submitting a new void command."

  pipe :log_start_of_process
  pipe Hermes.Piping.Outbound.MakeCommandPipe

  defp log_start_of_process(envelope, _options) do
    Logger.debug(comment: @submitting_void_command_msg, envelope: envelope)
    envelope
  end
end
