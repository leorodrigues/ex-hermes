defmodule Hermes.Piping.Outbound.MakeEventPipe do
  use Hermes.Piping.Pipe

  import Hermes.Domain.Context

  require Logger

  @comment "Making an event."

  pipe :wrap_content

  defp wrap_content(content, options) do
    Logger.debug(comment: @comment, content: content, opts: options)
    make_event(content, options)
  end
end
