defmodule Hermes.Piping.Outbound.MakeCommandPipe do
  use Hermes.Piping.Pipe

  import Hermes.Domain.Context

  require Logger

  @comment "Making a command."

  pipe :wrap_content

  defp wrap_content(content, options) do
    Logger.debug(comment: @comment, content: content, options: options)
    make_command(content, options)
  end
end
