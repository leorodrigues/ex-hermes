defmodule Hermes.Piping.Outbound.CommandSubmission do
  use Hermes.Piping.Pipe

  alias Hermes.Domain.Envelope

  alias Hermes.Piping.Outbound.MakeCommandPipe

  require Logger

  @submitting_command_msg "Submitting a new command."

  pipe :log_start_of_process
  pipe MakeCommandPipe
  pipe :add_reply_headers

  defp log_start_of_process(payload, _options) do
    Logger.debug(comment: @submitting_command_msg, payload: payload)
    payload
  end

  defp add_reply_headers(envelope, options) do
    Keyword.get(options, :reply_topic) |> set_reply_topic(envelope)
  end

  defp set_reply_topic(nil, _) do
    Logger.error(comment: "Reply topic option is required.")
    halt({:error, :missing_reply_topic})
  end

  defp set_reply_topic(topic, envelope) do
    Envelope.set_header(envelope, :reply_topic, topic)
  end
end
