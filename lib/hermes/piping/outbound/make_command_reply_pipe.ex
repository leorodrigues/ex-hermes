defmodule Hermes.Piping.Outbound.MakeCommandReplyPipe do
  use Hermes.Piping.Pipe

  import Hermes.Domain.Context

  require Logger

  @comment "Making a command reply"

  pipe :wrap_payload
  pipe :try_to_continue

  defp wrap_payload(payload, options) do
    Logger.debug(comment: @comment, content: payload, opts: options)
    make_reply(payload, options)
  end

  defp try_to_continue({:error, _} = e, _), do: halt(e)
  defp try_to_continue(reply, _), do: reply
end
