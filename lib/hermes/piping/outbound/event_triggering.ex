defmodule Hermes.Piping.Outbound.EventTriggering do
  use Hermes.Piping.Pipe

  require Logger

  @triggering_event_msg "Triggering a new event."

  pipe :log_start_of_process
  pipe Hermes.Piping.Outbound.MakeEventPipe

  defp log_start_of_process(envelope, _options) do
    Logger.debug(comment: @triggering_event_msg); envelope
  end
end
