defmodule Hermes.Piping.Inbound.OpenVoidCommandPipe do
  use Hermes.Piping.Pipe

  alias Hermes.Piping.Inbound.StandardEnvelopeOpeningPipe

  require Logger

  @opening_msg "Opening a void command."

  @convey_opts scope: :open_void_command_pipe, message_type: :command

  pipe :announce_start
  pipe StandardEnvelopeOpeningPipe, convey_opts: @convey_opts

  defp announce_start(envelope, opts) do
    Logger.debug(comment: @opening_msg, options: opts, envelope: envelope)
    envelope
  end
end
