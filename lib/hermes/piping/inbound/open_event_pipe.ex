defmodule Hermes.Piping.Inbound.OpenEventPipe do
  use Hermes.Piping.Pipe

  alias Hermes.Piping.Inbound.StandardEnvelopeOpeningPipe

  require Logger

  @opening_msg "Opening an event."

  @convey_opts scope: :open_event_pipe, message_type: :event

  pipe :announce_start
  pipe StandardEnvelopeOpeningPipe, convey_opts: @convey_opts

  defp announce_start(envelope, opts) do
    Logger.debug(comment: @opening_msg, options: opts, envelope: envelope)
    envelope
  end
end
