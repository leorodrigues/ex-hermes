defmodule Hermes.Piping.Inbound.OpenCommandReplyPipe do
  use Hermes.Piping.Pipe

  alias Hermes.Piping.Inbound.ConfirmEnvelopeStructurePipe
  alias Hermes.Piping.Inbound.StandardEnvelopeOpeningPipe

  require Logger

  @opening_msg "Opening a command reply."

  @scope :open_command_reply_pipe

  @reply_convey_opts scope: @scope, names: [:source_id]

  @standard_convey_opts scope: @scope, message_type: :reply

  pipe :announce_start
  pipe ConfirmEnvelopeStructurePipe, convey_opts: @reply_convey_opts
  pipe StandardEnvelopeOpeningPipe, convey_opts: @standard_convey_opts

  defp announce_start(envelope, opts) do
    Logger.debug(comment: @opening_msg, options: opts, envelope: envelope)
    envelope
  end
end
