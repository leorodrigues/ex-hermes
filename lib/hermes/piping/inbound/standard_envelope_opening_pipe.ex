defmodule Hermes.Piping.Inbound.StandardEnvelopeOpeningPipe do
  use Hermes.Piping.Pipe

  alias Hermes.Domain.Envelope

  alias Hermes.Piping.Inbound.ConfirmEnvelopeStructurePipe

  import Hermes.Domain.Context, only: [
    extract_logger_metadata: 1,
    copy_from_envelope_to_process: 1
  ]

  require Logger

  @wrong_type_msg "Wrong message type received."

  @missing_message_type_option_msg "Option :message_type is required."

  @scope :standard_envelope_opening_pipe

  @convey_opts [ scope: @scope, names: [:message_type, :message_id] ]

  pipe ConfirmEnvelopeStructurePipe, convey_opts: @convey_opts
  pipe :confirm_type
  pipe :load_logger_metadata
  pipe :load_process_context
  pipe :open_envelope

  defp confirm_type(%Envelope{ } = e, options) when is_list(options) do
    confirm_type(e, Keyword.get(options, :message_type))
  end

  defp confirm_type(%Envelope{header: %{message_id: id}}, nil) do
    Logger.error(comment: @missing_message_type_option_msg, message_id: id)
    halt(make_missing_expected_type_error(id))
  end

  defp confirm_type(%Envelope{header: %{message_id: id} = h} = e, type) do
    case h do
      %{message_type: ^type} -> e
      %{message_type: t} -> fail_by_wrong_type(type, t, id)
    end
  end

  defp load_logger_metadata(%Envelope{ } = envelope, _opts) do
    extract_logger_metadata(envelope) |> Logger.metadata()
    envelope
  end

  defp load_process_context(%Envelope{ } = envelope, _opts) do
    copy_from_envelope_to_process(envelope)
  end

  defp open_envelope(%Envelope{ } = envelope, _opts) do
    Envelope.get_content(envelope)
  end

  defp fail_by_wrong_type(expected_type, given_type, id) do
    Logger.error(
      comment: @wrong_type_msg,
      scope: @scope,
      given_message_type: given_type,
      expected_message_type: expected_type,
      message_id: id
    )
    halt(make_wrong_type_given_error(expected_type, given_type, id))
  end

  defp make_missing_expected_type_error(message_id) do
    {:error, {:pipe_config, [
      scope: @scope,
      reason: :missing_key,
      key: :message_type,
      message_id: message_id
    ]}}
  end

  defp make_wrong_type_given_error(expected_type, given_type, id) do
    {:error, {:malformed_envelope, [
      scope: @scope,
      reason: :wrong_message_type,
      expected_type: expected_type,
      given_message_type: given_type,
      message_id: id
    ]}}
  end
end
