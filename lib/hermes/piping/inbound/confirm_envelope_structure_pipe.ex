defmodule Hermes.Piping.Inbound.ConfirmEnvelopeStructurePipe do
  use Hermes.Piping.Pipe

  alias Hermes.Domain.Envelope

  import Keyword, only: [get: 3]

  require Logger

  @required_keys_missing_msg "Required headers are missing."

  @not_an_envelope_msg "The given payload is no an envelope."

  pipe :confirm

  defp confirm(envelope, opts) do
    do_confirm(envelope, get(opts, :names, [ ]), get(opts, :scope, :no_scope))
  end

  defp do_confirm(%Envelope{ } = e, [ ], _), do: e

  defp do_confirm(%Envelope{header: h} = e, names, scope) do
    ensure_no_missing_names(e, get_missing(names, Map.keys(h)), scope)
  end

  defp do_confirm(_, _, scope) do
    Logger.warning(comment: @not_an_envelope_msg, scope: scope)
    halt(make_not_an_envelope_error(scope))
  end

  defp ensure_no_missing_names(envelope, [ ], _), do: envelope
  defp ensure_no_missing_names(%Envelope{ }, missing_keys, scope) do
    Logger.error(
      comment: @required_keys_missing_msg,
      scope: scope,
      reason: :missing_header_keys,
      keys: missing_keys
    )
    halt(make_missing_header_keys_error(scope, missing_keys))
  end

  defp get_missing(left, right) do
    Enum.filter(left, fn k -> k not in right end)
  end

  defp make_missing_header_keys_error(scope, missing_keys) do
    {:error, {:malformed_envelope, [
      scope: scope,
      reason: :missing_header_keys,
      keys: missing_keys
    ]}}
  end

  defp make_not_an_envelope_error(scope) do
    {:error, {:malformed_envelope, [
      scope: scope,
      reason: :not_an_envelope
    ]}}
  end
end
