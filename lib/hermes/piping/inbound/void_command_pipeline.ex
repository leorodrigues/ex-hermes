defmodule Hermes.Piping.Inbound.VoidCommandPipeline do
  use Hermes.Piping.Pipe

  pipe Hermes.Piping.Inbound.OpenVoidCommandPipe
  pipe Hermes.Piping.Inbound.RunBusinessTargetPipe

end
