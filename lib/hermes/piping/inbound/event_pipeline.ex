defmodule Hermes.Piping.Inbound.EventPipeline do
  use Hermes.Piping.Pipe

  pipe Hermes.Piping.Inbound.OpenEventPipe
  pipe Hermes.Piping.Inbound.RunBusinessTargetPipe

end
