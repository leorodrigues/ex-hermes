defmodule Hermes.Piping.Inbound.CommandReplyPipeline do
  use Hermes.Piping.Pipe

  pipe Hermes.Piping.Inbound.OpenCommandReplyPipe
  pipe Hermes.Piping.Inbound.RunBusinessTargetPipe

end
