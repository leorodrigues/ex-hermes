defmodule Hermes.Piping.Inbound.RunBusinessTargetPipe do
  use Hermes.Piping.Pipe

  pipe :run_business_target

  defp run_business_target(payload, options) do
    {module, function} = Keyword.get(options, :business_target)
    apply(module, function, [payload, Enum.into(options, %{ })])
  end
end
