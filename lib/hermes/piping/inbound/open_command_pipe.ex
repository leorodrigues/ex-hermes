defmodule Hermes.Piping.Inbound.OpenCommandPipe do
  use Hermes.Piping.Pipe

  alias Hermes.Piping.Inbound.ConfirmEnvelopeStructurePipe
  alias Hermes.Piping.Inbound.StandardEnvelopeOpeningPipe

  require Logger

  @opening_msg "Opening a command."

  @scope :open_command_pipe

  @command_convey_opts scope: @scope, names: [:reply_topic]

  @standard_convey_opts scope: @scope, message_type: :command

  pipe :announce_start
  pipe ConfirmEnvelopeStructurePipe, convey_opts: @command_convey_opts
  pipe StandardEnvelopeOpeningPipe, convey_opts: @standard_convey_opts

  defp announce_start(envelope, opts) do
    Logger.debug(comment: @opening_msg, options: opts, envelope: envelope)
    envelope
  end
end
