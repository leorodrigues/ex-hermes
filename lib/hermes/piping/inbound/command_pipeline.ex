defmodule Hermes.Piping.Inbound.CommandPipeline do
  use Hermes.Piping.Pipe

  pipe Hermes.Piping.Inbound.OpenCommandPipe
  pipe Hermes.Piping.Inbound.RunBusinessTargetPipe
  pipe Hermes.Piping.Outbound.MakeCommandReplyPipe
  pipe Hermes.Piping.Outbound.PublishReplyPipe

end
