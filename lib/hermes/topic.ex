defmodule Hermes.Topic do
  use GenServer
  @moduledoc false

  @myself __MODULE__

  @tasker Hermes.Dispatching.TaskSupervisor
  @task_opts restart: :temporaty

  @error_topic "@defaults/error-occurred"
  def error_topic, do: @error_topic

  @exit_topic "@defaults/exit-occurred"
  def exit_topic, do: @exit_topic

  @default_dispatch_on_publish :single_topic
  def default_dispatch_on_publish, do: @default_dispatch_on_publish

  @default_max_recovery 1

  alias Task.Supervisor

  alias Hermes.Configuration
  alias Hermes.Dispatching.Dispatcher
  alias Hermes.Topic.ExpandTopic
  alias Hermes.Topic.SinkMessage
  alias Hermes.Topic.HandleRegister
  alias Hermes.Topic.HandleUnregister

  def start_link(options \\ [ ]) do
    GenServer.start_link(__MODULE__, options, [name: @myself])
  end

  @impl true
  def init(options) do
    {:ok, Enum.into(options, %{
      max_recovery: @default_max_recovery,
      topics: [ ]
    })}
  end

  def get_dispatch_on_publish do
    Configuration.get(@myself, :dispatch_on_publish, @default_dispatch_on_publish)
  end

  def set_dispatch_on_publish(value) do
    Configuration.set(@myself, :dispatch_on_publish, value)
  end

  def register_server_cast(topic_id, server_id, message_id) do
    request = {:register, {topic_id, {{:server, server_id}, message_id}}}
    GenServer.cast(@myself, request)
  end

  def unregister_server_cast(topic_id, server_id, message_id) do
    request = {:unregister, {topic_id, {{:server, server_id}, message_id}}}
    GenServer.cast(@myself, request)
  end

  def broadcast(topic_id, message) do
    GenServer.cast(@myself, make_broadcast_req(topic_id, message))
  end

  @impl true
  def handle_cast({:register, request}, %{topics: t} = state) do
    HandleRegister.invoke(request, t) |> respond_to_cast(state)
  end

  @impl true
  def handle_cast({:unregister, request}, %{topics: t} = state) do
    HandleUnregister.invoke(request, t) |> respond_to_cast(state)
  end

  @impl true
  def handle_cast({:broadcast, {topic_id, message}}, %{topics: t} = state) do
    unless invoke_all_operations(ExpandTopic.invoke(topic_id, t), message, topic_id) == 0 do
      try_dispatching(topic_id)
    end
    {:noreply, state}
  end

  defp try_dispatching(topic_id) do
    case get_dispatch_on_publish() do
      :single_topic -> Dispatcher.dispatch(topic_id)
      :all_topics -> Dispatcher.dispatch()
      :offline -> :ok
    end
  end

  defp respond_to_cast({:ok, topics}, state) do
    {:noreply, %{state | topics: topics}}
  end

  defp invoke_all_operations({:ok, [ ] = operations}, message, id) do
    SinkMessage.invoke(id, message, :no_handlers_available)
    length(operations)
  end

  defp invoke_all_operations({:ok, operations}, message, _) do
    Enum.each(operations, invoke_operations(message))
    length(operations)
  end

  defp invoke_operations(message) do
    fn ({t, n, o}) -> start_task(t, n, o, message) end
  end

  defp start_task(target_type, target_name, operation_id, message) do
    task_function = safe_apply(target_type, target_name, operation_id, message)
    Supervisor.start_child(@tasker, task_function, @task_opts)
  end

  defp safe_apply(:server, server_id, message_id, %{payload: p}) do
    fn -> GenServer.cast(server_id, {message_id, p}) end
  end

  defp make_broadcast_req(topic_id, payload) do
    {:broadcast, {topic_id, %{payload: payload, recover: nil}}}
  end
end
