defmodule Hermes.Shutdown do
  alias Hermes.InboxSchemaServer
  alias Hermes.Dispatching.Coordinator

  require Logger

  def run do
    Logger.debug("Commencing shutdown")
    InboxSchemaServer.sync()
    Coordinator.cease_operation()
    Logger.debug("Shutdown complete")
  end
end
