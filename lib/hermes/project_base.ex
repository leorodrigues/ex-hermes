defmodule Hermes.ProjectBase do
  use GenServer

  @self __MODULE__

  @separator "/"

  import Enum, only: [join: 2]

  @spec set_base_path(path :: [String.t]) :: [String.t]
  def set_base_path(path) do
    GenServer.call(@self, {:set_base_path, path})
  end

  @spec compute_event_topic(
    app_name :: atom, event_name :: String.t | atom
  ) :: String.t
  def compute_event_topic(app_name, event_name) do
    GenServer.call(@self, {:compute_event_topic, app_name, event_name})
  end

  @spec compute_command_topic(
    app_name :: atom, command_name :: String.t | atom
  ) :: String.t
  def compute_command_topic(app_name, command_name) do
    GenServer.call(@self, {:compute_command_topic, app_name, command_name})
  end

  @spec compute_reply_topic(
    app_name :: atom, target_name :: atom, command_name :: String.t | atom
  ) :: String.t
  def compute_reply_topic(app_name, target_name, cmd_name) do
    GenServer.call(@self, {:compute_reply_topic, app_name, target_name, cmd_name})
  end

  def start_link(opts \\ [ ]) do
    GenServer.start_link(__MODULE__, Keyword.get(opts, :base_path, [ ]), name: @self)
  end

  @impl true
  def init(base_path \\ [ ]) do
    {:ok, base_path}
  end

  @impl true
  def handle_call({:set_base_path, path}, _from, old_path) do
    {:reply, old_path, path}
  end

  @impl true
  def handle_call({:compute_event_topic, app_name, event_name}, _from, base_path) do
    [comp_base_path(base_path, app_name, "events"), dashify(event_name)]
      |> join(@separator)
      |> reply(base_path)
  end

  @impl true
  def handle_call({:compute_command_topic, app_name, command_name}, _from, base_path) do
    [comp_base_path(base_path, app_name, "commands"), dashify(command_name)]
      |> join(@separator)
      |> reply(base_path)
  end

  @impl true
  def handle_call({:compute_reply_topic, app_name, target_name, cmd_name}, _from, base_path) do
    [comp_base_path(base_path, app_name, "replies"), dashify(target_name), dashify(cmd_name)]
      |> join(@separator)
      |> reply(base_path)
  end

  defp reply(topic, path) do
    {:reply, topic, path}
  end

  defp comp_base_path(base_path, app_name, type) do
    base_path ++ [dashify(app_name), type] |> join(@separator)
  end

  defp dashify(input) when is_atom(input), do: to_string(input) |> dashify()
  defp dashify(input) when is_binary(input), do: String.replace(input, "_", "-")
end
