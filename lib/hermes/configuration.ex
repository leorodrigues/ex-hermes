defmodule Hermes.Configuration do
  use Scrolls.PermanentScroll, scroll_name: :hermes_configuration

  @match {:"$1", :"$2", :"$3"}

  def get(server, key, default \\ nil) do
    execute_fn(&handle_get/4, [server, key, default])
  end

  def set(server, key, value) do
    execute_fn(&handle_set/4, [server, key, value])
    value
  end

  defp handle_get(table, server, key, default) do
    case select(table, server, key) do
      [ ] -> default
      result -> result |> List.first()
    end
  end

  defp handle_set(table, server, key, value) do
    :dets.insert(table, {server, key, value})
  end

  defp select(table, server, key) do
    :dets.select(table, [{@match, make_query(server, key), [:"$3"]}])
  end

  defp make_query(server, key) do
    [{:andalso, {:==, :"$1", server}, {:==, :"$2", key}}]
  end
end
