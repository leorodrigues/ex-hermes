defmodule Hermes.AsyncHandling do

  alias Task.Supervisor

  require Logger

  @running_msg "Running async task."

  def run_task(supervisor, module, function, args \\ [ ]) do
    Logger.debug([comment: @running_msg, module: module])
    start_and_wait_task(supervisor, make_call(module, function, args))
  end

  defp start_and_wait_task(supervisor, call) do
    Task.await(Supervisor.async_nolink(supervisor, call))
  end

  defp make_call(module, function, args) do
    fn -> apply(module, function, args) end
  end
end
