defmodule Hermes.TopicHandler do

  alias Hermes.InboxServer
  alias Hermes.InboxSchemaServer
  alias Hermes.Dispatching.DispatchSignaling
  alias Hermes.Dispatching.Dispatcher
  alias Hermes.Topic

  def create_inboxes(topic_id, inbox_ids) do
    register_all_server_casts(inbox_ids, topic_id)
    InboxSchemaServer.add_inboxes(topic_id, inbox_ids)
  end

  def drop_inboxes(topic_id, inbox_ids) do
    unregister_all_server_casts(inbox_ids, topic_id)
    InboxSchemaServer.drop_inboxes(topic_id, inbox_ids)
  end

  def restore_inboxes(topic_id, inbox_ids) do
    register_all_server_casts(inbox_ids, topic_id)
  end

  def broadcast(topic_id, message) do
    Topic.broadcast(topic_id, message)
  end

  def subscribe(topic_id, inbox_id, function) do
    register_cast_if_needed(inbox_id, topic_id)
    Dispatcher.set_handler(topic_id, inbox_id, function)
    DispatchSignaling.register_inbox(topic_id, inbox_id)
    {topic_id, inbox_id}
  end

  def subscribe(topic_id, inbox_id, module_name, function_name) do
    register_cast_if_needed(inbox_id, topic_id)
    Dispatcher.set_handler(topic_id, inbox_id, module_name, function_name)
    DispatchSignaling.register_inbox(topic_id, inbox_id)
    {topic_id, inbox_id}
  end

  def unsubscribe(topic_id, inbox_id) do
    Dispatcher.clear_handler(topic_id, inbox_id)
    DispatchSignaling.signal_halt(topic_id, inbox_id)
    DispatchSignaling.unregister_inbox(topic_id, inbox_id)
    try_clearing_inbox(topic_id, inbox_id)
  end

  defp register_cast_if_needed(inbox_id, topic_id) do
    case InboxSchemaServer.inbox_exists(topic_id, inbox_id) do
      true -> :ok
      false -> register_server_cast({:put, topic_id, inbox_id})
    end
  end

  defp try_clearing_inbox(topic_id, inbox_id) do
    case InboxSchemaServer.inbox_exists(topic_id, inbox_id) do
      true -> :ok
      false ->
        unregister_server_cast({:put, topic_id, inbox_id})
        InboxServer.drop(topic_id, inbox_id)
    end
  end

  defp register_all_server_casts(inbox_ids, topic_id) do
    to_operation_tuple = fn name -> make_operation_id(topic_id, name) end
    inbox_ids
      |> Enum.map(to_operation_tuple)
      |> Enum.each(&register_server_cast/1)
  end

  defp register_server_cast({:put, topic_id, inbox_id} = operation_id) do
    Topic.register_server_cast(topic_id, InboxServer.self, operation_id)
    DispatchSignaling.register_inbox(topic_id, inbox_id)
  end

  defp unregister_all_server_casts(inbox_ids, topic_id) do
    to_operation_tuple = fn name -> make_operation_id(topic_id, name) end
    inbox_ids
      |> Enum.map(to_operation_tuple)
      |> Enum.each(&unregister_server_cast/1)
  end

  defp unregister_server_cast({:put, topic_id, inbox_id} = operation_id) do
    Topic.unregister_server_cast(topic_id, InboxServer.self, operation_id)
    DispatchSignaling.unregister_inbox(topic_id, inbox_id)
  end

  defp make_operation_id(topic_id, inbox_id) do
    {:put, topic_id, inbox_id}
  end
end
