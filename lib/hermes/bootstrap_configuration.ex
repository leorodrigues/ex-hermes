defmodule Hermes.BootstrapConfiguration do

  alias Hermes.Configuration

  require Logger

  import Hermes.Paths, only: [
    compute_config_import_path: 0, compute_default_config_import_path: 0
  ]

  @starting_bootstrap "Commencing configuration bootstrap."

  @bootstrap_finished "Configuration bootstrap finished."

  @attempting_to_load_config "Attempting to load configuration."

  @failed_to_import "Failed to import configuration. Using defaults."

  def run do
    Logger.debug([comment: @starting_bootstrap])
    compute_config_import_path() |> import_file()
    Logger.debug([comment: @bootstrap_finished])
  end

  defp import_file(:no_file) do
    path = compute_default_config_import_path()
    Logger.debug([comment: @attempting_to_load_config, from: path])
    Configuration.import(path)
  end

  defp import_file(path) do
    Logger.debug([comment: @attempting_to_load_config, from: path])
    case File.exists?(path) do
      true -> Configuration.import(path)
      false ->
        Logger.warning([comment: @failed_to_import, from: path])
        Configuration.import(compute_default_config_import_path())
    end
  end

end
