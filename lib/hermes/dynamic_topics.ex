defmodule Hermes.DynamicTopics do

  import Enum, only: [join: 2]

  @separator "/"
  def separator, do: @separator

  @spec compute_event_topic(
    domain :: String.t,
    app_name :: atom,
    event_name :: String.t | atom
  ) :: String.t
  def compute_event_topic(domain, app_name, event_name) do
    compute_event_topic_elements(domain, app_name, event_name)
      |> join_topic_elements()
  end

  @spec compute_event_topic_elements(
    domain :: String.t,
    app_name :: atom,
    event_name :: String.t | atom
  ) :: [String.t]
  def compute_event_topic_elements(domain, app_name, event_name) do
    [domain, dashify(app_name), "events", dashify(event_name)]
  end

  @spec compute_command_topic(
    domain :: String.t,
    app_name :: atom,
    command_name :: String.t | atom
  ) :: String.t
  def compute_command_topic(domain, app_name, command_name) do
    compute_command_topic_elements(domain, app_name, command_name)
      |> join_topic_elements()
  end

  @spec compute_command_topic_elements(
    domain :: String.t,
    app_name :: atom,
    command_name :: String.t | atom
  ) :: [String.t]
  def compute_command_topic_elements(domain, app_name, command_name) do
    [domain, dashify(app_name), "commands", dashify(command_name)]
  end

  @spec compute_reply_topic(
    domain :: String.t,
    app_name :: atom,
    caller_name :: atom,
    command_name :: String.t | atom
  ) :: String.t
  def compute_reply_topic(domain, app_name, caller_name, cmd_name) do
    compute_reply_topic_elements(domain, app_name, caller_name, cmd_name)
      |> join_topic_elements()
  end

  @spec compute_reply_topic_elements(
    domain :: String.t,
    app_name :: atom,
    caller_name :: atom,
    command_name :: String.t | atom
  ) :: [String.t]
  def compute_reply_topic_elements(domain, app_name, caller_name, cmd_name) do
    [domain, dashify(caller_name), "replies", dashify(app_name), dashify(cmd_name)]
  end

  @spec join_topic_elements([String.t]) :: String.t
  def join_topic_elements(elements), do: elements |> join(@separator)

  defp dashify(input) when is_atom(input), do: to_string(input) |> dashify()
  defp dashify(input) when is_binary(input), do: String.replace(input, "_", "-")

end
