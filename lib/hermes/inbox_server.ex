defmodule Hermes.InboxServer do
  use GenServer

  alias Hermes.Inbox

  @myself __MODULE__
  def self, do: @myself

  def put(topic_id, inbox_id, payload, placement \\ :end) do
    GenServer.cast(@myself, {{:put, topic_id, inbox_id, placement}, payload})
  end

  def take(topic_id, inbox_id) do
    GenServer.call(@myself, {:take, topic_id, inbox_id})
  end

  def drop(topic_id, inbox_id) do
    GenServer.call(@myself, {:drop, topic_id, inbox_id})
  end

  def start_link(opts \\ [ ]) do
    GenServer.start_link(__MODULE__, Keyword.get(opts, :init_args, [ ]), name: @myself)
  end

  def stop do
    GenServer.stop(@myself)
  end

  @impl true
  def init(args \\ [ ]) do
    {:ok, Enum.into(args, %{inboxes: %{ }})}
  end

  @impl true
  def handle_call({:take, topic_id, inbox_id}, _from, state) do
    {result, new_state} = handle_take(topic_id, inbox_id, state)
    {:reply, result, new_state}
  end

  @impl true
  def handle_call({:drop, topic_id, inbox_id}, _from, state) do
    {:reply, :ok, handle_drop(topic_id, inbox_id, state)}
  end

  @impl true
  def handle_cast({{:put, topic_id, inbox_id}, payload}, state) do
    {:noreply, handle_put(topic_id, inbox_id, payload, :end, state)}
  end

  @impl true
  def handle_cast({{:put, topic_id, inbox_id, placement}, payload}, state) do
    {:noreply, handle_put(topic_id, inbox_id, payload, placement, state)}
  end

  defp handle_put(topic_id, inbox_id, payload, placement, %{inboxes: q} = state) do
    key = {topic_id, inbox_id}
    run_put(key, q, payload, Map.get(q, key, [ ]), placement, state)
  end

  defp handle_take(topic_id, inbox_id, %{inboxes: q} = state) do
    key = {topic_id, inbox_id}
    case Map.get(q, key, nil) do
      nil -> {:inbox_empty, state}
      inbox -> run_take(inbox, q, key, state)
    end
  end

  defp handle_drop(topic_id, inbox_id, %{inboxes: inboxes_map} = state) do
    key = {topic_id, inbox_id}
    Map.put(state, :inboxes, Map.drop(inboxes_map, [key]))
  end

  defp run_put(key, inbox_map, payload, inbox, placement, state) do
    updated_inbox = Inbox.put(inbox, payload, placement)
    updated_inbox_map = Map.put(inbox_map, key, updated_inbox)
    Map.put(state, :inboxes, updated_inbox_map)
  end

  defp run_take(inbox, inbox_map, key, state) do
    {message, new_inbox} = case Inbox.take(inbox) do
      {nil, [ ]} -> {:inbox_empty, [ ]}
      anything_else -> anything_else
    end

    new_inbox_map = Map.put(inbox_map, key, new_inbox)
    {message, Map.put(state, :inboxes, new_inbox_map)}
  end
end
