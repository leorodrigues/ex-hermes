defmodule Hermes.Clock do
  def now, do: :os.timestamp()
end
