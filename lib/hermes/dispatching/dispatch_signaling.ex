defmodule Hermes.Dispatching.DispatchSignaling do
  use GenServer

  import Enum, only: [reverse: 1]
  import List, only: [keydelete: 3]

  @myself __MODULE__

  @stop_consuming :stop_consuming

  def start_link(opts \\ [ ]) do
    GenServer.start_link(__MODULE__, opts, Keyword.put(opts, :name, @myself))
  end

  @impl true
  def init(_args \\ [ ]) do
    {:ok, %{signals: [ ], entries: [ ]}}
  end

  def register_inbox(topic_id, inbox_id) do
    GenServer.call(@myself, {:register, {topic_id, inbox_id}})
  end

  def unregister_inbox(topic_id, inbox_id) do
    GenServer.call(@myself, {:unregister, {topic_id, inbox_id}})
  end

  def signal_halt() do
    GenServer.call(@myself, :halt)
  end

  def signal_halt(topic_id, inbox_id) do
    GenServer.call(@myself, {:halt, {topic_id, inbox_id}})
  end

  def take(topic_id, inbox_id) do
    GenServer.call(@myself, {:take, {topic_id, inbox_id}})
  end

  @impl true
  def handle_call({:register, inbox_ref}, _from, state) do
    {:reply, :ok, handle_register(state, inbox_ref)}
  end

  @impl true
  def handle_call({:unregister, inbox_ref}, _from, state) do
    {:reply, :ok, handle_unregister(state, inbox_ref)}
  end

  @impl true
  def handle_call(:halt, _from, state) do
    {:reply, :ok, handle_halt(state)}
  end

  @impl true
  def handle_call({:halt, inbox_ref}, _from, state) do
    {:reply, :ok, handle_halt(state, inbox_ref)}
  end

  @impl true
  def handle_call({:take, inbox_ref}, _from, state) do
    handle_take(state, inbox_ref) |> reply_call()
  end

  defp handle_halt(%{signals: s, entries: e} = state) do
    %{state|signals: broadcast_stop_consuming(s, e)}
  end

  defp handle_halt(%{signals: s} = state, inbox_ref) do
    %{state|signals: append_stop_consuming(s, inbox_ref)}
  end

  defp handle_register(%{entries: e} = state, inbox_ref) do
    Map.put(state, :entries, Enum.uniq([inbox_ref|e]))
  end

  defp handle_unregister(%{entries: e} = state, inbox_ref) do
    Map.put(state, :entries, Enum.filter(e, fn e -> e !== inbox_ref end))
  end

  defp handle_take(%{signals: signals} = state, inbox_ref) do
    case List.keyfind(signals, inbox_ref, 0, nil) do
      nil -> {:no_signal, state}
      {_, [s]} -> {s, %{state|signals: keydelete(signals, inbox_ref, 0)}}
      {_, [s|queue]} -> {s, %{state|signals: keyreplace(signals, inbox_ref, queue)}}
    end
  end

  defp broadcast_stop_consuming(signals, [ ]), do: signals
  defp broadcast_stop_consuming(signals, [current|remaining]) do
    broadcast_stop_consuming(append_stop_consuming(signals, current), remaining)
  end

  defp append_stop_consuming(signals, inbox_ref) do
    case List.keyfind(signals, inbox_ref, 0) do
      nil -> [{inbox_ref, [@stop_consuming]}|signals]
      {_, [_] = queue} -> keyreplace(signals, inbox_ref, reverse([@stop_consuming|queue]))
      {_, queue} -> keyreplace(signals, inbox_ref, reverse([@stop_consuming|reverse(queue)]))
    end
  end

  defp reply_call({result, state}) do
    {:reply, result, state}
  end

  defp keyreplace(list, key, replacement) do
    List.keyreplace(list, key, 0, {key, replacement})
  end
end
