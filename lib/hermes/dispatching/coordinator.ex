defmodule Hermes.Dispatching.Coordinator do
  use GenServer

  require Logger

  alias Hermes.Dispatching.DispatchSignaling
  alias Hermes.Dispatching.Dispatcher
  alias Hermes.Configuration

  @myself __MODULE__

  @already_running_msg "Already running."
  @commencing_operation_msg "Commencing operation."
  @already_stopped_msg "Already stopped."
  @ceasing_operation_msg "Ceasing operation."
  @scheduling_cycle_stopped_msg "Scheduling cycle stopped."

  @interval 1000
  def default_sleep_interval, do: @interval

  def get_sleep_interval do
    Configuration.get(@myself, :sleep_interval, @interval)
  end

  def set_sleep_interval(value) do
    Configuration.set(@myself, :sleep_interval, value)
  end

  def commence_operation do
    GenServer.cast(@myself, :commence_operation)
  end

  def cease_operation do
    GenServer.cast(@myself, :cease_operation)
  end

  def is_alive do
    GenServer.call(@myself, :is_alive)
  end

  def start_link(opts \\ [ ]) do
    GenServer.start_link(__MODULE__, opts, Keyword.put(opts, :name, @myself))
  end

  def stop do
    GenServer.stop(@myself)
  end

  @impl true
  def init(init_args \\ [ ]) do
    {:ok, Enum.into(init_args, %{alive: false})}
  end

  @impl true
  def handle_call(:is_alive, _from, %{alive: a} = state) do
    {:reply, a, state}
  end

  @impl true
  def handle_cast(:commence_operation, state) do
    {:noreply, handle_commence_operation(state)}
  end

  @impl true
  def handle_cast(:cease_operation, state) do
    {:noreply, handle_cease_operation(state)}
  end

  @impl true
  def handle_info(:wake_up, state) do
    Dispatcher.dispatch()
    {:noreply, schedule_wake_up_call(state)}
  end

  defp handle_commence_operation(%{alive: true} = state) do
    Logger.warning([comment: @already_running_msg])
    state
  end

  defp handle_commence_operation(state) do
    Logger.info([comment: @commencing_operation_msg])
    schedule_wake_up_call(Map.put(state, :alive, true))
  end

  defp handle_cease_operation(%{alive: false} = state) do
    Logger.warning([comment: @already_stopped_msg])
    state
  end

  defp handle_cease_operation(state) do
    Logger.info([comment: @ceasing_operation_msg])
    DispatchSignaling.signal_halt()
    Map.put(state, :alive, false)
  end

  defp schedule_wake_up_call(%{alive: false} = state) do
    Logger.debug([comment: @scheduling_cycle_stopped_msg])
    state
  end

  defp schedule_wake_up_call(%{timer: t} = state) do
    Process.cancel_timer(t)
    timer = Process.send_after(@myself, :wake_up, get_sleep_interval())
    Map.put(state, :timer, timer)
  end

  defp schedule_wake_up_call(%{ } = state) do
    timer = Process.send_after(@myself, :wake_up, get_sleep_interval())
    Map.put(state, :timer, timer)
  end
end
