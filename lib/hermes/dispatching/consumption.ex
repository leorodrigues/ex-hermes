defmodule Hermes.Dispatching.Consumption do

  alias Hermes.Dispatching.DispatchSignaling
  alias Hermes.Dispatching.Coordinator
  alias Hermes.InboxServer

  require Logger

  @dispatch_start "Message dispatch started."

  @dispatch_end "Message dispatch finished."

  def run(topic_id, inbox_id, handler) do
    case DispatchSignaling.take(topic_id, inbox_id) do
      :stop_consuming -> {:interrupted, topic_id, inbox_id}
      :no_signal -> consume_from_queue(topic_id, inbox_id, handler)
    end
  end

  def consume_from_queue(topic_id, inbox_id, handler) do
    case try_taking_next(topic_id, inbox_id) do
      :inbox_empty -> {:inbox_empty, topic_id, inbox_id}
      :not_alive -> {:not_alive, topic_id, inbox_id}
      message ->
        dispatch(message, topic_id, inbox_id, handler)
        run(topic_id, inbox_id, handler)
    end
  end

  defp try_taking_next(topic_id, inbox_id) do
    case Coordinator.is_alive() do
      true -> InboxServer.take(topic_id, inbox_id)
      false -> :not_alive
    end
  end

  defp dispatch(message, topic_id, inbox_id, {module_name, function_name}) do
    Logger.debug([comment: @dispatch_start, topic_id: topic_id, inbox_id: inbox_id])
    apply(module_name, function_name, [topic_id, inbox_id, message])
    Logger.debug([comment: @dispatch_end, topic_id: topic_id, inbox_id: inbox_id])
  end

  defp dispatch(message, topic_id, inbox_id, fun) do
    Logger.debug([comment: @dispatch_start, topic_id: topic_id, inbox_id: inbox_id])
    fun.(topic_id, inbox_id, message)
    Logger.debug([comment: @dispatch_end, topic_id: topic_id, inbox_id: inbox_id])
  end
end
