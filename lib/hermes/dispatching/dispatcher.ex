defmodule Hermes.Dispatching.Dispatcher do
  use GenServer

  require Logger

  alias Hermes.Dispatching.Consumption
  alias Hermes.Dispatching.TaskSupervisor, as: Tasker

  import List, only: [keyreplace: 4, keyfind: 3, keymember?: 3]

  @not_alive_msg "Hermes is not in operation or has just been stopped."
  @inbox_empty_msg "Dispatching finished. Inbox is now empty."
  @interrupted_msg "Dispatching stopped."
  @repl_task_msg "Restarting dispatch."
  @task_failed_msg "Dispatch failed."
  @sweep_msg "Sweeping handlers."
  @normal_termination_msg "Dispatch finished normally"

  @myself __MODULE__

  def set_handler(topic_id, inbox_id, fun) do
    GenServer.call(@myself, {:set_handler, {topic_id, inbox_id, fun}})
  end

  def set_handler(topic_id, inbox_id, module_name, function_name) do
    handler_spec = {topic_id, inbox_id, module_name, function_name}
    GenServer.call(@myself, {:set_handler, handler_spec})
  end

  def clear_handler(topic_id, inbox_id) do
    GenServer.call(@myself, {:clear_handler, {topic_id, inbox_id}})
  end

  def dispatch do
    GenServer.cast(@myself, :dispatch)
  end

  def dispatch(topic_id) do
    GenServer.cast(@myself, {:dispatch, topic_id})
  end

  def start_link(opts \\ [ ]) do
    GenServer.start_link(__MODULE__, [ ], Keyword.put(opts, :name, @myself))
  end

  def stop do
    GenServer.stop(@myself)
  end

  @impl true
  def init(_init_arg \\ [ ]) do
    {:ok, %{handlers: %{ }, refs: [ ], running: [ ]}}
  end

  @impl true
  def handle_call({:set_handler, handler_spec}, _from, state) do
    {:reply, :ok, handle_set_handler(handler_spec, state)}
  end

  @impl true
  def handle_call({:clear_handler, handler_spec}, _from, state) do
    {:reply, :ok, handle_clear_handler(handler_spec, state)}
  end

  @impl true
  def handle_cast(:dispatch, state) do
    {:noreply, state |> sweep_handlers()}
  end

  @impl true
  def handle_cast({:dispatch, topic_id}, state) do
    {:noreply, state |> sweep_handlers(topic_id)}
  end

  @impl true
  def handle_info({reference, {:not_alive, topic_id, inbox_id}}, state) do
    Logger.debug([comment: @not_alive_msg, target: {topic_id, inbox_id}])
    {:noreply, clear_task(state, topic_id, inbox_id, reference)}
  end

  @impl true
  def handle_info({reference, {:inbox_empty, topic_id, inbox_id}}, state) do
    Logger.debug([comment: @inbox_empty_msg, target: {topic_id, inbox_id}])
    {:noreply, clear_task(state, topic_id, inbox_id, reference)}
  end

  @impl true
  def handle_info({reference, {:interrupted, topic_id, inbox_id}}, state) do
    Logger.debug([comment: @interrupted_msg, target: {topic_id, inbox_id}])
    {:noreply, clear_task(state, topic_id, inbox_id, reference)}
  end

  @impl true
  def handle_info({:DOWN, _, _, _, :normal}, state) do
    Logger.debug([comment: @normal_termination_msg])
    {:noreply, state}
  end

  @impl true
  def handle_info({:DOWN, reference, :process, _, reason}, state) do
    Logger.warning([comment: @task_failed_msg, reason: reason])
    {:noreply, handle_task_failure(reference, state)}
  end

  defp handle_set_handler(handler_spec, %{handlers: h_map} = state) do
    Map.put(state, :handlers, puthandler(h_map, handler_spec))
  end

  defp handle_clear_handler(handler_spec, %{handlers: h_map} = state) do
    Map.put(state, :handlers, drophandler(h_map, handler_spec))
  end

  defp handle_task_failure(reference, %{refs: r_list} = state) do
    case keymember?(r_list, reference, 0) do
      true -> restart_task(r_list, reference, state)
      false -> state
    end
  end

  defp restart_task(r_list, reference, %{handlers: h_map} = state) do
    {_, _, {topic_id, inbox_id}} = keyfind(r_list, reference, 0)
    Logger.debug([comment: @repl_task_msg, topic_id: topic_id, inbox_id: inbox_id])

    t = start_consume_task(topic_id, inbox_id, h_map)
    refreplace(reference, t, topic_id, inbox_id, state)
  end

  defp sweep_handlers(%{running: running} = state) do
    do_sweep(state, fn n -> n not in running end)
  end

  defp sweep_handlers(%{running: running} = state, topic_id) do
    do_sweep(state, fn {t, _} = n -> n not in running and t == topic_id end)
  end

  defp do_sweep(%{handlers: h_map, running: running} = state, except_running) do
    Logger.debug([comment: @sweep_msg])
    all_names = Map.keys(h_map)
    not_running = Enum.filter(all_names, except_running)
    new_state = Map.put(state, :running, Enum.concat(not_running, running))
    refs = Enum.map(not_running, fn {t, q} -> consume(t, q, state) end)
    add_all_refs(refs, new_state)
  end

  defp consume(topic_id, inbox_id, %{handlers: h_map}) do
    t = start_consume_task(topic_id, inbox_id, h_map)
    {t.ref, t, {topic_id, inbox_id}}
  end

  defp start_consume_task(topic_id, inbox_id, handler_map) do
    handler = Map.get(handler_map, {topic_id, inbox_id})
    arguments = [topic_id, inbox_id, handler]
    Task.Supervisor.async_nolink(Tasker, Consumption, :run, arguments)
  end

  defp add_all_refs(new_refs, %{refs: existing_refs} = state) do
    Map.put(state, :refs, Enum.concat(existing_refs, new_refs))
  end

  defp puthandler(handler_map, {topic_id, inbox_id, module_name, f_name}) do
    Map.put(handler_map, {topic_id, inbox_id}, {module_name, f_name})
  end

  defp puthandler(handler_map, {topic_id, inbox_id, fun}) do
    Map.put(handler_map, {topic_id, inbox_id}, fun)
  end

  defp drophandler(handler_map, {topic_id, inbox_id}) do
    Map.drop(handler_map, [{topic_id, inbox_id}])
  end

  defp refreplace(old_ref, task, topic_id, inbox_id, %{refs: r} = state) do
    new_ref_list = keyreplace(r, old_ref, 0, {task.ref, task, {topic_id, inbox_id}})
    Map.put(state, :refs, new_ref_list)
  end

  defp clear_task(state, topic_id, inbox_id, reference) do
    state |> clear_running(topic_id, inbox_id) |> clear_ref(reference)
  end

  defp clear_running(%{running: running} = state, topic_id, inbox_id) do
    key = {topic_id, inbox_id}
    Map.put(state, :running, Enum.filter(running, fn n -> n !== key end))
  end

  defp clear_ref(%{refs: r_list} = state, reference) do
    {_, new_ref_list} = List.keytake(r_list, reference, 0)
    Map.put(state, :refs, new_ref_list)
  end
end
