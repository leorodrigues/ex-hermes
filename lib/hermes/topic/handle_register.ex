defmodule Hermes.Topic.HandleRegister do
  @moduledoc false

  def invoke({topic_id, {target_id, operation_id}}, state) do
    {:ok, register({topic_id, {target_id, operation_id}}, state)}
  end

  defp register({topic_id, {target_id, operation_id}}, state) do
    case keyfind(state, topic_id) do
      :not_found ->
        [make_topic(topic_id, target_id, operation_id) | state]
      topic ->
        keyreplace(state, add_to_topic(topic, target_id, operation_id))
    end
  end

  defp make_topic(topic_id, target_id, operation_id) do
    {topic_id, [make_target(target_id, operation_id)]}
  end

  defp add_to_topic({topic_id, targets}, target_id, operation_id) do
    case keyfind(targets, target_id) do
      :not_found ->
        {topic_id, [make_target(target_id, operation_id) | targets]}
      target ->
        {topic_id, keyreplace(targets, add_to_target(target, operation_id))}
    end
  end

  defp make_target(target_id, operation_id) do
    {target_id, [operation_id]}
  end

  defp add_to_target({target_id, operation_ids}, operation_id) do
    {target_id, Enum.uniq([operation_id | operation_ids])}
  end

  defp keyreplace(list, {key, _} = tuple) do
    List.keyreplace(list, key, 0, tuple)
  end

  defp keyfind(list, key) do
    List.keyfind(list, key, 0, :not_found)
  end
end
