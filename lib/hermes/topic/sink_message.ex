defmodule Hermes.Topic.SinkMessage do
  require Logger

  def invoke(topic, message, reason) do
    Logger.warning(inspect({:hermes_message_sunk, reason, topic, message}))
  end
end
