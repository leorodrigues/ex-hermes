defmodule Hermes.Topic.HandleUnregister do
  @moduledoc false

  def invoke({id, {module_name, callback_name}}, state) do
    {:ok, unregister({id, {module_name, callback_name}}, state)}
  end

  defp unregister({id, {module_name, callback_name}}, state) do
    case keyfind(state, id) do
      :not_found -> state
      t -> keyreplace(state, remove_from_topic(t, module_name, callback_name))
    end
  end

  defp remove_from_topic({id, modules} = topic, module_name, callback_name) do
    case keyfind(modules, module_name) do
      :not_found -> topic
      m -> {id, keyreplace(modules, remove_from_module(m, callback_name))}
    end
  end

  defp remove_from_module({module_name, callbacks}, callback_name) do
    {module_name, Enum.filter(callbacks, &(&1 !== callback_name))}
  end

  defp keyreplace(list, {key, _} = tuple) do
    List.keyreplace(list, key, 0, tuple)
  end

  defp keyfind(list, key) do
    List.keyfind(list, key, 0, :not_found)
  end
end
