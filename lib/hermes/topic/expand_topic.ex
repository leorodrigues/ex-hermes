defmodule Hermes.Topic.ExpandTopic do
  @moduledoc false

  def invoke(topic_id, topics) do
    {:ok, expand(topic_id, topics)}
  end

  defp expand(topic_id, topics) do
    topic_id |> expand_topic(topics) |> expand_targets()
  end

  defp expand_topic(topic_id, topics) do
    case keyfind(topics, topic_id) do
      :not_found -> [ ]
      {_, targets} -> targets
    end
  end

  defp expand_targets(targets) do
    Enum.flat_map(targets, &expand_operations/1)
  end

  defp expand_operations({{target_type, target_name}, operation_ids}) do
    Enum.map(operation_ids, &({target_type, target_name, &1}))
  end

  defp keyfind(list, key) do
    List.keyfind(list, key, 0, :not_found)
  end
end
