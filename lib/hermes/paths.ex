defmodule Hermes.Paths do

  alias Hermes.EnvProvider

  @default_base_dir "/var/lib"

  @data_file_name "schema.bin"

  @env_provider Application.compile_env(:hermes, :env_provider, EnvProvider)

  def compute_bootstrap_import_path do
    @env_provider.get_env(:hermes, :schema_bootstrap_file, nil)
  end

  def compute_config_import_path do
    @env_provider.get_env(:hermes, :config_bootstrap_file, :no_file)
  end

  def compute_default_config_import_path do
    Application.app_dir(:hermes, ["priv", "default-configuration.terms"])
  end

  def compute_schema_file_path do
    Path.join([compute_data_path(), @data_file_name])
  end

  def compute_data_path do
    Path.join([get_base_path(), "hermes", "data"])
  end

  defp get_base_path do
    @env_provider.get_env(:hermes, :base_dir, @default_base_dir)
  end
end
