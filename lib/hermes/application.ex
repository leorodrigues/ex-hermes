defmodule Hermes.Application do
  @moduledoc false

  use Application

  require Logger

  import Enum, only: [filter: 2]
  import Hermes.Paths, only: [compute_bootstrap_import_path: 0]

  @env_provider Application.compile_env(:hermes, :env_provider, Hermes.EnvProvider)

  @opts [strategy: :one_for_one, name: Hermes.Supervisor]

  @impl true
  def start(_type, _args) do
    children = [
      {Task.Supervisor, name: Hermes.Dispatching.TaskSupervisor},
      {Hermes.Configuration, [ ]},
      {Hermes.Topic, [ ]},
      {Hermes.InboxServer, [ ]},
      {Hermes.InboxSchemaServer, [ ]},
      {Hermes.Dispatching.DispatchSignaling, [ ]},
      {Hermes.Dispatching.Dispatcher, [ ]},
      {Hermes.Dispatching.Coordinator, [ ]}
    ]

    result = bootstrap(children, get_exclusion_list())
    Logger.debug("Hermes running")
    result
  end

  @impl true
  def prep_stop(_) do
    Logger.debug("Preparing to stop Hermes")
    shutdown(get_exclusion_list())
  end

  defp bootstrap(children, [ ]) do
    Logger.debug("Starting Hermes")
    result = start_supervisor(children)
    Hermes.BootstrapConfiguration.run()
    Logger.debug("Commencing bootstrap procedures")
    try_bootstraping_schema()
    result
  end

  defp bootstrap(children, exclusion_list) do
    Logger.debug("Partially starting Hermes")
    Logger.debug("Skipping servers: #{inspect(exclusion_list)}")
    result = start_supervisor(filter(children, excluding(exclusion_list)))
    Hermes.BootstrapConfiguration.run()
    Logger.debug("Skipping bootstrap procedures")
    result
  end

  defp try_bootstraping_schema do
    case @env_provider.get_env(:hermes, :skip_schema_bootstrap, false) do
      false -> Hermes.BootstrapSchema.run(compute_bootstrap_import_path())
      true -> Logger.debug("Skipping schema bootstrap")
    end
  end

  defp shutdown([ ]) do
    Hermes.Shutdown.run()
  end

  defp shutdown(_) do
    Logger.debug("Skipping shutdown procedures")
  end

  defp excluding(list) do
    fn {name, _} -> name not in list end
  end

  defp get_exclusion_list do
    @env_provider.get_env(:hermes, :prevent_start_list, [ ])
  end

  defp start_supervisor(children) do
    Supervisor.start_link(children, @opts)
  end
end
