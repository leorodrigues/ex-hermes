defmodule Hermes.Domain.Context do

  alias Hermes.Domain.Envelope

  import :church, only: [partial_left: 2, chain_apply: 2, break_on_error: 1]

  @logger_keys [:message_type, :message_id, :correlation_id, :source_id]

  @context_name :hermes_context
  @spec context_name :: :hermes_context
  def context_name, do: @context_name

  @new_message_opts [key_name: :message_id]

  def copy_from_envelope_to_process(%Envelope{ } = e) do
    Envelope.copy_header_to_process(e, @context_name)
  end

  def get_reply_topic_from_process do
    Process.get(@context_name, %{ }) |> Map.get(:reply_topic)
  end

  def make_command(content, options \\ [ ]) do
    make_header(:command, options) |> make_envelope(content)
  end

  def make_event(content, options \\ [ ]) do
    make_header(:event, options) |> make_envelope(content)
  end

  def make_reply(content, options \\ [ ]) do
    make_header(:reply, options) |> make_envelope(content)
  end

  def extract_logger_metadata(%Envelope{header: h}) do
    Map.to_list(h) |> filter_logger_keys(@logger_keys)
  end

  defp make_envelope({:error, _} = e, _), do: e
  defp make_envelope(header, content) do
    Envelope.new(content, @new_message_opts) |> Envelope.merge_header(header)
  end

  defp make_header(type, options) do
    add_extras(%{message_type: type}, Process.get(@context_name), options)
  end

  defp add_extras(ctx, map, opts) do
    chain_apply(ctx, [
      partial_left(&add_source_id/2, [map]),
      &break_on_error/1,
      partial_left(&add_correlation_id/3, [
        map, Keyword.get(opts, :correlation_id)
      ])
    ])
  end

  defp add_source_id(%{message_type: :reply} = ctx, %{message_id: id}) do
    Map.put(ctx, :source_id, id)
  end

  defp add_source_id(%{message_type: :reply}, _) do
    {:error, {:process_context_key_missing, :message_id}}
  end

  defp add_source_id(ctx, %{message_id: id}) do
    Map.put(ctx, :source_id, id)
  end

  defp add_source_id(ctx, _), do: ctx

  defp add_correlation_id(ctx, %{correlation_id: id}, nil) do
    Map.put(ctx, :correlation_id, id)
  end

  defp add_correlation_id(ctx, _, nil) do
    Map.put(ctx, :correlation_id, UUID.uuid4())
  end

  defp add_correlation_id(ctx, _, override) do
    Map.put(ctx, :correlation_id, override)
  end

  defp filter_logger_keys(list, keys) do
    Enum.filter(list, fn {k, _} -> k in keys end)
  end
end
