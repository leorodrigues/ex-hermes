defmodule Hermes.Domain.Envelope do
  defstruct [
    header: %{ },
    content: nil
  ]

  @type t :: %__MODULE__{
    header: map,
    content: any
  }

  @spec new(any) :: __MODULE__.t
  def new(content, options \\ [ ]) do
    %__MODULE__{content: content, header: make_header(options)}
  end

  @spec merge_header(__MODULE__.t, map, [atom]) :: __MODULE__.t
  def merge_header(%__MODULE__{ } = e, %{ } = header, preserve_keys \\ [ ]) do
    with %__MODULE__{header: h} <- e do
      %__MODULE__{e|header: Map.merge(h, Map.drop(header, preserve_keys))}
    end
  end

  def set_header(%__MODULE__{header: h} = e, key, value) do
    %__MODULE__{e|header: Map.put(h, key, value)}
  end

  @spec get_content(__MODULE__.t()) :: any
  def get_content(%__MODULE__{content: c}), do: c

  @spec copy_header_to_process(__MODULE__.t, atom) :: __MODULE__.t
  def copy_header_to_process(%__MODULE__{header: header} = envelope, name) do
    header |> put_in_process(name)
    envelope
  end

  def copy_header_from_process(%__MODULE__{header: h} = envelope, name, keys) do
    %__MODULE__{envelope|header: merge_context_with_header(h, name, keys)}
  end

  @spec copy_header_to_process(__MODULE__.t, atom, [atom]) :: __MODULE__.t
  def copy_header_to_process(%__MODULE__{header: h} = envelope, name, keys) do
    Map.to_list(h)
      |> Enum.filter(fn {k, _} -> k in keys end)
      |> Enum.into(%{ })
      |> put_in_process(name)
    envelope
  end

  defp put_in_process(map, name) do
    Process.put(name, Map.merge(Process.get(name, %{ }), map))
  end

  defp merge_context_with_header(map, name, keys) do
    Map.to_list(Process.get(name, %{ }))
      |> Enum.filter(fn {k, _} -> k in keys end)
      |> Enum.into(map)
  end

  defp make_header(options) do
    Map.put(%{ }, Keyword.get(options, :key_name, :id), UUID.uuid4)
  end
end
