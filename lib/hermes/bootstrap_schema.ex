defmodule Hermes.BootstrapSchema do

  require Logger

  import :church, only: [chain_apply: 2, partial_left: 2]

  import Hermes.TopicHandler, only: [restore_inboxes: 2]
  import Hermes.Dispatching.Coordinator, only: [commence_operation: 0]
  import Hermes.InboxSchemaServer, only: [
    list_inboxes: 0, create_schema: 0, import_schema: 1
  ]

  @start_schema_bootstrap "Commencing schema bootstrap."

  @schema_bootstrap_finished "Schema bootstrap finished."

  @attempting_to_load_schema "Attempting to load schema."

  def run(import_path) do
    Logger.debug([comment: @start_schema_bootstrap])
    chain_apply(list_inboxes(), [
      partial_left(&try_importing_schema/2, [import_path]),
      &restore_all_topics/1
    ])
    commence_operation()
    Logger.debug([comment: @schema_bootstrap_finished])
  end

  defp try_importing_schema({:error, {:schema_not_found, _}}, nil) do
    create_schema()
    [ ]
  end

  defp try_importing_schema({:error, {:schema_not_found, _}}, path) do
    Logger.debug([comment: @attempting_to_load_schema, from: path])
    create_schema()
    import_schema(path)
  end

  defp try_importing_schema(inbox_list, _), do: inbox_list

  defp restore_all_topics(inbox_list) do
    Enum.each(inbox_list, &restore_topic/1)
  end

  defp restore_topic({topic, inboxes}), do: restore_inboxes(topic, inboxes)
end
