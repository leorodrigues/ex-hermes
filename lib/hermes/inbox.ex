defmodule Hermes.Inbox do

  import List, only: [flatten: 1, pop_at: 2]
  import Enum, only: [reverse: 1]

  @spec put(inbox :: list, payload :: term | [term]) :: list
  def put(inbox, payload), do: put(inbox, payload, :end)

  @spec put(inbox :: list, payload :: term | [term], placement :: :start|:end) :: list
  def put(inbox, payload, :end)
    when is_list(inbox), do: flatten([inbox, payload])

  def put(inbox, payload, :start) when is_list(inbox) and is_list(payload) do
    flatten([reverse(payload), inbox])
  end

  def put(inbox, payload, :start) when is_list(inbox) do
    flatten([payload, inbox])
  end

  @spec take(inbox :: list) :: {message :: term, inbox :: list}
  def take(inbox) when is_list(inbox), do: pop_at(inbox, 0)

end
