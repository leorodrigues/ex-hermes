defmodule Hermes.Piping do

  alias Hermes.Piping.Outbound.EventTriggering
  alias Hermes.Piping.Outbound.VoidCommandSubmission
  alias Hermes.Piping.Outbound.CommandSubmission

  @type id :: String.t | atom

  @spec set_base_path([String.t]) :: [String.t]
  def set_base_path(base_topic_path) do
    Hermes.ProjectBase.set_base_path(base_topic_path)
  end

  @spec compute_event_topic(atom, id) :: String.t
  def compute_event_topic(app_name, event_name) do
    Hermes.ProjectBase.compute_event_topic(app_name, event_name)
  end

  @spec compute_command_topic(atom, id) :: String.t
  def compute_command_topic(app_name, command_name) do
    Hermes.ProjectBase.compute_command_topic(app_name, command_name)
  end

  @spec compute_reply_topic(atom, atom, id) :: String.t
  def compute_reply_topic(app_name, target_name, cmd_name) do
    Hermes.ProjectBase.compute_reply_topic(app_name, target_name, cmd_name)
  end

  @spec subscribe_pipeline_handling(id, id, atom) :: any
  def subscribe_pipeline_handling(topic, inbox, handler_module) do
    Hermes.subscribe(topic, inbox, handler_module, :handle)
  end

  @spec subscribe_pipeline_handling(id, id, atom, atom) :: any
  def subscribe_pipeline_handling(topic, inbox, module, function) do
    Hermes.subscribe(topic, inbox, module, function)
  end

  @spec unsubscribe_pipeline_handling(id, id) :: any
  def unsubscribe_pipeline_handling(topic, inbox) do
    Hermes.unsubscribe(topic, inbox)
  end

  @spec submit_void_command(id, term, [keyword]) :: :ok
  def submit_void_command(command_topic, payload, opts \\ [ ]) do
    VoidCommandSubmission.convey(payload, opts) |> publish(command_topic)
  end

  @spec submit_command(id, id, term, [keyword]) :: :ok
  def submit_command(cbk_topic, cmd_topic, payload, opts \\ [ ]) do
    CommandSubmission.convey(payload, put_reply(opts, cbk_topic))
      |> publish(cmd_topic)
    :ok
  end

  def trigger_event(event_topic, payload, opts \\ [ ]) do
    EventTriggering.convey(payload, opts) |> publish(event_topic)
    :ok
  end

  defp put_reply(options, reply_topic) do
    Keyword.put(options, :reply_topic, reply_topic)
  end

  defp publish(payload, topic), do: Hermes.publish(topic, payload)
end
