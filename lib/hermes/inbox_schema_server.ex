defmodule Hermes.InboxSchemaServer do
  use GenServer

  require Logger

  import :church, only: [chain_apply: 2, partial_left: 2]
  import Hermes.Paths, only: [compute_schema_file_path: 0]
  import Enum, only: [into: 2, concat: 2, uniq: 1, reject: 2]

  @myself __MODULE__

  @opening_schema "Opening schema."
  @removing_schema "Removing schema."
  @closing_schema "Closing schema and clearing state."

  def self, do: @myself

  def create_schema do
    GenServer.call(@myself, :create_schema)
  end

  def drop_schema do
    GenServer.call(@myself, :drop_schema)
  end

  def import_schema(file_path) do
    GenServer.call(@myself, {:import_schema, file_path})
  end

  def sync do
    GenServer.call(@myself, :sync)
  end

  def add_inboxes(topic_id, inbox_ids) do
    GenServer.call(@myself, {:add_inboxes, topic_id, inbox_ids})
  end

  def drop_inboxes(topic_id, inbox_ids) do
    GenServer.call(@myself, {:drop_inboxes, topic_id, inbox_ids})
  end

  def drop_inboxes(topic_id) do
    GenServer.call(@myself, {:drop_inboxes, topic_id})
  end

  def list_inboxes do
    GenServer.call(@myself, :list_inboxes)
  end

  def inbox_exists(topic_id, inbox_id) do
    GenServer.call(@myself, {:inbox_exists, topic_id, inbox_id})
  end

  def start_link(opts \\ [ ]) do
    args = Keyword.get(opts, :init_args, [ ])
    GenServer.start_link(__MODULE__, args, name: @myself)
  end

  def stop do
    GenServer.stop(@myself)
  end

  @impl true
  def init(args \\ [ ]) do
    {:ok, into(args, %{schema_path: resolve_default_schema_path()})}
  end

  @impl true
  def handle_call({:inbox_exists, topic_id, inbox_id}, _from, state) do
    handle_inbox_exists(topic_id, inbox_id, state) |> reply_call()
  end

  @impl true
  def handle_call({:add_inboxes, topic_id, inbox_ids}, _from, state) do
    handle_add_inboxes(topic_id, inbox_ids, state) |> reply_call()
  end

  @impl true
  def handle_call({:drop_inboxes, topic_id, inbox_ids}, _from, state) do
    handle_drop_inboxes(topic_id, inbox_ids, state) |> reply_call()
  end

  @impl true
  def handle_call({:drop_inboxes, topic_id}, _from, state) do
    handle_drop_inboxes(topic_id, state) |> reply_call()
  end

  @impl true
  def handle_call(:list_inboxes, _from, state) do
    state |> handle_list_inboxes() |> reply_call()
  end

  @impl true
  def handle_call({:import_schema, file_path}, _from, state) do
    handle_import_schema(file_path, state) |> reply_call()
  end

  @impl true
  def handle_call(:create_schema, _from, state) do
    handle_create_schema(state) |> reply_call()
  end

  @impl true
  def handle_call(:drop_schema, _from, state) do
    handle_drop_schema(state) |> reply_call()
  end

  @impl true
  def handle_call(:sync, _from, %{table: t} = state) do
    handle_sync(t, state) |> reply_call()
  end

  @impl true
  def terminate(_reason, %{table: t}) do
    :dets.sync(t)
    :dets.close(t)
    :ok
  end

  @impl true
  def terminate(_, _), do: :ok

  defp handle_create_schema(state) do
    chain_apply(state, [
      &ensure_base_path_exists/1,
      &confirm_schema_is_missing/1,
      &open_dets_table/1,
      &init_records/1,
      partial_left(&add_table_to_state/2, [state])
    ])
  end

  defp handle_drop_schema(state) do
    chain_apply(state, [
      &confirm_schema_exists/1,
      &remove_schema_from_state/1,
      &remove_schema_file/1
    ])
  end

  defp handle_import_schema(file_path, state) do
    chain_apply(state, [
      &try_loading_schema/1,
      &break_on_error/1,
      partial_left(&import_schema/2, [file_path])
    ])
  end

  defp handle_inbox_exists(topic_id, inbox_id, state) do
    chain_apply(state, [
      &try_loading_schema/1,
      &break_on_error/1,
      partial_left(&run_inbox_exists/4, [topic_id, inbox_id, state])
    ])
  end

  defp handle_add_inboxes(topic_id, inbox_ids, state) do
    chain_apply(state, [
      &try_loading_schema/1,
      &break_on_error/1,
      partial_left(&run_add_inboxes/3, [topic_id, inbox_ids])
    ])
  end

  defp handle_drop_inboxes(topic_id, inbox_ids, %{table: table} = state) do
    chain_apply(state, [
      &try_loading_schema/1,
      &break_on_error/1,
      partial_left(&drop_inboxes/4, [topic_id, inbox_ids, table])
    ])
  end

  defp handle_drop_inboxes(topic_id, state) do
    chain_apply(state, [
      &try_loading_schema/1,
      &break_on_error/1,
      partial_left(&run_drop_inboxes/2, [topic_id])
    ])
  end

  defp handle_list_inboxes(state) do
    chain_apply(state, [
      &try_loading_schema/1,
      &break_on_error/1,
      &list_inboxes/1
    ])
  end

  defp handle_sync(table, state) do
    case :dets.sync(table) do
      {:error, _} = e -> {e, state}
      :ok -> state
    end
  end

  defp try_loading_schema(%{table: _} = state), do: state

  defp try_loading_schema(state) do
    chain_apply(state, [
      &confirm_schema_exists/1,
      &open_dets_table/1,
      partial_left(&add_table_to_state/2, [state])
    ])
  end

  defp import_schema(state, file_path) do
    chain_apply(file_path, [
      partial_left(&confirm_terms_file_exists/2, [state]),
      &consult_schema/1,
      partial_left(&persist_schema/2, [state]),
      &list_inboxes/1
    ])
  end

  defp run_add_inboxes(%{table: table} = state, topic_id, inbox_ids) do
    chain_apply(table, [
      &load_current_schema/1,
      partial_left(&add_inboxes_to_topic/3, [topic_id, inbox_ids]),
      partial_left(&persist_schema/2, [state])
    ])
  end

  defp drop_inboxes(state, topic_id, inbox_ids, table) do
    chain_apply(table, [
      &load_current_schema/1,
      partial_left(&remove_inboxes_from_topic/3, [topic_id, inbox_ids]),
      partial_left(&persist_schema/2, [state])
    ])
  end

  defp run_drop_inboxes(%{table: table} = state, topic_id) do
    chain_apply(table, [
      &load_current_schema/1,
      partial_left(&remove_topic/2, [topic_id]),
      partial_left(&persist_schema/2, [state])
    ])
  end

  defp run_inbox_exists(%{table: table}, topic_id, inbox_id, state) do
    chain_apply(table, [
      &load_current_schema/1,
      partial_left(&lookup_inbox/4, [topic_id, inbox_id, state])
    ])
  end

  defp lookup_inbox(schema, topic_id, inbox_id, state) do
    case Map.get(schema, topic_id, [ ]) do
      [ ] -> {false, state}
      inbox_ids -> {inbox_id in inbox_ids, state}
    end
  end

  defp list_inboxes(%{table: t} = state) do
    {load_current_schema(t) |> to_tuple_list(), state}
  end

  defp to_tuple_list(map) do
    append = fn key, list -> [{key, Map.get(map, key)}|list] end
    map |> Map.keys() |> List.foldr([], append)
  end

  defp confirm_schema_is_missing(%{schema_path: path} = state) do
    case File.exists?(path) do
      true -> {:break, {{:error, {:schema_already_exists, path}}, state}}
      false -> state
    end
  end

  defp confirm_schema_exists(%{schema_path: path} = state) do
    case File.exists?(path) do
      false -> {:break, {{:error, {:schema_not_found, path}}, state}}
      true -> state
    end
  end

  defp confirm_terms_file_exists(file_path, state) do
    case File.exists?(file_path) do
      false -> {:break, {{:error, {:file_not_found, file_path}}, state}}
      true -> file_path
    end
  end

  defp open_dets_table(%{schema_path: path}) do
    Logger.debug([comment: @opening_schema, path: path])
    {:ok, table} = :dets.open_file(:schemas, [
      {:type, :set},
      {:file, to_charlist(path)}
    ])
    table
  end

  defp init_records(table) do
    :ok = :dets.insert(table, {:current_version, 0})
    :ok = :dets.insert(table, {{:schema, 0}, %{ }})
    table
  end

  defp add_table_to_state(table, state) do
    Map.put(state, :table, table)
  end

  defp remove_schema_file(%{schema_path: path} = state) do
    Logger.debug([comment: @removing_schema, path: path])
    File.rm!(path)
    state
  end

  defp remove_schema_from_state(%{table: t} = state) do
    Logger.debug([comment: @closing_schema])
    :dets.close(t)
    Map.drop(state, [:table])
  end

  defp load_current_schema(table) do
    [{_, version}] = :dets.lookup(table, :current_version)
    [{_, schema}] = :dets.lookup(table, {:schema, version})
    schema
  end

  defp consult_schema(file_path) do
    {:ok, terms} = :file.consult(to_charlist(file_path))
    into(terms, %{ })
  end

  defp add_inboxes_to_topic(schema, topic_id, inbox_ids) do
    inbox_list = uniq(concat(Map.get(schema, topic_id, [ ]), inbox_ids))
    Map.put(schema, topic_id, inbox_list)
  end

  defp remove_inboxes_from_topic(schema, topic_id, inbox_ids) do
    inbox_list = reject(Map.get(schema, topic_id, [ ]), except_from(inbox_ids))
    Map.put(schema, topic_id, inbox_list)
  end

  defp remove_topic(schema, topic_id), do: Map.drop(schema, [topic_id])

  defp except_from(exception_list), do: fn e -> e in exception_list end

  defp persist_schema(schema, %{table: t} = state) do
    version = :dets.update_counter(t, :current_version, 1)
    case :dets.insert(t, {{:schema, version}, schema}) do
      {:error, _} = e -> {e, state}
      :ok -> state
    end
  end

  defp break_on_error({{:error, _}, _} = e), do: {:break, e}

  defp break_on_error(outcome), do: outcome

  defp reply_call({{:error, _} = e, state}) do
    {:reply, e, state}
  end

  defp reply_call({result, state}) do
    {:reply, result, state}
  end

  defp reply_call(%{ } = state) do
    {:reply, :ok, state}
  end

  defp resolve_default_schema_path(), do: compute_schema_file_path()

  defp ensure_base_path_exists(%{schema_path: p} = state) do
    File.mkdir_p!(Path.dirname(p))
    state
  end
end
