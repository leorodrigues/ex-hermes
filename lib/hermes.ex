defmodule Hermes do

  alias Hermes.Dispatching.Coordinator
  alias Hermes.InboxSchemaServer
  alias Hermes.TopicHandler
  alias Hermes.BootstrapSchema
  alias Hermes.Topic

  import Hermes.Paths, only: [compute_config_import_path: 0]

  def get_sleep_interval do
    Coordinator.get_sleep_interval()
  end

  def set_sleep_interval(value) do
    Coordinator.set_sleep_interval(value)
  end

  def get_dispatch_on_publish do
    Topic.get_dispatch_on_publish()
  end

  def set_dispatch_on_publish(value) do
    Topic.set_dispatch_on_publish(value)
  end

  def bootstrap_schema do
    BootstrapSchema.run(compute_config_import_path())
  end

  def commence_operation do
    Coordinator.commence_operation
  end

  def cease_operation do
    Coordinator.cease_operation
  end

  def create_schema do
    InboxSchemaServer.create_schema
  end

  def drop_schema do
    InboxSchemaServer.drop_schema
  end

  def import_schema(file_path) do
    InboxSchemaServer.import_schema(file_path)
  end

  def list_inboxes do
    InboxSchemaServer.list_inboxes
  end

  def add_inboxes(topic_id, inbox_ids) do
    TopicHandler.create_inboxes(topic_id, inbox_ids)
  end

  def drop_inboxes(topic_id, inbox_ids) do
    TopicHandler.drop_inboxes(topic_id, inbox_ids)
  end

  def publish(topic_id, message) do
    TopicHandler.broadcast(topic_id, message)
  end

  def subscribe(topic_id, function) when is_function(function) do
    TopicHandler.subscribe(topic_id, UUID.uuid4(), function)
  end

  def subscribe(topic_id, inbox_id, function) when is_function(function) do
    TopicHandler.subscribe(topic_id, inbox_id, function)
  end

  def subscribe(topic_id, module_name, function_name) do
    TopicHandler.subscribe(topic_id, UUID.uuid4(), module_name, function_name)
  end

  def subscribe(topic_id, inbox_id, module_name, function_name) do
    TopicHandler.subscribe(topic_id, inbox_id, module_name, function_name)
  end

  def unsubscribe(topic_id, inbox_id) do
    TopicHandler.unsubscribe(topic_id, inbox_id)
  end
end
