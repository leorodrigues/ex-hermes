
# Version 3.0.0

## ADDED

- Modules:
  - Hermes.Domain.Context

## REMOVED

- Modules:
  - Hermes.Piping.Inbound.OpenMessagePipe
  - Hermes.Piping.Outbound.MakeMessagePipe
  - Hermes.Piping.CommandLayer
  - Hermes.Piping.MessageLayer

# Version 3.0.0-beta

## IMPROVED

- Listener modules will unregister from the dispatching cycle uppon termination.

# Version 3.0.0-alpha

## MOVED / RENAMED

- Macros:
  - Hermes.Piping.Listener.listen/3 --> Hermes.Piping.Listener.listen_to/3
  - Hermes.Piping.Listener.listen/4 --> Hermes.Piping.Listener.listen_to/4

## REMOVED

- Macros:
  - Hermes.Piping.Listener.listen_to_command/5
  - Hermes.Piping.Listener.listen_to_command/6
  - Hermes.Piping.Listener.listen_to_event/5
  - Hermes.Piping.Listener.listen_to_event/6
  - Hermes.Piping.Listener.listen_to_reply/6
  - Hermes.Piping.Listener.listen_to_reply/7

- Modules:
  - Hermes.Message
  - Hermes.Piping.Inbound.CommandHandling
  - Hermes.Piping.Inbound.CommandReplyHandling
  - Hermes.Piping.Inbound.VoidCommandHandling
  - Hermes.Piping.Inbound.EventHandling

## ADDED

- Expansion of the following piping handler parameters:
  - domain
  - app_name
  - event_name
  - command_name
  - caller_name

## FIXED

- Corrected the declaration of overloaded piping handlers

## IMPROVED

- Added regression test infrastructure